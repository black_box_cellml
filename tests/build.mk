bin_PROGRAMS += GenBlackBox BlackBoxesForVariants
GenBlackBox_SOURCES = $(top_srcdir)/tests/GenBlackBox.cpp
GenBlackBox_LDADD = \
  $(top_builddir)/libBlaBoC_API.la \
  $(CELLML_API_DIR)/libcevas.la \
  $(CELLML_API_DIR)/libcuses.la \
  $(CELLML_API_DIR)/libmalaes.la
GenBlackBox_CXXFLAGS = -Wall \
  -I$(top_srcdir) -I$(top_srcdir)/sources -I $(top_srcdir)/sources/BlaBoC \
  -I$(CELLML_API_DIR) -I$(CELLML_API_DIR)/sources -I$(CELLML_API_DIR)/interfaces \
  -I$(CELLML_API_DIR)/CeVAS/sources -I$(CELLML_API_DIR)/AnnoTools/sources \
  -I$(CELLML_API_DIR)/CUSES/sources -I$(CELLML_API_DIR)/MaLaES/sources \
  -I$(top_builddir)/interfaces $(AM_CXXFLAGS)

BlackBoxesForVariants_SOURCES = $(top_srcdir)/tests/BlackBoxesForVariants.cpp
BlackBoxesForVariants_LDADD = \
  $(top_builddir)/libBlaBoC_API.la \
  $(CELLML_API_DIR)/libcevas.la \
  $(CELLML_API_DIR)/libcuses.la \
  $(CELLML_API_DIR)/libmalaes.la -lmysqlpp
BlackBoxesForVariants_CXXFLAGS = -Wall \
  -I$(top_srcdir) -I$(top_srcdir)/sources -I $(top_srcdir)/sources/BlaBoC \
  -I$(CELLML_API_DIR) -I$(CELLML_API_DIR)/sources -I$(CELLML_API_DIR)/interfaces \
  -I$(CELLML_API_DIR)/CeVAS/sources -I$(CELLML_API_DIR)/AnnoTools/sources \
  -I$(CELLML_API_DIR)/CUSES/sources -I$(CELLML_API_DIR)/MaLaES/sources \
  -I$(top_builddir)/interfaces $(AM_CXXFLAGS)
