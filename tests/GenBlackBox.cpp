#include "Utilities.hxx"
#include "IfaceBlaBoC.hxx"
#include "BlaBoCBootstrap.hpp"
#include "CellMLBootstrap.hpp"
#include <string>
#include <iostream>

iface::cellml_api::CellMLVariable*
findVariable(iface::cellml_api::Model* m, const char* path)
{
  const char* slash = strchr(path, '/');
  if (slash == NULL)
  {
    printf("Variable paths should be written as component/variable not %s\n", path);
    throw std::exception();
  }

  wchar_t comp[slash - path + 1];
  mbstowcs(comp, path, slash - path + 1);
  comp[slash-path] = 0;
  
  slash++;
  uint32_t l = strlen(slash);
  wchar_t var[l + 1];
  mbstowcs(var, slash, l + 1);
  var[l] = 0;

  RETURN_INTO_OBJREF(mc, iface::cellml_api::CellMLComponentSet,
                     m->modelComponents());
  RETURN_INTO_OBJREF(c, iface::cellml_api::CellMLComponent,
                     mc->getComponent(comp));
  if (c == NULL)
  {
    printf("No such component as %S\n", comp);
    throw iface::cellml_api::CellMLException();
  }
  RETURN_INTO_OBJREF(vs, iface::cellml_api::CellMLVariableSet,
                     c->variables());
  iface::cellml_api::CellMLVariable* v =
    vs->getVariable(var);
  if (v == NULL)
  {
    printf("No such component as %S\n", var);
    throw iface::cellml_api::CellMLException();
  }

  return v;
}

int
main(int argc, char** argv)
{
  bool showAsm = false;
  if (argc >= 2)
  {
    if (!strcmp(argv[1], "-asm"))
    {
      int i;
      showAsm = true;
      for (i = 2; i < argc; i++)
        argv[i - 1] = argv[i];
      argc--;
    }
  }
  if (argc < 2)
  {
    printf("Usage: GenBlackBox [-asm] url (component/variable )*[- (component/variable )*]\n");
    return 1;
  }
  RETURN_INTO_OBJREF(bbs, iface::blaboc_api::BlaBoCBootstrap,
                     createBlaBoCBootstrap());
  RETURN_INTO_OBJREF(cbs, iface::cellml_api::CellMLBootstrap,
                     CreateCellMLBootstrap());
  RETURN_INTO_OBJREF(ml, iface::cellml_api::DOMModelLoader,
                     cbs->modelLoader());
  char* doingWhat;

  try
  {
    doingWhat = "Loading the model";
    uint32_t l = strlen(argv[1]) + 1;
    wchar_t data[l];
    mbstowcs(data, argv[1], l);
    data[l - 1] = 0;
    RETURN_INTO_OBJREF(m, iface::cellml_api::Model, ml->loadFromURL(data));
    doingWhat = "Creating BlaBoC";
    RETURN_INTO_OBJREF(bb, iface::blaboc_api::BlaBoC, bbs->createBlaBoC(m));

    uint32_t i = 2;
    bool inp = true;
    for (; i < (uint32_t)argc; i++)
    {
      if (!strcmp(argv[i], "-"))
      {
        inp = false;
        continue;
      }
      if (!strcmp(argv[i], "*"))
      {
        if (inp)
          bb->othersAreInput();
        else
          bb->othersAreOutput();
        continue;
      }
      doingWhat = "Finding a variable";
      RETURN_INTO_OBJREF(v, iface::cellml_api::CellMLVariable,
                         findVariable(m, argv[i]));
      if (inp)
      {
        doingWhat = "Setting a variable as input";
        bb->variableIsInput(v);
      }
      else
      {
        doingWhat = "Setting a variable as output";
        bb->variableIsOutput(v);
      }
    }

    doingWhat = "Computing the BlaBoC code";

    try
    {
      bb->computeBlaBoC();
    }
    catch (iface::blaboc_api::BlaBoCException& bbe)
    {
    }
    
    RETURN_INTO_WSTRING(em, bb->errorMessage());
    if (em != L"")
    {
      printf("Error message: %S\n", em.c_str());
    }
    else
    {
      RETURN_INTO_WSTRING(ci, bb->classImplementation());
      uint32_t len;
      if (showAsm)
        printf("%S\n", ci.c_str());
      else
      {
        char* bb = bbs->assembleBlaBoC(ci.c_str(), &len);
        std::string code(bb, len);
        free(bb);
        std::cout << code;
      }
    }
  }
  catch (...)
  {
    printf("Failure: %s.\n", doingWhat);
    return 1;
  }

  return 0;
}
