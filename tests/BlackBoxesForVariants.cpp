#include "Utilities.hxx"
#include "IfaceBlaBoC.hxx"
#include "BlaBoCBootstrap.hpp"
#include "CellMLBootstrap.hpp"
#include <string>
#include <iostream>
#include <fstream>
#include <mysql++/mysql++.h>
#include <mysql++/transaction.h>

class BlackBoxesForVariants
{
public:
  BlackBoxesForVariants()
    : mInputIndex(0), mOutputIndex(0)
  {
    mBBS = already_AddRefd<iface::blaboc_api::BlaBoCBootstrap>
      (createBlaBoCBootstrap());
    mCBS = already_AddRefd<iface::cellml_api::CellMLBootstrap>
      (CreateCellMLBootstrap());
    mML = already_AddRefd<iface::cellml_api::DOMModelLoader>
      (mCBS->modelLoader());
  }

  void
  SetConnection
  (
   mysqlpp::Connection& aConn
  )
  {
    mConn = &aConn;
    mysqlpp::Query q(mConn->query());
    q << "SELECT id, component, variable, dirtype FROM variables";
    try
    {
      mVariables = q.store();
    }
    catch (std::exception& e)
    {
      printf("Error getting variables: %s\n", e.what());
    }
  }

  void
  DoVariant
  (
   uint32_t aVariantId,
   const char* aVariantName
  )
  {
    std::string inputFn(aVariantName);
    std::string outputFn(aVariantName);
    
    inputFn += ".cellml";
    outputFn += ".obj";

    mOutputIndex = 0;
    mInputIndex = 0;
    
    printf("Generating %s from %s\n", outputFn.c_str(), inputFn.c_str());
    
    char* doingWhat;
    
    try
    {
      doingWhat = "Loading the model";
      uint32_t l = inputFn.length() + 1;
      wchar_t data[l];
      mbstowcs(data, inputFn.c_str(), l);
      data[l - 1] = 0;
      RETURN_INTO_OBJREF(m, iface::cellml_api::Model, mML->loadFromURL(data));
      doingWhat = "Creating BlaBoC";
      RETURN_INTO_OBJREF(bb, iface::blaboc_api::BlaBoC, mBBS->createBlaBoC(m));

      mysqlpp::Transaction trans(*mConn);

      mysqlpp::Query q(mConn->query());
      q << "UPDATE training_data SET array_index=%0q "
           "WHERE variable_id=%1q "
        "AND variant_id=%2q;";
      q.parse();

      // Now we need to assign inputs and outputs...
      uint32_t i = 0;
      l = mVariables.rows();
      for (; i < l; i++)
      {
        mysqlpp::Row r = mVariables.at(i);
        const char* comp8 = r["component"];
        uint32_t comp_l = strlen(comp8);
        wchar_t comp[comp_l + 1];
        mbstowcs(comp, comp8, comp_l + 1);

        const char* var8 = r["variable"];
        uint32_t var_l = strlen(var8);
        wchar_t var[var_l + 1];
        mbstowcs(var, var8, var_l + 1);

        RETURN_INTO_OBJREF(mc, iface::cellml_api::CellMLComponentSet,
                           m->modelComponents());
        RETURN_INTO_OBJREF(c, iface::cellml_api::CellMLComponent,
                           mc->getComponent(comp));

        // Ignore it if it is knocked out in this model...
        if (c == NULL)
          continue;

        RETURN_INTO_OBJREF(vs, iface::cellml_api::CellMLVariableSet,
                           c->variables());
        RETURN_INTO_OBJREF(v, iface::cellml_api::CellMLVariable,
                           vs->getVariable(var));
        if (v == NULL)
          continue;

        uint32_t idx;
        int dt = r["dirtype"];
        if (dt == 0)
        {
          idx = mInputIndex++;
          bb->variableIsInput(v);
        }
        else
        {
          idx = mOutputIndex++;
          bb->variableIsOutput(v);
        }

        try
        {
          uint32_t id = r["id"];
          q.execute(mysqlpp::SQLString(idx), mysqlpp::SQLString(id),
                    mysqlpp::SQLString(aVariantId));
        }
        catch (std::exception& e)
        {
          printf("Error updating training array index: %s\n", e.what());
        }
      }

      trans.commit();

      doingWhat = "Computing the BlaBoC code";
      
      try
      {
        bb->computeBlaBoC();
      }
      catch (iface::blaboc_api::BlaBoCException& bbe)
      {
      }
    
      RETURN_INTO_WSTRING(em, bb->errorMessage());
      if (em != L"")
      {
        printf("Error message: %S\n", em.c_str());
      }
      else
      {
        RETURN_INTO_WSTRING(ci, bb->classImplementation());
        uint32_t len;

        char* bb = mBBS->assembleBlaBoC(ci.c_str(), &len);
        std::string code(bb, len);
        free(bb);

        std::ofstream obj(outputFn.c_str());
        obj << code;
      }
    }
    catch (...)
    {
      printf("Failure: %s.\n", doingWhat);
      return;
    }
  }

private:
  ObjRef<iface::blaboc_api::BlaBoCBootstrap> mBBS;
  ObjRef<iface::cellml_api::CellMLBootstrap> mCBS;
  ObjRef<iface::cellml_api::DOMModelLoader> mML;
  mysqlpp::Connection* mConn;
  mysqlpp::Result mVariables;
  uint32_t mInputIndex, mOutputIndex;
};

int
main(int argc, char** argv)
{
  if (argc < 5)
  {
    printf("Usage: BlackBoxesForVariants db_host db_user db_password db_database\n");
    return 1;
  }

  mysqlpp::Connection conn(mysqlpp::use_exceptions);
  mysqlpp::Connection conn1(mysqlpp::use_exceptions);

  try
  {
    conn.connect(argv[4], argv[1], argv[2], argv[3]);
    conn1.connect(argv[4], argv[1], argv[2], argv[3]);
  }
  catch (std::exception& e)
  {
    printf("Error connecting to database: %s\n", e.what());
    return 2;
  }

  mysqlpp::Query query(conn.query());

  BlackBoxesForVariants bbfv;
  
  try
  {
    bbfv.SetConnection(conn1);

    mysqlpp::ResUse r = query.store("SELECT id, name FROM variants");

    try
    {
      while (true)
      {
        mysqlpp::Row row = r.fetch_row();
        bbfv.DoVariant(row["id"], row["name"]);
      }
    }
    catch (mysqlpp::EndOfResults& eor)
    {
    }
  }
  catch (std::exception& e)
  {
    printf("Error processing data: %s\n", e.what());
    return 2;
  }
}
