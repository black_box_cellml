#include <exception>
#include "IfaceBlaBoC.hxx"
#include "IfaceCeVAS.hxx"
#include "IfaceCUSES.hxx"
#include "IfaceMaLaES.hxx"
#include "Utilities.hxx"
#include <string>
#include <set>
#include <map>

template<class C> class CleanupList
  : public std::list<C>
{
public:
  ~CleanupList()
  {
    typename CleanupList<C>::iterator i;
    for (i = CleanupList<C>::begin(); i != CleanupList<C>::end(); i++)
      (*i)->release_ref();
  }
};

class BBABlaBoC
  : public iface::blaboc_api::BlaBoC
{
public:
  CDA_IMPL_REFCOUNT;
  CDA_IMPL_ID;
  CDA_IMPL_QI1(blaboc_api::BlaBoC);

  BBABlaBoC(iface::cellml_api::Model* aModel);
  ~BBABlaBoC();

  void variableIsInput(iface::cellml_api::CellMLVariable* aVariable)
    throw(std::exception&);
  void variableIsOutput(iface::cellml_api::CellMLVariable* aVariable)
    throw(std::exception&);
  void othersAreOutput() throw();
  void othersAreInput() throw();
  void computeBlaBoC() throw(std::exception&);

  wchar_t* errorMessage() throw();
  wchar_t* classImplementation() throw(std::exception&);

private:
  class BBAEquation
    : public iface::XPCOM::IObject
  {
  public:
    CDA_IMPL_QI0;
    CDA_IMPL_ID;
    CDA_IMPL_REFCOUNT;

    BBAEquation(iface::cellml_api::CellMLElement* aContext,
                iface::mathml_dom::MathMLElement* aEquation)
      : _cda_refcount(1), mContext(aContext), mEquation(aEquation), used(false)
    {
    }
    
    ObjRef<iface::cellml_api::CellMLElement> mContext;
    ObjRef<iface::mathml_dom::MathMLElement> mEquation;
    // Not owning because the model is owned the whole time.
    // For black-boxes, the input variables.
    std::list<iface::cellml_api::CellMLVariable*> mSourceVariables;
    // Only used for black boxes.
    std::list<iface::cellml_api::CellMLVariable*> mOutputs;
    bool used;
  };

  void BuildEquationList(std::list<BBAEquation*>& eqns);
  bool IsEquationExternal(iface::mathml_dom::MathMLApplyElement* mae);
  void ClassifyEquations(std::list<BBAEquation*>& eqns,
                         std::list<BBAEquation*>& aBlackBoxes,
                         std::list<BBAEquation*>& aWhiteBoxes);
  void getServices(
                   ObjRef<iface::cellml_services::CUSES>& cuses,
                   ObjRef<iface::cellml_services::MaLaESTransform>& mt
                  );
  void determineEquationVariables(
                                  iface::cellml_services::CUSES* cuses,
                                  iface::cellml_services::MaLaESTransform * mt,
                                  std::list<BBAEquation*>& eqns
                                 );
  void processConstants(
                        iface::cellml_services::MaLaESTransform* mt,
                        iface::cellml_services::CUSES* cuses,
                        std::set<iface::cellml_api::CellMLVariable*>& allSourceVariables,
                        std::set<iface::cellml_api::CellMLVariable*>& directConstants,
                        std::set<iface::cellml_api::CellMLVariable*>& allConstants,
                        std::set<iface::cellml_api::CellMLVariable*>& wanted,
                        std::list<BBAEquation*>& whiteBoxes,
                        std::wstring& aComment,
                        std::wstring& aCode,
                        std::list<std::wstring>& aSupplementary
                       );
  void determineEvaluationDAG(
                              std::set<iface::cellml_api::CellMLVariable*>& know,
                              std::set<iface::cellml_api::CellMLVariable*>& want,
                              std::list<BBAEquation*>& equations,
                              std::multimap<iface::cellml_api::CellMLVariable*,
                                            iface::cellml_api::CellMLVariable*>& forwardEdges,
                              std::multimap<iface::cellml_api::CellMLVariable*,
                                            iface::cellml_api::CellMLVariable*>& backEdges
                             );
  void dfsComputeSet(iface::cellml_services::MaLaESTransform* mt,
                     iface::cellml_services::CUSES* cuses,
                     std::set<iface::cellml_api::CellMLVariable*>& targets,
                     std::multimap<iface::cellml_api::CellMLVariable*,
                     iface::cellml_api::CellMLVariable*>& backEdges,
                     std::wstring& aCode,
                     std::list<std::wstring>& aSupplementary
                    );
  void dfsComputeVariable
  (
   iface::cellml_services::MaLaESTransform* mt,
   iface::cellml_services::CUSES* cuses,
   iface::cellml_api::CellMLVariable* var,
   std::multimap<iface::cellml_api::CellMLVariable*,
   iface::cellml_api::CellMLVariable*>& backEdges,
   std::wstring& aCode,
   std::list<std::wstring>& aSupplementary
  );
  void WriteDirectAssignment
  (
   std::wstring& aCode,
   std::list<std::wstring>& aSupplementary,
   iface::cellml_services::MaLaESTransform* mt,
   iface::cellml_services::CUSES* cuses,
   iface::cellml_api::CellMLVariable* var,
   iface::mathml_dom::MathMLElement* lhs,
   iface::mathml_dom::MathMLElement* rhs
  );
  void determineBlackBoxIO
  (
   iface::cellml_services::MaLaESTransform* mt,
   iface::cellml_services::CUSES* cuses,
   std::list<BBAEquation*>& blackBoxes,
   std::set<iface::cellml_api::CellMLVariable*>& blackBoxInputs,
   std::set<iface::cellml_api::CellMLVariable*>& blackBoxOutputs,
   std::set<iface::cellml_api::CellMLVariable*>& blackBoxIO,
   uint32_t& tmpInMax,
   uint32_t& tmpOutMax,
   std::wstring& blackBoxCode
  );
  void addBlackBoxInput
  (
   iface::cellml_services::MaLaESTransform* mt,
   iface::cellml_services::CUSES* cuses,
   BBAEquation* eqn,
   iface::mathml_dom::MathMLCiElement* el,
   std::set<iface::cellml_api::CellMLVariable*>& blackBoxInputs,
   std::set<iface::cellml_api::CellMLVariable*>& blackBoxIO
  );
  void addBlackBoxOutput
  (
   iface::cellml_services::MaLaESTransform* mt,
   iface::cellml_services::CUSES* cuses,
   BBAEquation* eqn,
   iface::mathml_dom::MathMLCiElement* el,
   std::set<iface::cellml_api::CellMLVariable*>& blackBoxOutputs,
   std::set<iface::cellml_api::CellMLVariable*>& blackBoxIO
  );
  void processTraining
  (
   iface::cellml_services::MaLaESTransform* mt,
   iface::cellml_services::CUSES* cuses,
   std::set<iface::cellml_api::CellMLVariable*>& blackBoxIO,
   std::set<iface::cellml_api::CellMLVariable*>& allConstants,
   std::set<iface::cellml_api::CellMLVariable*>& wanted,
   std::list<BBAEquation*>& whiteBoxes,
   std::list<BBAEquation*>& blackBoxes,
   std::wstring& trainingCode,
   std::list<std::wstring>& supplements
  );
  void processRun
  (
   iface::cellml_services::MaLaESTransform* mt,
   iface::cellml_services::CUSES* cuses,
   std::set<iface::cellml_api::CellMLVariable*>& blackBoxInputs,
   std::set<iface::cellml_api::CellMLVariable*>& blackBoxOutputs,
   std::set<iface::cellml_api::CellMLVariable*>& allConstants,
   std::set<iface::cellml_api::CellMLVariable*>& wanted,
   std::list<BBAEquation*>& whiteBoxes,
   std::list<BBAEquation*>& blackBoxes,
   std::wstring& runCode,
   std::list<std::wstring>& supplements
  );
  void assignVariableIndices(std::set<iface::cellml_api::CellMLVariable*>& candidates,
                             std::wstring& comment, uint32_t& count);
  void resetSeen(std::set<iface::cellml_api::CellMLVariable*>& resetSet);
  std::wstring EscapeString(const std::wstring& input);

  std::wstring mMessage;
  std::wstring mImpl;
  ObjRef<iface::cellml_api::Model> mModel;
  ObjRef<iface::cellml_services::CeVAS> mCeVAS;
  ObjRef<iface::cellml_services::AnnotationSet> mAnnoSet;
  // Not owner because mModel already owns them, and we own it.
  std::set<iface::cellml_api::CellMLVariable*> mInputVariableSet;
  std::set<iface::cellml_api::CellMLVariable*> mOutputVariableSet;
  uint32_t mUniqueSolve, mInputIndex, mOutputIndex;
};

class BBABlaBoCBootstrap
  : public iface::blaboc_api::BlaBoCBootstrap
{
public:
  CDA_IMPL_REFCOUNT;
  CDA_IMPL_ID;
  CDA_IMPL_QI1(blaboc_api::BlaBoCBootstrap);

  BBABlaBoCBootstrap()
    : _cda_refcount(1) {}

  iface::blaboc_api::BlaBoC* createBlaBoC(iface::cellml_api::Model* aModel)
    throw()
  {
    return new BBABlaBoC(aModel);
  }

  char* assembleBlaBoC(const wchar_t* aSource, uint32_t* aLen) throw();
};
