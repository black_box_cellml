#include "cda_compiler_support.h"
#ifdef IN_BLABOC_MODULE
#define BLABOC_PUBLIC_PRE CDA_EXPORT_PRE
#define BLABOC_PUBLIC_POST CDA_EXPORT_POST
#else
#define BLABOC_PUBLIC_PRE CDA_IMPORT_PRE
#define BLABOC_PUBLIC_POST CDA_IMPORT_POST
#endif

BLABOC_PUBLIC_PRE
  iface::blaboc_api::BlaBoCBootstrap* createBlaBoCBootstrap()
BLABOC_PUBLIC_POST;
