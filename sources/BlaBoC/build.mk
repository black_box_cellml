lib_LTLIBRARIES += libBlaBoC_API.la

libBlaBoC_API_la_SOURCES = \
  sources/BlaBoC/BlaBoCImpl.cpp \
  sources/BlaBoC/BlaBoCAssembler.cpp

libBlaBoC_API_la_CXXFLAGS = -Wall \
  -I$(top_srcdir) -I$(top_srcdir)/sources -I $(top_srcdir)/sources/BlaBoC \
  -I$(CELLML_API_DIR) -I$(CELLML_API_DIR)/sources -I$(CELLML_API_DIR)/interfaces \
  -I$(CELLML_API_DIR)/CeVAS/sources -I$(CELLML_API_DIR)/AnnoTools/sources \
  -I$(CELLML_API_DIR)/CUSES/sources -I$(CELLML_API_DIR)/MaLaES/sources \
  -I$(top_builddir)/interfaces $(AM_CXXFLAGS)
