#define IN_BLABOC_MODULE
#include "BlaBoCImpl.hpp"
#include "CeVASBootstrap.hpp"
#include "IfaceAnnoTools.hxx"
#include "AnnoToolsBootstrap.hpp"
#include "CUSESBootstrap.hpp"
#include "MaLaESBootstrap.hpp"
#include "BlaBoCBootstrap.hpp"

BBABlaBoC::BBABlaBoC(iface::cellml_api::Model* aModel)
  : _cda_refcount(1), mModel(aModel), mUniqueSolve(0), mInputIndex(0),
    mOutputIndex(0)
{
  RETURN_INTO_OBJREF(cb, iface::cellml_services::CeVASBootstrap,
                     CreateCeVASBootstrap());
  mCeVAS = already_AddRefd<iface::cellml_services::CeVAS>
    (cb->createCeVASForModel(aModel));

  wchar_t* tmp = mCeVAS->modelError();
  mMessage += tmp;
  free(tmp);

  RETURN_INTO_OBJREF(ats, iface::cellml_services::AnnotationToolService,
                     CreateAnnotationToolService());
  mAnnoSet = already_AddRefd<iface::cellml_services::AnnotationSet>
    (ats->createAnnotationSet());
}

BBABlaBoC::~BBABlaBoC()
{
}

void
BBABlaBoC::variableIsInput(iface::cellml_api::CellMLVariable* aVariable)
  throw(std::exception&)
{
  // Make sure the variable is the source...
  RETURN_INTO_OBJREF(cvs, iface::cellml_services::ConnectedVariableSet,
                     mCeVAS->findVariableSet(aVariable));
  if (cvs == NULL)
  {
    mMessage += L"Could not find variable in the model; ";
    throw iface::blaboc_api::BlaBoCException();
  }
  RETURN_INTO_OBJREF(v, iface::cellml_api::CellMLVariable,
                     cvs->sourceVariable());

  if (mInputVariableSet.count(v))
  {
    mMessage += L"Attempt to set a variable as an input twice; ";
    throw iface::blaboc_api::BlaBoCException();
  }

  if (mOutputVariableSet.count(v))
  {
    mMessage += L"Attempt to set an output variable as an input; ";
    throw iface::blaboc_api::BlaBoCException();
  }

  wchar_t buf[30];
  swprintf(buf, 30, L"input %lu", mInputIndex++);
  mAnnoSet->setStringAnnotation(v, L"expression", buf);
  mInputVariableSet.insert(v);
}

void
BBABlaBoC::variableIsOutput(iface::cellml_api::CellMLVariable* aVariable)
  throw(std::exception&)
{
  // Make sure the variable is the source...
  RETURN_INTO_OBJREF(cvs, iface::cellml_services::ConnectedVariableSet,
                     mCeVAS->findVariableSet(aVariable));
  if (cvs == NULL)
  {
    mMessage += L"Could not find variable in the model; ";
    throw iface::blaboc_api::BlaBoCException();
  }
  RETURN_INTO_OBJREF(v, iface::cellml_api::CellMLVariable,
                     cvs->sourceVariable());

  if (mOutputVariableSet.count(v))
  {
    mMessage += L"Attempt to set a variable as an output twice; ";
    throw iface::blaboc_api::BlaBoCException();
  }

  if (mInputVariableSet.count(v))
  {
    mMessage += L"Attempt to set an input variable as an output; ";
    throw iface::blaboc_api::BlaBoCException();
  }

  wchar_t buf[30];
  swprintf(buf, 30, L"output %lu", mOutputIndex++);
  mAnnoSet->setStringAnnotation(v, L"expression", buf);
  mOutputVariableSet.insert(v);
}

void
BBABlaBoC::othersAreOutput()
  throw()
{
  uint32_t l = mCeVAS->length(), i;
  for (i = 0; i < l; i++)
  {
    RETURN_INTO_OBJREF(vs, iface::cellml_services::ConnectedVariableSet,
                       mCeVAS->getVariableSet(i));
    RETURN_INTO_OBJREF(sv, iface::cellml_api::CellMLVariable,
                       vs->sourceVariable());

    if (mInputVariableSet.count(sv) || mOutputVariableSet.count(sv))
      continue;

    mOutputVariableSet.insert(sv);

    wchar_t buf[30];
    swprintf(buf, 30, L"output %lu", mOutputIndex++);
    mAnnoSet->setStringAnnotation(sv, L"expression", buf);
  }
}

void
BBABlaBoC::othersAreInput()
  throw()
{
  uint32_t l = mCeVAS->length(), i;
  for (i = 0; i < l; i++)
  {
    RETURN_INTO_OBJREF(vs, iface::cellml_services::ConnectedVariableSet,
                       mCeVAS->getVariableSet(i));
    RETURN_INTO_OBJREF(sv, iface::cellml_api::CellMLVariable,
                       vs->sourceVariable());

    if (mInputVariableSet.count(sv) || mOutputVariableSet.count(sv))
      continue;

    mInputVariableSet.insert(sv);

    wchar_t buf[30];
    swprintf(buf, 30, L"input %lu", mInputIndex++);
    mAnnoSet->setStringAnnotation(sv, L"expression", buf);
  }
}

void
BBABlaBoC::BuildEquationList
(
 std::list<BBAEquation*>& eqns
)
{
  RETURN_INTO_OBJREF(cci, iface::cellml_api::CellMLComponentIterator,
                     mCeVAS->iterateRelevantComponents());
  while (true)
  {
    RETURN_INTO_OBJREF(cc, iface::cellml_api::CellMLComponent,
                       cci->nextComponent());
    if (cc == NULL)
      break;

    RETURN_INTO_OBJREF(ml, iface::cellml_api::MathList, 
                       cc->math());
    RETURN_INTO_OBJREF(mi, iface::cellml_api::MathMLElementIterator,
                       ml->iterate());
    while (true)
    {
      RETURN_INTO_OBJREF(me, iface::mathml_dom::MathMLElement,
                         mi->next());
      if (me == NULL)
        break;
      DECLARE_QUERY_INTERFACE_OBJREF(mme, me, mathml_dom::MathMLMathElement);
      if (mme == NULL)
      {
        mMessage += L"Found a MathML element not inside a mathml:math element; ";
        throw iface::blaboc_api::BlaBoCException();
      }
      uint32_t l = mme->nArguments(), i;
      for (i = 1; i <= l; i++)
      {
        RETURN_INTO_OBJREF(mae, iface::mathml_dom::MathMLElement,
                           mme->getArgument(i));
        eqns.push_back(new BBAEquation(cc, mae));
      }
    }

    // Whether we need this is dubious, but lets do it right anyway...
    RETURN_INTO_OBJREF(rs, iface::cellml_api::ReactionSet,
                       cc->reactions());
    RETURN_INTO_OBJREF(ri, iface::cellml_api::ReactionIterator,
                       rs->iterateReactions());
    while (true)
    {
      RETURN_INTO_OBJREF(r, iface::cellml_api::Reaction,
                         ri->nextReaction());
      if (r == NULL)
        break;

      RETURN_INTO_OBJREF(vrs, iface::cellml_api::VariableRefSet,
                         r->variableReferences());
      RETURN_INTO_OBJREF(vri, iface::cellml_api::VariableRefIterator,
                         vrs->iterateVariableRefs());
      while (true)
      {
        RETURN_INTO_OBJREF(vr, iface::cellml_api::VariableRef,
                           vri->nextVariableRef());
        if (vr == NULL)
          break;

        RETURN_INTO_OBJREF(rls, iface::cellml_api::RoleSet,
                           vr->roles());
        RETURN_INTO_OBJREF(rli, iface::cellml_api::RoleIterator,
                           rls->iterateRoles());
        while (true)
        {
          RETURN_INTO_OBJREF(rl, iface::cellml_api::Role,
                             rli->nextRole());
          if (rl == NULL)
            break;

          RETURN_INTO_OBJREF(ml, iface::cellml_api::MathList, 
                             rl->math());
          RETURN_INTO_OBJREF(mi, iface::cellml_api::MathMLElementIterator,
                             ml->iterate());
          while (true)
          {
            RETURN_INTO_OBJREF(me, iface::mathml_dom::MathMLElement,
                               mi->next());

            if (me == NULL)
              break;

            DECLARE_QUERY_INTERFACE_OBJREF(mme, me, mathml_dom::MathMLMathElement);
            if (mme == NULL)
            {
              mMessage += L"Found a MathML element not inside a mathml:math element; ";
              throw iface::blaboc_api::BlaBoCException();
            }
            uint32_t l = mme->nArguments(), i;
            for (i = 1; i <= l; i++)
            {
              RETURN_INTO_OBJREF(mae, iface::mathml_dom::MathMLElement,
                                 mme->getArgument(i));
              eqns.push_back(new BBAEquation(cc, mae));
            }
          }
        }
      }
    }
  }
}

bool
BBABlaBoC::IsEquationExternal(iface::mathml_dom::MathMLApplyElement* mae)
{
  if (mae->nArguments() < 3)
    return false;

  RETURN_INTO_OBJREF(arg, iface::mathml_dom::MathMLElement,
                     mae->getArgument(3));
  if (arg == NULL)
    return false;

  DECLARE_QUERY_INTERFACE_OBJREF(mcae, arg, mathml_dom::MathMLApplyElement);
  if (mcae == NULL)
    return false;

  RETURN_INTO_OBJREF(op, iface::mathml_dom::MathMLElement, mcae->_cxx_operator());
  DECLARE_QUERY_INTERFACE_OBJREF(mce, op, mathml_dom::MathMLCsymbolElement);
  return (mce != NULL);
}

void
BBABlaBoC::ClassifyEquations
(
 std::list<BBAEquation*>& eqns,
 std::list<BBAEquation*>& aBlackBoxes,
 std::list<BBAEquation*>& aWhiteBoxes
)
{
  std::list<BBAEquation*>::iterator i;
  for (i = eqns.begin(); i != eqns.end(); i++)
  {
    DECLARE_QUERY_INTERFACE_OBJREF(mae, (*i)->mEquation,
                                   mathml_dom::MathMLApplyElement);
    if (mae == NULL)
    {
      mMessage += L"Found a MathML element inside mathml:math that wasn't "
        L"apply. This is unsupported; ";
      throw iface::blaboc_api::BlaBoCException();
    }

    RETURN_INTO_OBJREF(op, iface::mathml_dom::MathMLElement,
                       mae->_cxx_operator());
    if (op == NULL)
    {
      mMessage += L"Found a MathML apply element with no operator; ";
      throw iface::blaboc_api::BlaBoCException();
    }
    RETURN_INTO_WSTRING(ln, op->localName());
    if (ln != L"eq")
    {
      mMessage += L"Found a MathML apply element in mathml:math which applied "
        L"an operator other than eq. This is unsupported; ";
      throw iface::blaboc_api::BlaBoCException();
    }

    (*i)->add_ref();
    if (IsEquationExternal(mae))
      aBlackBoxes.push_back(*i);
    else
      aWhiteBoxes.push_back(*i);
  }
}

void
BBABlaBoC::getServices
(
 ObjRef<iface::cellml_services::CUSES>& cuses,
 ObjRef<iface::cellml_services::MaLaESTransform>& mt
)
{
  RETURN_INTO_OBJREF(cb, iface::cellml_services::CUSESBootstrap,
                     CreateCUSESBootstrap());
  RETURN_INTO_OBJREF(mb, iface::cellml_services::MaLaESBootstrap,
                     CreateMaLaESBootstrap());
  cuses = already_AddRefd<iface::cellml_services::CUSES>
    (cb->createCUSESForModel(mModel, false));

  mt = already_AddRefd<iface::cellml_services::MaLaESTransform>
    (mb->compileTransformer(
L"opengroup: (\r\n"
L"closegroup: )\r\n"
L"abs: #prec[H]#expr1 fabs\r\n"
L"and: #prec[H]#exprs[ ] I#count and\r\n"
L"arccos: #prec[H]#expr1 acos\r\n"
L"arccosh: #prec[H]#expr1 acosh\r\n"
L"arccot: #prec[H]1.0 #expr1 divide atan\r\n"
L"arccoth: #prec[H]1.0 #expr1 divide atanh\r\n"
L"arccsc: #prec[H]1.0 #expr1 divide asin\r\n"
L"arccsch: #prec[H]1.0 #expr1 divide asinh\r\n"
L"arcsec: #prec[H]1.0 #expr1 divide acos\r\n"
L"arcsech: #prec[H]1.0 #expr1 divide acosh\r\n"
L"arcsin: #prec[H]#expr1 asin\r\n"
L"arcsinh: #prec[H]#expr1 asinh\r\n"
L"arctan: #prec[H]#expr1 atan\r\n"
L"arctanh: #prec[H]#expr1 atanh\r\n"
L"ceiling: #prec[H]#expr1 ceil\r\n"
L"cos: #prec[H]#expr1 cos\r\n"
L"cosh: #prec[H]#expr1 cosh\r\n"
L"cot: #prec[H]1.0 #expr1 tan divide\r\n"
L"coth: #prec[H]1.0 #expr1 tanh divide\r\n"
L"csc: #prec[H]1.0 #expr1 sin divide\r\n"
L"csch: #prec[H]1.0 #expr1 sinh divide\r\n"
L"divide: #prec[H]#expr1 #expr2 divide\r\n"
L"eq: #prec[H]#exprs[ ] I#count equal\r\n"
L"exp: #prec[H]#expr1 exp\r\n"
L"factorial: #prec[H]#expr1 factorial\r\n"
L"factorof: #prec[H]#expr1 #expr2 mod 0.0 I2 equal\r\n"
L"floor: #prec[H]#expr1 floor\r\n"
L"gcd: #prec[H]#exprs[ ] I#count gcd\r\n"
L"geq: #prec[H]#exprs[ ] I#count geq\r\n"
L"gt: #prec[H]#exprs[ ] I#count gt\r\n"
L"implies: #prec[H]#expr1 not #expr2 I2 or\r\n"
L"int: #prec[H] funcptr func#unique1 varptr #bvarIndex integrate #supplement def func#unique1 #expr1 return1 end\r\n"
L"lcm: #prec[H]#exprs[ ] I#count lcm\r\n"
L"leq: #prec[H]#exprs[ ] I#count leq\r\n"
L"ln: #prec[H]#expr1 log\r\n"
L"log: #prec[H]#expr1 log #logbase log divide\r\n"
L"lt: #prec[H]#exprs[ ] I#count lt\r\n"
L"max: #prec[H]#exprs[ ] I#count max\r\n"
L"min: #prec[H]#exprs[ ] I#count min\r\n"
L"minus: #prec[H]#expr1 #expr2 minus\r\n"
L"neq: #prec[H]#expr1 #expr2 neq\r\n"
L"not: #prec[H]#expr1 not\r\n"
L"or: #prec[H]#exprs[ ] I#count or\r\n"
L"plus: #prec[H]#exprs[ ] I#count plus\r\n"
L"power: #prec[H]#expr1 #expr2 pow\r\n"
L"quotient: #prec[H]#expr1 #expr2 quot\r\n"
L"rem: #prec[H]#expr1 #expr2 mod\r\n"
L"root: #prec[H]#expr1 1.0 #degree divide pow\r\n"
L"sec: #prec[H]1.0 #expr1 cos divide\r\n"
L"sech: #prec[H]1.0 #expr1 cosh divide\r\n"
L"sin: #prec[H]#expr1 sin\r\n"
L"sinh: #prec[H]#expr1 sinh\r\n"
L"tan: #prec[H]#expr1 tan\r\n"
L"tanh: #prec[H]#expr1 tanh\r\n"
L"times: #prec[H]#exprs[ ] I#count times\r\n"
L"unary_minus: #prec[H]#expr1 uminus\r\n"
L"units_conversion: #prec[H]#expr1 #expr2 I2 times #expr3 I2 plus\r\n"
L"units_conversion_offset: #prec[H]#expr1 #expr2 I2 plus\r\n"
L"units_conversion_factor: #prec[H]#expr1 #expr2 I2 times\r\n"
L"xor: #prec[H]#expr1 #expr2 xor\r\n"
L"http://www.bioeng.auckland.ac.nz/people/miller/black_box: blackbox#exprs{}\r\n"
                     ));
}

void
BBABlaBoC::determineEquationVariables
(
 iface::cellml_services::CUSES* cuses,
 iface::cellml_services::MaLaESTransform * mt,
 std::list<BBAEquation*>& eqns
)
{
  std::list<BBAEquation*>::iterator ei;
  for (ei = eqns.begin(); ei != eqns.end(); ei++)
  {
    RETURN_INTO_OBJREF(mr, iface::cellml_services::MaLaESResult,
                       mt->transform(mCeVAS, cuses, mAnnoSet, (*ei)->mEquation,
                                     (*ei)->mContext, NULL, NULL, 0));
    RETURN_INTO_WSTRING(mce, mr->compileErrors());
    if (mce != L"")
    {
      mMessage += mce;
      throw iface::blaboc_api::BlaBoCException();
    }

    RETURN_INTO_OBJREF(cvi, iface::cellml_api::CellMLVariableIterator,
                       mr->iterateInvolvedVariables());
    while (true)
    {
      RETURN_INTO_OBJREF(cv, iface::cellml_api::CellMLVariable,
                         cvi->nextVariable());
      if (cv == NULL)
        break;

      (*ei)->mSourceVariables.push_back(cv);
    }
  }
}

void
BBABlaBoC::determineEvaluationDAG
(
 std::set<iface::cellml_api::CellMLVariable*>& know,
 std::set<iface::cellml_api::CellMLVariable*>& want,
 std::list<BBAEquation*>& equations,
 std::multimap<iface::cellml_api::CellMLVariable*,
 iface::cellml_api::CellMLVariable*>& forwardEdges,
 std::multimap<iface::cellml_api::CellMLVariable*,
 iface::cellml_api::CellMLVariable*>& backEdges
)
{
  bool progress;
  do
  {
    progress = false;

    std::list<BBAEquation*>::iterator ei;
    for (ei = equations.begin(); ei != equations.end(); ei++)
    {
      iface::cellml_api::CellMLVariable* unknown = NULL;
      std::list<iface::cellml_api::CellMLVariable*>::iterator svi;
      bool success = true;
      for (svi = (*ei)->mSourceVariables.begin();
           svi != (*ei)->mSourceVariables.end();
           svi++)
      {
        if (know.count(*svi))
          continue;
        if (want.count(*svi) == 0)
        {
          success = false;
          break;
        }
        if (unknown && CDA_objcmp(*svi, unknown) != 0)
        {
          success = false;
          break;
        }

        if (unknown == NULL)
          unknown = *svi;
      }
      if (!success)
        continue;

      if (!unknown)
      {
        // mMessage += L"Warning: Found a useless (extraneous) equation; ";
        continue;
      }

      progress = true;
      
      want.erase(unknown);
      know.insert(unknown);

      for (svi = (*ei)->mSourceVariables.begin();
           svi != (*ei)->mSourceVariables.end();
           svi++)
      {
        if (CDA_objcmp(*svi, unknown) == 0)
          continue;
        forwardEdges.insert(std::pair<iface::cellml_api::CellMLVariable*,
                                      iface::cellml_api::CellMLVariable*>
                            (*svi, unknown));
        backEdges.insert(std::pair<iface::cellml_api::CellMLVariable*,
                                   iface::cellml_api::CellMLVariable*>
                         (unknown, *svi));
      }

      mAnnoSet->setObjectAnnotation(unknown, L"computeWith", *ei);

      //std::list<BBAEquation*>::iterator eid;
      //eid = ei;
      //ei--;
      //(*eid)->release_ref();
      //equations.erase(eid);
    }
  }
  while (progress);
}

void
BBABlaBoC::processConstants
(
 iface::cellml_services::MaLaESTransform* mt,
 iface::cellml_services::CUSES* cuses,
 std::set<iface::cellml_api::CellMLVariable*>& allSourceVariables,
 std::set<iface::cellml_api::CellMLVariable*>& directConstants,
 std::set<iface::cellml_api::CellMLVariable*>& allConstants,
 std::set<iface::cellml_api::CellMLVariable*>& wanted,
 std::list<BBAEquation*>& whiteBoxes,
 std::wstring& aComment,
 std::wstring& aCode,
 std::list<std::wstring>& aSupplementary
)
{
  uint32_t l = mCeVAS->length(), i;

  for (i = 0; i < l; i++)
  {
    RETURN_INTO_OBJREF(vs, iface::cellml_services::ConnectedVariableSet,
                       mCeVAS->getVariableSet(i));
    if (vs == NULL)
      break;

    RETURN_INTO_OBJREF(sv, iface::cellml_api::CellMLVariable,
                       vs->sourceVariable());

    allSourceVariables.insert(sv);
    RETURN_INTO_WSTRING(iv, sv->initialValue());

    // Inputs and outputs can't be constants. We could make this an error, but
    // it is probably more useful to just override them.
    wchar_t* type = NULL;
    if (mInputVariableSet.count(sv) != 0)
      type = L"input";

    if (mOutputVariableSet.count(sv) != 0)
      type = L"output";

    if (type != NULL)
    {
      aComment += L"# Variable ";
      RETURN_INTO_WSTRING(n, sv->name());
      aComment += n;
      aComment += L" in component ";
      RETURN_INTO_WSTRING(cn, sv->componentName());
      aComment += cn;
      aComment += L" is an ";
      aComment += type;
      aComment += L" variable (";
      RETURN_INTO_WSTRING(expr, mAnnoSet->getStringAnnotation(sv,
                                                              L"expression"));
      aComment += expr;
      aComment += L").\r\n";
      continue;
    }
    
    if (iv != L"")
    {
      directConstants.insert(sv);
      allConstants.insert(sv);
    }
  }

  std::set<iface::cellml_api::CellMLVariable*> unwantedt, unwanted;
  std::set_union(mInputVariableSet.begin(), mInputVariableSet.end(),
                 allConstants.begin(), allConstants.end(),
                 std::inserter(unwantedt, unwantedt.begin()));
  std::set_union(mOutputVariableSet.begin(), mOutputVariableSet.end(),
                 unwantedt.begin(), unwantedt.end(),
                 std::inserter(unwanted, unwanted.begin()));
  std::set_difference(allSourceVariables.begin(), allSourceVariables.end(),
                      unwanted.begin(), unwanted.end(),
                      std::inserter(wanted, wanted.begin()));

  std::multimap<iface::cellml_api::CellMLVariable*,
                iface::cellml_api::CellMLVariable*> forwardEdges, backEdges;
  determineEvaluationDAG(allConstants, wanted, whiteBoxes, forwardEdges,
                         backEdges);

  // Now assign constants identifiers...
  uint32_t nextIndex = 0;

  aCode += L"def initConstants\r\n";

  std::set<iface::cellml_api::CellMLVariable*>::iterator vi;
  for (vi = allConstants.begin(); vi != allConstants.end(); vi++)
  {
    wchar_t buf[30];

    swprintf(buf, 30, L"constant %lu", nextIndex++);
    aComment += L"# ";
    aComment += buf;
    aComment += L": Constant ";
    RETURN_INTO_WSTRING(cn, (*vi)->name());
    aComment += cn;
    aComment += L" in component ";
    RETURN_INTO_WSTRING(ccn, (*vi)->componentName());
    aComment += ccn;
    aComment += L"\r\n";
    mAnnoSet->setStringAnnotation(*vi, L"expression", buf);

    RETURN_INTO_WSTRING(iv, (*vi)->initialValue());
    if (iv != L"")
    {
      aCode += iv;
      aCode += L" varptr ";
      aCode += buf;
      aCode += L" mov\r\n";
    }
  }

  dfsComputeSet(mt, cuses, allConstants, backEdges, aCode, aSupplementary);

  aCode += L"return\r\nend\r\n";
}

void
BBABlaBoC::dfsComputeSet
(
 iface::cellml_services::MaLaESTransform* mt,
 iface::cellml_services::CUSES* cuses,
 std::set<iface::cellml_api::CellMLVariable*>& targets,
 std::multimap<iface::cellml_api::CellMLVariable*,
               iface::cellml_api::CellMLVariable*>& backEdges,
 std::wstring& aCode,
 std::list<std::wstring>& aSupplementary
)
{
  std::set<iface::cellml_api::CellMLVariable*>::iterator i;
  for (i = targets.begin(); i != targets.end(); i++)
  {
    dfsComputeVariable(mt, cuses, *i, backEdges, aCode, aSupplementary);
  }
}


void
BBABlaBoC::WriteDirectAssignment
(
 std::wstring& aCode,
 std::list<std::wstring>& aSupplementary,
 iface::cellml_services::MaLaESTransform* mt,
 iface::cellml_services::CUSES* cuses,
 iface::cellml_api::CellMLVariable* var,
 iface::mathml_dom::MathMLElement* lhs,
 iface::mathml_dom::MathMLElement* rhs
)
{
  DECLARE_QUERY_INTERFACE_OBJREF(ci, lhs, mathml_dom::MathMLCiElement);
  RETURN_INTO_OBJREF(n, iface::dom::Node, ci->getArgument(1));
  DECLARE_QUERY_INTERFACE_OBJREF(t, n, dom::Text);
  RETURN_INTO_WSTRING(txt, t->data());
  int i = 0, j = txt.length() - 1;
  wchar_t c;
  while ((c = txt[i]) == ' ' || c == '\t' || c == '\r' || c == '\n')
    i++;
  while ((c = txt[j]) == ' ' || c == '\t' || c == '\r' || c == '\n')
    j--;
  if (j < i)
  {
    mMessage += L"Invalid CI with no variable name; ";
    return;
  }

  txt = txt.substr(i, j - i + 1);

  RETURN_INTO_OBJREF(compn, iface::cellml_api::CellMLElement,
                     var->parentElement());
  DECLARE_QUERY_INTERFACE_OBJREF(comp, compn, cellml_api::CellMLComponent);
  RETURN_INTO_OBJREF(vs, iface::cellml_api::CellMLVariableSet,
                     comp->variables());
  RETURN_INTO_OBJREF(v, iface::cellml_api::CellMLVariable,
                     vs->getVariable(txt.c_str()));
  RETURN_INTO_OBJREF(rhst, iface::cellml_services::MaLaESResult,
                     mt->transform(mCeVAS, cuses, mAnnoSet, rhs, comp, v, NULL, 0));
  RETURN_INTO_WSTRING(expr, rhst->expression());

  RETURN_INTO_WSTRING(lhsv, mAnnoSet->getStringAnnotation(var, L"expression"));
  aCode += expr;
  aCode += L" varptr ";
  aCode += lhsv + L" mov\r\n";

  uint32_t l = rhst->supplementariesLength();
  uint32_t si;
  for (si = 0; si < l; si++)
  {
    RETURN_INTO_WSTRING(sup, rhst->getSupplementary(si));
    aSupplementary.push_back(sup);
  }
}

void
BBABlaBoC::resetSeen
(
 std::set<iface::cellml_api::CellMLVariable*>& resetSet
)
{
  std::set<iface::cellml_api::CellMLVariable*>::iterator i;
  for (i = resetSet.begin(); i != resetSet.end(); i++)
  {
    mAnnoSet->setStringAnnotation(*i, L"seen", L"");
  }
}

void
BBABlaBoC::dfsComputeVariable
(
 iface::cellml_services::MaLaESTransform* mt,
 iface::cellml_services::CUSES* cuses,
 iface::cellml_api::CellMLVariable* var,
 std::multimap<iface::cellml_api::CellMLVariable*,
               iface::cellml_api::CellMLVariable*>& backEdges,
 std::wstring& aCode,
 std::list<std::wstring>& aSupplementary
)
{
  RETURN_INTO_WSTRING(seen, mAnnoSet->getStringAnnotation(var, L"seen"));
  if (seen == L"true")
    return;
  mAnnoSet->setStringAnnotation(var, L"seen", L"true");

  std::multimap<iface::cellml_api::CellMLVariable*,
    iface::cellml_api::CellMLVariable*>::iterator bei =
    backEdges.find(var);
  for (; bei != backEdges.end(); bei++)
  {
    dfsComputeVariable(mt, cuses, (*bei).second, backEdges, aCode, aSupplementary);
  }

  RETURN_INTO_OBJREF(eqo, iface::XPCOM::IObject, 
                     mAnnoSet->getObjectAnnotation(var, L"computeWith"));
  BBAEquation* eqn = dynamic_cast<BBAEquation*>(eqo.getPointer());
  
  if (eqn == NULL)
  {
    return;
  }

  DECLARE_QUERY_INTERFACE_OBJREF(apply, eqn->mEquation,
                                 mathml_dom::MathMLApplyElement);
  if (apply->nArguments() < 3)
  {
    mMessage += L"Invalid MathML <apply><eq/> with only one child; ";
    return;
  }

  RETURN_INTO_OBJREF(lhs, iface::mathml_dom::MathMLElement,
                     apply->getArgument(2));
  RETURN_INTO_OBJREF(rhs, iface::mathml_dom::MathMLElement,
                     apply->getArgument(3));

  RETURN_INTO_OBJREF(mrlhs, iface::cellml_services::MaLaESResult,
                     mt->transform(mCeVAS, cuses, mAnnoSet, lhs,
                                   eqn->mContext, NULL, NULL, 0));

  RETURN_INTO_WSTRING(lhsmce, mrlhs->compileErrors());
  if (lhsmce != L"")
  {
    mMessage += lhsmce;
    throw iface::blaboc_api::BlaBoCException();
  }

  RETURN_INTO_OBJREF(mrrhs, iface::cellml_services::MaLaESResult,
                     mt->transform(mCeVAS, cuses, mAnnoSet, rhs,
                                   eqn->mContext, NULL, NULL, 0));

  RETURN_INTO_WSTRING(rhsmce, mrrhs->compileErrors());
  if (rhsmce != L"")
  {
    mMessage += rhsmce;
    throw iface::blaboc_api::BlaBoCException();
  }

  DECLARE_QUERY_INTERFACE_OBJREF(lhsci, lhs, mathml_dom::MathMLCiElement);
  if (lhsci != NULL)
  {
    RETURN_INTO_OBJREF(cvi, iface::cellml_api::CellMLVariableIterator,
                       mrrhs->iterateInvolvedVariables());
    bool rhsInvolvesVar = false;
    while (true)
    {
      RETURN_INTO_OBJREF(v, iface::cellml_api::CellMLVariable,
                         cvi->nextVariable());
      if (v == NULL)
        break;
      if (CDA_objcmp(v, var) == 0)
      {
        rhsInvolvesVar = true;
        break;
      }
    }

    if (!rhsInvolvesVar)
    {
      WriteDirectAssignment(aCode, aSupplementary, mt, cuses, var, lhs, rhs);
      return;
    }
  }
  DECLARE_QUERY_INTERFACE_OBJREF(rhsci, rhs, mathml_dom::MathMLCiElement);
  if (rhsci != NULL)
  {
    RETURN_INTO_OBJREF(cvi, iface::cellml_api::CellMLVariableIterator,
                       mrlhs->iterateInvolvedVariables());
    bool lhsInvolvesVar = false;
    while (true)
    {
      RETURN_INTO_OBJREF(v, iface::cellml_api::CellMLVariable,
                         cvi->nextVariable());
      if (v == NULL)
        break;
      if (CDA_objcmp(v, var) == 0)
      {
        lhsInvolvesVar = true;
        break;
      }
    }

    if (!lhsInvolvesVar)
    {
      WriteDirectAssignment(aCode, aSupplementary, mt, cuses, var, rhs, lhs);
      return;
    }
  }

  // No luck trying to do a direct assignment. Need to solve. It would be good
  // to support various types of algebraic manipulations, but failing that, we
  // just do a Newton-Raphson solve.

  uint32_t l = mrlhs->supplementariesLength();
  uint32_t i;
  for (i = 0; i < l; i++)
  {
    RETURN_INTO_WSTRING(ss, mrlhs->getSupplementary(i));
    aSupplementary.push_back(ss);
  }
  l = mrrhs->supplementariesLength();
  for (i = 0; i < l; i++)
  {
    RETURN_INTO_WSTRING(ss, mrrhs->getSupplementary(i));
    aSupplementary.push_back(ss);
  }

  std::wstring val;
  uint32_t solveId = mUniqueSolve++;
  val += L"def minfunc_";
  wchar_t buf[30];
  swprintf(buf, 30, L"%lu", solveId);
  val += buf;
  val += L"\r\n";
  RETURN_INTO_WSTRING(srhs, mrrhs->expression());
  val += srhs;
  val += L" ";
  RETURN_INTO_WSTRING(slhs, mrlhs->expression());
  val += slhs;
  val += L" minus fabs return1\r\n";
  val += L"end\r\n";
  aSupplementary.push_back(val);

  aCode += L"funcptr minfunc_";
  aCode += buf;
  aCode += L" varptr ";
  RETURN_INTO_WSTRING(ve, mAnnoSet->getStringAnnotation(var, L"expression"));
  aCode += ve;
  aCode += L" minimise\r\n";
}

void
BBABlaBoC::assignVariableIndices
(
 std::set<iface::cellml_api::CellMLVariable*>& candidates,
 std::wstring& comment,
 uint32_t& count
)
{
  std::set<iface::cellml_api::CellMLVariable*>::iterator i;
  uint32_t nextIdx = 0;
  for (i = candidates.begin(); i != candidates.end(); i++)
  {
    RETURN_INTO_WSTRING(expr, mAnnoSet->getStringAnnotation(*i, L"expression"));
    if (expr == L"")
      continue;

    wchar_t buf[30];
    swprintf(buf, 30, L"variable %lu", nextIdx++);
    mAnnoSet->setStringAnnotation(*i, L"expression", buf);

    comment += L"# ";
    comment += buf;
    comment += L" is variable ";
    RETURN_INTO_WSTRING(n, (*i)->name());
    comment += n;
    comment += L" in component ";
    RETURN_INTO_WSTRING(cn, (*i)->componentName());
    comment += cn;
    comment += L"\r\n";
  }

  count = nextIdx;
}

void
BBABlaBoC::determineBlackBoxIO
(
 iface::cellml_services::MaLaESTransform* mt,
 iface::cellml_services::CUSES* cuses,
 std::list<BBAEquation*>& blackBoxes,
 std::set<iface::cellml_api::CellMLVariable*>& blackBoxInputs,
 std::set<iface::cellml_api::CellMLVariable*>& blackBoxOutputs,
 std::set<iface::cellml_api::CellMLVariable*>& blackBoxIO,
 uint32_t& tmpInMax,
 uint32_t& tmpOutMax,
 std::wstring& blackBoxCode
)
{
  tmpInMax = 1;
  tmpOutMax = 1;
  std::list<BBAEquation*>::iterator i;

  blackBoxCode += L"def initBlackBoxes\r\n";

  uint32_t bbIdx = 0;
  for (i = blackBoxes.begin(); i != blackBoxes.end(); i++)
  {
    DECLARE_QUERY_INTERFACE_OBJREF(apply, (*i)->mEquation,
                                   mathml_dom::MathMLApplyElement);
    RETURN_INTO_OBJREF(lhs, iface::mathml_dom::MathMLElement,
                       apply->getArgument(2));
    RETURN_INTO_OBJREF(rhs, iface::mathml_dom::MathMLElement,
                       apply->getArgument(3));
    DECLARE_QUERY_INTERFACE_OBJREF(rhsA, rhs, mathml_dom::MathMLApplyElement);
    RETURN_INTO_OBJREF(op, iface::mathml_dom::MathMLElement,
                       rhsA->_cxx_operator());
    DECLARE_QUERY_INTERFACE_OBJREF(csym, op, mathml_dom::MathMLCsymbolElement);
    
    DECLARE_QUERY_INTERFACE_OBJREF(vec, lhs, mathml_dom::MathMLVectorElement);
    uint32_t outCount = 0;
    if (vec != NULL)
    {
      uint32_t j, l = vec->ncomponents();
      for (j = 1; j <= l; j++)
      {
        RETURN_INTO_OBJREF(mce, iface::mathml_dom::MathMLContentElement,
                           vec->getComponent(j));
        DECLARE_QUERY_INTERFACE_OBJREF(ci, mce, mathml_dom::MathMLCiElement);
        if (ci == NULL)
        {
          mMessage += L"Found a black-box with something other than ci inside "
            L"vector on the left hand side; ";
          return;
        }
        addBlackBoxOutput(mt, cuses, (*i), ci, blackBoxOutputs, blackBoxIO);
        outCount++;
      }
      if (tmpOutMax < outCount)
        tmpOutMax = outCount;
    }
    else
    {
      outCount = 1;
      DECLARE_QUERY_INTERFACE_OBJREF(ci, lhs, mathml_dom::MathMLCiElement);
      if (ci != NULL)
        addBlackBoxOutput(mt, cuses, (*i), ci, blackBoxOutputs, blackBoxIO);
      else
      {
        mMessage += L"Found a black-box with something other than vector or "
          L"ci on the left hand side; ";
        return;
      }
    }

    // Now time to add the outputs...
    DECLARE_QUERY_INTERFACE_OBJREF(arhs, rhs,
                                   mathml_dom::MathMLApplyElement);
    uint32_t j, l = arhs->nArguments();
    if (tmpInMax < l - 1)
        tmpInMax = l - 1;
    for (j = 2; j <= l; j++)
    {
      RETURN_INTO_OBJREF(el, iface::mathml_dom::MathMLElement,
                         arhs->getArgument(j));
      DECLARE_QUERY_INTERFACE_OBJREF(ci, el, mathml_dom::MathMLCiElement);
      if (ci == NULL)
      {
        mMessage += L"Found a black-box with an input that isn't a ci; ";
        return;
      }

      addBlackBoxInput(mt, cuses, *i, ci, blackBoxInputs, blackBoxIO);
    }

    blackBoxCode += L"stringconstant \"";
    RETURN_INTO_WSTRING(du, csym->definitionURL());
    blackBoxCode += EscapeString(du);
    blackBoxCode += L"\" ";
    wchar_t buf[30];
    swprintf(buf, 30, L"I%lu", l - 1);
    blackBoxCode += buf;
    blackBoxCode += L" ";
    swprintf(buf, 30, L"I%lu", outCount);
    blackBoxCode += buf;
    blackBoxCode += L" ";
    swprintf(buf, 30, L"I%lu", bbIdx++);
    blackBoxCode += buf;
    blackBoxCode += L" createBlackBox\r\n";
  }

  blackBoxCode += L"return\r\nend\r\n";
}

std::wstring
BBABlaBoC::EscapeString(const std::wstring& input)
{
  std::wstring output;

  size_t i, l = input.size();
  for (i = 0; i < l; i++)
  {
    wchar_t c = input[i];
    if (c == L'\\')
      output += L"\\\\";
    else if (c == L'\n')
      output += L"\\n";
    else if (c == L'\r')
      output += L"\\r";
    else if (c == L'"')
      output += L"\\\"";
    else
      output += c;
  }

  return output;
}

void
BBABlaBoC::addBlackBoxInput
(
 iface::cellml_services::MaLaESTransform* mt,
 iface::cellml_services::CUSES* cuses,
 BBAEquation* eqn,
 iface::mathml_dom::MathMLCiElement* el,
 std::set<iface::cellml_api::CellMLVariable*>& blackBoxInputs,
 std::set<iface::cellml_api::CellMLVariable*>& blackBoxIO
)
{
  RETURN_INTO_OBJREF(mr, iface::cellml_services::MaLaESResult,
                     mt->transform(mCeVAS, cuses, mAnnoSet, el,
                                   eqn->mContext, NULL, NULL, 0));
  RETURN_INTO_OBJREF(cvi, iface::cellml_api::CellMLVariableIterator,
                     mr->iterateInvolvedVariables());
  RETURN_INTO_OBJREF(v, iface::cellml_api::CellMLVariable,
                     cvi->nextVariable());
  
  eqn->mSourceVariables.push_back(v);

  if (blackBoxInputs.count(v) == 0)
    blackBoxInputs.insert(v);

  if (blackBoxIO.count(v) == 0)
    blackBoxIO.insert(v);
}

void
BBABlaBoC::addBlackBoxOutput
(
 iface::cellml_services::MaLaESTransform* mt,
 iface::cellml_services::CUSES* cuses,
 BBAEquation* eqn,
 iface::mathml_dom::MathMLCiElement* el,
 std::set<iface::cellml_api::CellMLVariable*>& blackBoxOutputs,
 std::set<iface::cellml_api::CellMLVariable*>& blackBoxIO
)
{
  RETURN_INTO_OBJREF(mr, iface::cellml_services::MaLaESResult,
                     mt->transform(mCeVAS, cuses, mAnnoSet, el,
                                   eqn->mContext, NULL, NULL, 0));
  RETURN_INTO_OBJREF(cvi, iface::cellml_api::CellMLVariableIterator,
                     mr->iterateInvolvedVariables());
  RETURN_INTO_OBJREF(v, iface::cellml_api::CellMLVariable,
                     cvi->nextVariable());
  
  eqn->mOutputs.push_back(v);

  if (blackBoxOutputs.count(v) == 0)
    blackBoxOutputs.insert(v);

  if (blackBoxIO.count(v) == 0)
    blackBoxIO.insert(v);
}

void
BBABlaBoC::processTraining
(
 iface::cellml_services::MaLaESTransform* mt,
 iface::cellml_services::CUSES* cuses,
 std::set<iface::cellml_api::CellMLVariable*>& blackBoxIO,
 std::set<iface::cellml_api::CellMLVariable*>& allConstants,
 std::set<iface::cellml_api::CellMLVariable*>& awanted,
 std::list<BBAEquation*>& whiteBoxes,
 std::list<BBAEquation*>& blackBoxes,
 std::wstring& trainingCode,
 std::list<std::wstring>& supplements
)
{
  std::set<iface::cellml_api::CellMLVariable*> wanted1, wanted;

  std::set_difference(awanted.begin(), awanted.end(),
                      mInputVariableSet.begin(), mInputVariableSet.end(),
                      std::inserter(wanted1, wanted1.end()));
  std::set_difference(wanted1.begin(), wanted1.end(),
                      mOutputVariableSet.begin(), mOutputVariableSet.end(),
                      std::inserter(wanted, wanted.end()));

  std::multimap<iface::cellml_api::CellMLVariable*,
    iface::cellml_api::CellMLVariable*>
    forwardEdges, backEdges;

  std::set<iface::cellml_api::CellMLVariable*> knowt, know;
  std::set_union(allConstants.begin(), allConstants.end(),
                 mInputVariableSet.begin(), mInputVariableSet.end(),
                 std::inserter(knowt, knowt.begin()));
  std::set_union(knowt.begin(), knowt.end(),
                 mOutputVariableSet.begin(), mOutputVariableSet.end(),
                 std::inserter(know, know.begin()));
  determineEvaluationDAG(know, wanted, whiteBoxes, forwardEdges, backEdges);

  if (wanted.size() != 0)
  {
    bool noProblem = true;
    std::set<iface::cellml_api::CellMLVariable*>::iterator i;
    for (i = wanted.begin(); i != wanted.end(); i++)
    {
      if (blackBoxIO.count(*i) == 0)
        continue;

      noProblem = false;
      mMessage += L"Black box input/output variable ";
      RETURN_INTO_WSTRING(n, (*i)->name());
      RETURN_INTO_WSTRING(cn, (*i)->componentName());
      mMessage += n;
      mMessage += L" in component ";
      mMessage += cn;
      mMessage += L" could not be computed by any equation; ";
    }
    if (!noProblem)
      return;
  }

  std::set<iface::cellml_api::CellMLVariable*> compute;
  std::set_difference(blackBoxIO.begin(), blackBoxIO.end(),
                      know.begin(), know.end(),
                      std::inserter(compute, compute.begin()));

  trainingCode += L"def train\r\n";
  dfsComputeSet(mt, cuses, compute, backEdges, trainingCode, supplements);
  std::list<BBAEquation*>::iterator i;
  uint32_t bbidx = 0;
  for (i = blackBoxes.begin(); i != blackBoxes.end(); i++)
  {
    std::list<iface::cellml_api::CellMLVariable*>::iterator j
      ((*i)->mSourceVariables.begin());
    uint32_t inpIdx = 0;
    for (; j != (*i)->mSourceVariables.end(); j++)
    {
      wchar_t buf[30];
      swprintf(buf, 30, L"%lu", inpIdx++);
      RETURN_INTO_WSTRING(expr, mAnnoSet->getStringAnnotation((*j),
                                                              L"expression"));
      trainingCode += expr;
      trainingCode += L" varptr tmpinputs ";
      trainingCode += buf;
      trainingCode += L" mov\r\n";
    }
    inpIdx = 0;
    for (j = (*i)->mOutputs.begin(); j != (*i)->mOutputs.end(); j++)
    {
      wchar_t buf[30];
      swprintf(buf, 30, L"%lu", inpIdx++);
      RETURN_INTO_WSTRING(expr, mAnnoSet->getStringAnnotation((*j),
                                                              L"expression"));
      trainingCode += expr;
      trainingCode += L" varptr tmpoutputs ";
      trainingCode += buf;
      trainingCode += L" mov\r\n";
    }
    wchar_t buf[30];
    swprintf(buf, 30, L"I%lu", bbidx++);
    trainingCode += buf;
    trainingCode += L" calltrain\r\n";
  }
  trainingCode += L"return\r\nend\r\n";
}

void
BBABlaBoC::processRun
(
 iface::cellml_services::MaLaESTransform* mt,
 iface::cellml_services::CUSES* cuses,
 std::set<iface::cellml_api::CellMLVariable*>& blackBoxInputs,
 std::set<iface::cellml_api::CellMLVariable*>& blackBoxOutputs,
 std::set<iface::cellml_api::CellMLVariable*>& allConstants,
 std::set<iface::cellml_api::CellMLVariable*>& wanted,
 std::list<BBAEquation*>& whiteBoxes,
 std::list<BBAEquation*>& blackBoxes,
 std::wstring& runCode,
 std::list<std::wstring>& supplements
)
{
  std::multimap<iface::cellml_api::CellMLVariable*,
    iface::cellml_api::CellMLVariable*>
    forwardEdges, backEdges;

  std::set<iface::cellml_api::CellMLVariable*> know;
  std::set_union(allConstants.begin(), allConstants.end(),
                 mInputVariableSet.begin(), mInputVariableSet.end(),
                 std::inserter(know, know.begin()));
  std::set<iface::cellml_api::CellMLVariable*> want;
  std::set_difference(wanted.begin(), wanted.end(),
                      know.begin(), know.end(),
                      std::inserter(want, want.begin()));
  determineEvaluationDAG(know, want, whiteBoxes, forwardEdges, backEdges);

  runCode += L"def run\r\n";
  std::set<iface::cellml_api::CellMLVariable*> compute;
  std::set_intersection(blackBoxInputs.begin(), blackBoxInputs.end(),
                        know.begin(), know.end(),
                        std::inserter(compute, compute.begin()));
  dfsComputeSet(mt, cuses, compute, backEdges, runCode, supplements);
  
  std::list<BBAEquation*>::iterator i;
  uint32_t bbIdx = 0;
  for (i = blackBoxes.begin(); i != blackBoxes.end(); i++)
  {
    std::list<iface::cellml_api::CellMLVariable*>::iterator j;
    uint32_t vidx = 0;
    for (j = (*i)->mSourceVariables.begin(); j != (*i)->mSourceVariables.end();
         j++)
    {
      RETURN_INTO_WSTRING(seen, mAnnoSet->getStringAnnotation(*j, L"seen"));
      RETURN_INTO_WSTRING(expr, mAnnoSet->getStringAnnotation(*j, L"expression"));
      if (seen == L"true")
      {
        runCode += expr;
        runCode += L" varptr tmpinputs ";
        wchar_t buf[30];
        swprintf(buf, 30, L"%lu", vidx++);
        runCode += buf;
        runCode += L" mov\r\n";
        continue;
      }
      wchar_t buf[30];
      swprintf(buf, 30, L"I%lu", bbIdx);
      runCode += buf;
      runCode += L" ";
      swprintf(buf, 30, L"I%lu", vidx);
      runCode += buf;
      runCode += L" getaverageinput varptr ";
      runCode += expr;
      runCode += L" mov\r\n";
      runCode += expr;
      runCode += L" varptr tmpinputs ";
      swprintf(buf, 30, L"%lu", vidx++);
      runCode += buf;
      runCode += L" mov\r\n";
    }
    wchar_t buf[30];
    swprintf(buf, 30, L"I%lu", bbIdx);
    runCode += buf;
    runCode += L" callrun\r\n";
    vidx = 0;
    for (j = (*i)->mOutputs.begin(); j != (*i)->mOutputs.end();
         j++)
    {
      if (know.count(*j) != 0)
      {
        vidx++;
        continue;
      }

      RETURN_INTO_WSTRING(expr, mAnnoSet->getStringAnnotation(*j, L"expression"));
      runCode += L"tmpoutputs ";
      swprintf(buf, 30, L"%lu", vidx++);
      runCode += buf;
      runCode += L" varptr ";
      runCode += expr;
      runCode += L" mov\r\n";
    }
    bbIdx++;
  }

  std::set<iface::cellml_api::CellMLVariable*>::iterator it;

  // Update the black box inputs...
  std::set<iface::cellml_api::CellMLVariable*> know_upd;
  std::set_union(know.begin(), know.end(),
                 blackBoxOutputs.begin(), blackBoxOutputs.end(),
                 std::inserter(know_upd, know_upd.begin()));
  std::set<iface::cellml_api::CellMLVariable*> want_upd;
  std::set_difference(wanted.begin(), wanted.end(),
                      blackBoxOutputs.begin(), blackBoxOutputs.end(),
                      std::inserter(want_upd, want_upd.begin()));
  forwardEdges.clear();
  backEdges.clear();

  determineEvaluationDAG(know_upd, want_upd, whiteBoxes, forwardEdges, backEdges);
  dfsComputeSet(mt, cuses, blackBoxInputs, backEdges, runCode, supplements);
  
  runCode += L"resetConvergenceCounter\r\n"
    L"I0 setjmp\r\n"
    L"convergenceSavePoint\r\n";
  
  // Now just a straight copy-run-save call into the black boxes, followed by
  // a recompute of the inputs...

  bbIdx = 0;
  for (i = blackBoxes.begin(); i != blackBoxes.end(); i++)
  {
    uint32_t vidx = 0;
    std::list<iface::cellml_api::CellMLVariable*>::iterator j;
    for (j = (*i)->mSourceVariables.begin(); j != (*i)->mSourceVariables.end();
         j++)
    {
      RETURN_INTO_WSTRING(expr, mAnnoSet->getStringAnnotation(*j, L"expression"));
      runCode += expr;
      runCode += L" varptr tmpinputs ";
      wchar_t buf[30];
      swprintf(buf, 30, L"%lu", vidx++);
      runCode += buf;
      runCode += L" mov\r\n";
    }

    wchar_t buf[30];
    swprintf(buf, 30, L"I%lu", bbIdx++);
    runCode += buf;
    runCode += L" callrun\r\n";

    vidx = 0;
    for (j = (*i)->mOutputs.begin(); j != (*i)->mOutputs.end();
         j++)
    {
      if (know.count(*j) != 0)
      {
        vidx++;
        continue;
      }
      RETURN_INTO_WSTRING(expr, mAnnoSet->getStringAnnotation(*j, L"expression"));

      runCode += L"tmpoutputs ";
      swprintf(buf, 30, L"%lu", vidx++);
      runCode += buf;
      runCode += L" varptr ";
      runCode += expr;
      runCode += L" mov\r\n";
    }
  }

  dfsComputeSet(mt, cuses, blackBoxInputs, backEdges, runCode, supplements);
  runCode += L"I0 runConverged not jmpif\r\n";

  // The model has now converged (in theory). Compute any extra output values...
  dfsComputeSet(mt, cuses, mOutputVariableSet, backEdges, runCode, supplements);

  runCode += L"return\r\nend\r\n";
}

void
BBABlaBoC::computeBlaBoC()
  throw(std::exception&)
{
  CleanupList<BBAEquation*> eqns;
  BuildEquationList(eqns);
  
  // We now have a list of all equations. Split that list into black-boxes and
  // white-box equations. Note, of course, that the structure by which the
  // black boxes are connected are also white box data, but this
  // information is not included in the whiteBoxes list.
  CleanupList<BBAEquation*> blackBoxes, whiteBoxes;
  ClassifyEquations(eqns, blackBoxes, whiteBoxes);
  
  ObjRef<iface::cellml_services::CUSES> cuses;
  ObjRef<iface::cellml_services::MaLaESTransform> mt;
  
  getServices(cuses, mt);
  determineEquationVariables(cuses, mt, whiteBoxes);
  
  std::set<iface::cellml_api::CellMLVariable*> allSourceVariables,
    directConstants, allConstants;
  
  std::wstring comment, constantCode;
  std::list<std::wstring> supplements;
  std::set<iface::cellml_api::CellMLVariable*> wanted;
  processConstants(mt, cuses, allSourceVariables, directConstants, allConstants,
                   wanted, whiteBoxes, comment, constantCode, supplements);
  
  std::set<iface::cellml_api::CellMLVariable*> blackBoxInputs, blackBoxOutputs,
    blackBoxIO;
  uint32_t tmpInMax, tmpOutMax;
  std::wstring blackBoxCode;
  determineBlackBoxIO(mt, cuses, blackBoxes, blackBoxInputs, blackBoxOutputs,
                      blackBoxIO, tmpInMax, tmpOutMax,
                      blackBoxCode);
  
  uint32_t nVars;
  assignVariableIndices(wanted, comment, nVars);
  std::wstring countsCode = L"setInputVariableCount ";
  wchar_t buf[30];
  swprintf(buf, 30, L"%lu", mInputVariableSet.size());
  countsCode += buf;
  countsCode += L"\r\n";
  countsCode += L"setOutputVariableCount ";
  swprintf(buf, 30, L"%lu", mOutputVariableSet.size());
  countsCode += buf;
  countsCode += L"\r\n";
  countsCode += L"setConstantCount ";
  swprintf(buf, 30, L"%lu", allConstants.size());
  countsCode += buf;
  countsCode += L"\r\n"
    L"setOtherVariableCount ";
  swprintf(buf, 30, L"%lu", nVars);
  countsCode += buf;
  countsCode += L"\r\n";
  countsCode += L"setTempInputCount ";
  swprintf(buf, 30, L"%lu", tmpInMax);
  countsCode += buf;
  countsCode += L"\r\n";
  countsCode += L"setTempOutputCount ";
  swprintf(buf, 30, L"%lu", tmpOutMax);
  countsCode += buf;
  countsCode += L"\r\n";
  countsCode += L"setBlackBoxCount ";
  swprintf(buf, 30, L"%lu", blackBoxes.size());
  countsCode += buf;
  countsCode += L"\r\n";

  std::wstring trainingCode;
  wanted.clear();
  std::set_difference(allSourceVariables.begin(), allSourceVariables.end(),
                      allConstants.begin(), allConstants.end(),
                      std::inserter(wanted, wanted.begin()));
  processTraining(mt, cuses, blackBoxIO, allConstants, wanted, whiteBoxes,
                  blackBoxes, trainingCode, supplements);
  std::wstring runCode;

  resetSeen(wanted);
  resetSeen(mInputVariableSet);
  resetSeen(mOutputVariableSet);

  processRun(mt, cuses, blackBoxInputs, blackBoxOutputs, allConstants,
             wanted, whiteBoxes, blackBoxes, runCode, supplements);
  
  // Now lets put the whole thing together into a class output...
  mImpl += comment;
  mImpl += countsCode;
  mImpl += constantCode;
  mImpl += trainingCode;
  mImpl += runCode;
  mImpl += blackBoxCode;
  std::list<std::wstring>::iterator si;
  for (si = supplements.begin(); si != supplements.end(); si++)
    mImpl += *si;
}

wchar_t*
BBABlaBoC::errorMessage()
  throw()
{
  return CDA_wcsdup(mMessage.c_str());
}

wchar_t*
BBABlaBoC::classImplementation()
  throw(std::exception&)
{
  if (mImpl == L"")
  {
    mMessage += L"Attempt to retrieve classImplementation before calling "
      L"computeBlaBoC(); ";
    throw iface::blaboc_api::BlaBoCException();
  }
  return CDA_wcsdup(mImpl.c_str());
}

iface::blaboc_api::BlaBoCBootstrap*
createBlaBoCBootstrap()
{
  return new BBABlaBoCBootstrap();
}
