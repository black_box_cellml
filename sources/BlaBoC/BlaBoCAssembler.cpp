#include <string>
#include <map>
#include "BlaBoCImpl.hpp"
#include "BlaBoCML.hpp"
#include <vector>

char*
BBA_wchar_to_UTF8(const wchar_t *str)
{
  uint32_t len = 0;
  const wchar_t* p = str;
  wchar_t c;
  for (; (c = *p); p++)
  {
    if (c <= 0x7F)
      len++;
    else
#ifndef WCHAR_T_IS_32BIT
    if (c <= 0x7FF)
#endif
      len += 2;
#ifndef WCHAR_T_IS_32BIT
    else if (c <= 0xFFFF)
      len += 3;
    else
      len += 4;
#endif
  }
  char* newData = (char*)malloc(len + 1);
  char* np = newData;
  p = str;

  while ((c = *p++))
  {
    if (c <= 0x7F)
      *np++ = (char)c;
    else if (c <= 0x7FF)
    {
      *np++ = (char)(0xC0 | ((c >> 6) & 0x1F));
      *np++ = (char)(0x80 | (c & 0x3F));
    }
#ifndef WCHAR_T_CONSTANT_WIDTH
    else if ((c & 0xFC00) == 0xD800)
    {
      uint16_t u = ((c >> 6) & 0xF) + 1;
      *np++ = 0xF0 | ((u >> 2) & 0x7);
      *np++ = 0x80 | ((u << 4) & 0x30) | ((c >> 2) & 0xF);
      wchar_t c2 = *p++;
      *np++ = 0x80 | ((c << 4) & 0x30) | ((c2 >> 6) & 0xF);
      *np++ = 0x80 | (c2 & 0x3F);
    }
#endif
    else
#if defined(WCHAR_T_CONSTANT_WIDTH) && !defined(WCHAR_T_IS_32BIT)
         if (c <= 0xFFFF)
#endif
    {
      *np++ = (char)(0xD0 | ((c >> 12) & 0xF));
      *np++ = (char)(0x80 | ((c >> 6) & 0x3F));
      *np++ = (char)(0x80 | (c & 0x3F));
    }
#if defined(WCHAR_T_CONSTANT_WIDTH) && !defined(WCHAR_T_IS_32BIT)
    else
    {
      *np++ = (char)(0xF0 | ((c >> 18) & 0x7));
      *np++ = (char)(0x80 | ((c >> 12) & 0x3F));
      *np++ = (char)(0x80 | ((c >> 6) & 0x3F));
      *np++ = (char)(0x80 | (c & 0x3F));
    }
#endif
  }

  *np++ = 0;
  return newData;
}

class BlaBoCAssembler
{
public:
  BlaBoCAssembler(const wchar_t* aTokenStream)
    : mTokenStream(aTokenStream)
  {
  }

  struct Token
  {
    enum Type
    {
      ACOS = 0,
      ACOSH,
      AND,
      ASIN,
      ASINH,
      ATAN,
      ATANH,
      CALLRUN,
      CALLTRAIN,
      CEIL,
      CONSTANT,
      CONVERGED,
      COS,
      COSH,
      CREATEBB,
      DEF,
      DIVIDE,
      DOUBLE,
      END,
      EQUAL,
      EXP,
      FABS,
      FACTORIAL,
      FLOOR,
      FUNCPTR,
      GCD,
      GETAVERAGEINPUT,
      GEQ,
      GT,
      IDENTIFIER,
      INPUT,
      INTEGER,
      INTEGRATE,
      JMPIF,
      LCM,
      LEQ,
      LOG,
      LT,
      MAX,
      MIN,
      MINIMISE,
      MINUS,
      MOD,
      MOV,
      NEQ,
      NOT,
      OR,
      OUTPUT,
      PLUS,
      POW,
      QUOT,
      QUOTED,
      RESETCONVERGENCE,
      RETURN,
      RETURN1,
      SAVEPOINT,
      SETBLACKBOXCOUNT,
      SETCONSTANTCOUNT,
      SETINPUTVARCOUNT,
      SETJMP,
      SETOTHERVARCOUNT,
      SETOUTPUTVARCOUNT,
      SETTMPINPUTCOUNT,
      SETTMPOUTPUTCOUNT,
      SIN,
      SINH,
      STRINGCONSTANT,
      TAN,
      TANH,
      TIMES,
      TMPINPUTS,
      TMPOUTPUTS,
      UMINUS,
      VARIABLE,
      VARPTR,
      XOR
    } mType;
    std::wstring mText;
    double mValue;
    int32_t mIntValue;
  };

  Token* getNextToken()
  {
    while (true)
    {
      /* printf("Starting token parse around %.10S\n", mTokenStream); */
      wchar_t c = *mTokenStream;
      if (c == 0)
        return NULL;
      if (c == L'#')
      {
        while ((c = *mTokenStream) != 0 && c != L'\r' && c != L'\n')
          mTokenStream++;
        continue;
      }
      if (c == L'\r' || c == L'\n' || c == L' ' || c == L'\t')
      {
        mTokenStream++;
        continue;
      }

      const wchar_t* end = mTokenStream + 1;
      while ((c = *end) != 0 && c != L' ' && c != L'\r' && c != L'\n' &&
             c != L'\t')
        end++;
      size_t len = end - mTokenStream;

      mLastToken.mType = getTokenType(mTokenStream, len);
      if (mLastToken.mType == Token::IDENTIFIER)
      {
        c = *mTokenStream;
        if (c == L'I' && mTokenStream[1] >= L'0' && mTokenStream[1] <= L'9')
        {
          mTokenStream++;
          mLastToken.mType = Token::INTEGER;
          mLastToken.mIntValue = wcstol(mTokenStream, NULL, 10);
        }
        else if (c >= L'0' && c <= L'9')
        {
          mLastToken.mType = Token::DOUBLE;
          mLastToken.mValue = wcstod(mTokenStream, NULL);
        }
        else if (c == L'"')
        {
          mTokenStream++;
          mLastToken.mType = Token::QUOTED;
          mLastToken.mText = L"";
          while (true)
          {
            c = *mTokenStream++;
            if (c == 0)
            {
              fprintf(stderr, "Warning: Unterminated string in cellasm assembly.\n");
              mTokenStream--;
              return NULL;
            }

            if (c == L'\\')
            {
              c = *mTokenStream++;
              if (c == 0)
              {
                fprintf(stderr, "Warning: Unterminated string escape in cellasm assembly.\n");
                mTokenStream--;
                return NULL;
              }

              if (c == L'r')
                mLastToken.mText += L'\r';
              else if (c == L'n')
                mLastToken.mText += L'\n';
              else if (c == L'"')
                mLastToken.mText += L'"';
              else if (c == L'\\')
                mLastToken.mText += L'\\';
              else
                mLastToken.mText += c;
            }
            else if (c == L'\"')
              return &mLastToken;
            else
              mLastToken.mText += c;
          }
        }
        else
          mLastToken.mText = std::wstring(mTokenStream, len);
      }

      mTokenStream += len;
      return &mLastToken;
    }
  }

#define FLAG_

  char*
  assemble(uint32_t* len)
  {
    inputVariableCount = 0;
    outputVariableCount = 0;
    constantCount = 0;
    otherVariableCount = 0;
    tempInputCount = 0;
    tempOutputCount = 0;
    blackBoxCount = 0;

    Token* t;
    while ((t = getNextToken()) != NULL)
    {
      if (t->mType == Token::DEF)
        break;

      switch (t->mType)
      {
      case Token::SETBLACKBOXCOUNT:
        setCount(blackBoxCount);
        break;
      case Token::SETCONSTANTCOUNT:
        setCount(constantCount);
        break;
      case Token::SETINPUTVARCOUNT:
        setCount(inputVariableCount);
        break;
      case Token::SETOTHERVARCOUNT:
        setCount(otherVariableCount);
        break;
      case Token::SETOUTPUTVARCOUNT:
        setCount(outputVariableCount);
        break;
      case Token::SETTMPINPUTCOUNT:
        setCount(tempInputCount);
        break;
      case Token::SETTMPOUTPUTCOUNT:
        setCount(tempOutputCount);
        break;
      default:
        fprintf(stderr, "Error: Syntax error before %.30S\n",
               mTokenStream);
        *len = 0;
        return strdup("");
      }
    }

#define UPD (sizeof(double)/sizeof(uint32_t))
    inputVariableOffset = 0;
    outputVariableOffset = UPD * inputVariableCount;
    constantOffset = outputVariableOffset + UPD * outputVariableCount;
    otherVariableOffset = constantOffset + UPD * constantCount;
    tempInputOffset = otherVariableOffset + UPD * otherVariableCount;
    tempOutputOffset = tempInputOffset + UPD * tempInputCount;
    lastArrayOffset = tempOutputOffset + UPD * tempOutputCount;

    inputVariableOffset = 0;
    inputVariableOffset = 0;
    mData.resize(lastArrayOffset * sizeof(uint32_t), 0);

    while (t != NULL)
    {
      if (t->mType != BlaBoCAssembler::Token::DEF)
      {
        fprintf(stderr, "Only function definitions are allowed after counts, before %.20S\n",
               mTokenStream);
        *len = 0;
        return strdup("");
      }
      else
        processFunction();
      t = getNextToken();
    }

    std::string funcTable;
    std::map<std::wstring, uint32_t>::iterator fi;
    mFunctions.insert(std::pair<std::wstring,uint32_t>(L"data", mCode.size()));
    mFunctions.insert(std::pair<std::wstring,uint32_t>(L"inputVariableCount", inputVariableCount));
    mFunctions.insert(std::pair<std::wstring,uint32_t>(L"outputVariableCount", outputVariableCount));
    mFunctions.insert(std::pair<std::wstring,uint32_t>(L"constantCount", constantCount));
    mFunctions.insert(std::pair<std::wstring,uint32_t>(L"otherVariableCount", otherVariableCount));
    mFunctions.insert(std::pair<std::wstring,uint32_t>(L"tempInputCount", tempInputCount));
    mFunctions.insert(std::pair<std::wstring,uint32_t>(L"tempOutputCount", tempOutputCount));
    mFunctions.insert(std::pair<std::wstring,uint32_t>(L"blackBoxCount", blackBoxCount));
    for (fi = mFunctions.begin(); fi != mFunctions.end(); fi++)
    {
      std::wstring name = (*fi).first;
      char* msg = BBA_wchar_to_UTF8(name.c_str());
      mConvert.asLong = strlen(msg);
      funcTable += mConvert.asChars[0];
      funcTable += mConvert.asChars[1];
      funcTable += mConvert.asChars[2];
      funcTable += mConvert.asChars[3];
      funcTable += msg;
      free(msg);
      mConvert.asLong = (*fi).second;
      funcTable += mConvert.asChars[0];
      funcTable += mConvert.asChars[1];
      funcTable += mConvert.asChars[2];
      funcTable += mConvert.asChars[3];
    }

    std::string result("\xB1\xAB\x0C\x00", 4);
    mConvert.asLong = funcTable.size();
    result += mConvert.asChars[0];
    result += mConvert.asChars[1];
    result += mConvert.asChars[2];
    result += mConvert.asChars[3];
    result += funcTable;
    result += mCode;
    result += mData;
    *len = result.size();
    char* out = (char*)malloc(*len + 1);
    memcpy(out, result.data(), *len);
    out[*len] = 0;
    return out;
  }

private:
  Token mLastToken;
  const wchar_t* mTokenStream;
  std::map<std::wstring, uint32_t> mFunctions;
  std::multimap<std::wstring, uint32_t> mFunctionRelocations;
  uint32_t inputVariableCount, outputVariableCount, constantCount,
    otherVariableCount, tempInputCount, tempOutputCount, blackBoxCount;
  uint32_t inputVariableOffset, outputVariableOffset, constantOffset,
    otherVariableOffset, tempInputOffset, tempOutputOffset, lastArrayOffset;
  uint32_t mFlags;
  std::string mCode, mData;

  union
  {
    double asDouble;
    uint32_t asLong;
    char asChars[8];
  } mConvert;

  static struct TokenLookup
  {
    const wchar_t* name;
    Token::Type type;
  } mTokenTypes[];

  static uint8_t getOpcodeForToken(Token::Type type);

  static uint32_t tokenCount();

  static Token::Type
  getTokenType(const wchar_t* input, size_t len)
  {
    int low = 0, high = tokenCount() - 1;

    while (true)
    {
      int mid = (low + high) / 2;
      int ret = wcsncmp(mTokenTypes[mid].name, input, len);

      if (ret == 0)
      {
        if (mTokenTypes[mid].name[len] != 0)
          ret = 1;
        else
          return mTokenTypes[mid].type;
      }
    
      if (ret > 0)
      {
        high = mid - 1;
      }
      else
      {
        low = mid + 1;
      }

      if (low > high)
        return Token::IDENTIFIER;
    }
  }

  void processFunction()
  {
    Token* t = getNextToken();
    if (t == NULL)
    {
      fprintf(stderr, "Unexpected end of file at beginning of function.\n");
    }
    if (t->mType != Token::IDENTIFIER)
    {
      fprintf(stderr, "Expected an identifier after def, before %.30S\n",
             mTokenStream);
      return;
    }

    mConvert.asLong = mCode.size();
    mFunctions.insert(std::pair<std::wstring, uint32_t>
                      (t->mText, mConvert.asLong));

    std::multimap<std::wstring, uint32_t>::iterator fri;
    fri = mFunctionRelocations.find(t->mText);
    for (; fri != mFunctionRelocations.end(); fri++)
    {
      uint32_t change = (*fri).second;
      mCode[change] = mConvert.asChars[0];
      mCode[change + 1] = mConvert.asChars[1];
      mCode[change + 2] = mConvert.asChars[2];
      mCode[change + 3] = mConvert.asChars[3];
    }
    
    while (true)
    {
      t = getNextToken();
      if (t == NULL)
      {
        fprintf(stderr, "Unexpected end of file in function definition near %.30S\n",
               mTokenStream);
        return;
      }
      uint8_t oc = getOpcodeForToken(t->mType);
      if (oc != OPCODE_INVALID)
      {
        /* printf("Appending opcode %02X\n", oc); */
        mCode += (char)oc;
        continue;
      }

      if (t->mType == Token::END)
        return;

      if (t->mType == Token::CONSTANT)
      {
        fromArray(constantOffset);
        continue;
      }
      else if (t->mType == Token::INPUT)
      {
        fromArray(inputVariableOffset);
        continue;
      }
      else if (t->mType == Token::OUTPUT)
      {
        fromArray(outputVariableOffset);
        continue;
      }
      else if (t->mType == Token::VARIABLE)
      {
        fromArray(otherVariableOffset);
        continue;
      }
      else if (t->mType == Token::TMPINPUTS)
      {
        fromArray(tempInputOffset);
        continue;
      }
      else if (t->mType == Token::TMPOUTPUTS)
      {
        fromArray(tempOutputOffset);
        continue;
      }
      else if (t->mType == Token::DOUBLE)
      {
        mConvert.asDouble = t->mValue;
        mCode += (char)OPCODE_PUSH_IMMED;
        mCode += (char)mConvert.asChars[0];
        mCode += (char)mConvert.asChars[1];
        mCode += (char)mConvert.asChars[2];
        mCode += (char)mConvert.asChars[3];
        mCode += (char)OPCODE_PUSH_IMMED;
        mCode += (char)mConvert.asChars[4];
        mCode += (char)mConvert.asChars[5];
        mCode += (char)mConvert.asChars[6];
        mCode += (char)mConvert.asChars[7];
        continue;
      }
      else if (t->mType == Token::INTEGER)
      {
        mConvert.asLong = t->mIntValue;
        mCode += (char)OPCODE_PUSH_IMMED;
        mCode += (char)mConvert.asChars[0];
        mCode += (char)mConvert.asChars[1];
        mCode += (char)mConvert.asChars[2];
        mCode += (char)mConvert.asChars[3];
        continue;
      }
      else if (t->mType == Token::STRINGCONSTANT)
      {
        t = getNextToken();
        if (t == NULL)
        {
          fprintf(stderr, "Unexpected end of file after stringconstant operator.\n");
          return;
        }
        if (t->mType != Token::QUOTED)
        {
          fprintf(stderr, "Expected quoted string after stringconstant.\n");
          return;
        }
        uint32_t pos = mData.size();
        mConvert.asLong = t->mText.size() * sizeof(wchar_t);
        mData += mConvert.asChars[0];
        mData += mConvert.asChars[1];
        mData += mConvert.asChars[2];
        mData += mConvert.asChars[3];

        mData += std::string((char*)t->mText.c_str(), t->mText.size() *
                             sizeof(wchar_t));
        mConvert.asLong = pos;
        mCode += (char)OPCODE_PUSH_IMMED;
        mCode += (char)mConvert.asChars[0];
        mCode += (char)mConvert.asChars[1];
        mCode += (char)mConvert.asChars[2];
        mCode += (char)mConvert.asChars[3];
        continue;
      }
      else if (t->mType == Token::FUNCPTR)
      {
        t = getNextToken();
        if (t == NULL)
        {
          fprintf(stderr, "Unexpected end of file after funcptr.\n");
          return;
        }
        if (t->mType != Token::IDENTIFIER)
        {
          fprintf(stderr, "Expected a function identifier after funcptr.\n");
          return;
        }
        std::map<std::wstring, uint32_t>::iterator fi =
          mFunctions.find(t->mText);
        mConvert.asLong = 0;
        if (fi == mFunctions.end())
          mFunctionRelocations.insert(std::pair<std::wstring, uint32_t>
                                      (t->mText, mCode.size()));
        else
          mConvert.asLong = (*fi).second;
        mCode += (char)OPCODE_PUSH_IMMED;
        mCode += (char)mConvert.asChars[0];
        mCode += (char)mConvert.asChars[1];
        mCode += (char)mConvert.asChars[2];
        mCode += (char)mConvert.asChars[3];
        continue;
      }
      else if (t->mType == Token::VARPTR)
      {
        t = getNextToken();
        if (t == NULL)
        {
          fprintf(stderr, "Unexpected end of file after varptr.\n");
          return;
        }
        uint32_t offset;
        switch (t->mType)
        {
        case Token::CONSTANT:
          offset = constantOffset;
          break;
        case Token::VARIABLE:
          offset = otherVariableOffset;
          break;
        case Token::INPUT:
          offset = inputVariableOffset;
          break;
        case Token::OUTPUT:
          offset = outputVariableOffset;
          break;
        case Token::TMPINPUTS:
          offset = tempInputOffset;
          break;
        case Token::TMPOUTPUTS:
          offset = tempOutputOffset;
          break;
        default:
          fprintf(stderr, "Invalid token after varptr before %.30S\n",
                 mTokenStream);
          return;
        }
        t = getNextToken();
        if (t == NULL)
        {
          fprintf(stderr, "Unexpected end of file after varptr array.\n");
          return;
        }

        if (t->mType != Token::DOUBLE)
        {
          fprintf(stderr, "Expected offset into array before %.30S.\n",
                 mTokenStream);
          return;
        }

        mConvert.asLong = offset + ((uint32_t)t->mValue) * 2;
        mCode += (char)OPCODE_PUSH_IMMED;
        mCode += (char)mConvert.asChars[0];
        mCode += (char)mConvert.asChars[1];
        mCode += (char)mConvert.asChars[2];
        mCode += (char)mConvert.asChars[3];
        continue;
      }

      fprintf(stderr, "Invalid operation (token type %d) in function before %.30S\n",
              (int)t->mType, mTokenStream);
      return;
    }
  }

  void setCount(uint32_t& count)
  {
    Token* t = getNextToken();
    if (t == NULL)
    {
      fprintf(stderr, "End of file when expecting a count number.\n");
      return;
    }
    if (t->mType != Token::DOUBLE)
    {
      fprintf(stderr, "Expected an integer after count token.\n");
      return;
    }
    count = ((uint32_t)t->mValue);
  }

  void fromArray(uint32_t offset)
  {
    Token* t = getNextToken();
    if (t == NULL)
    {
      fprintf(stderr, "End of file when expecting a count number.\n");
      return;
    }
    if (t->mType != Token::DOUBLE)
    {
      fprintf(stderr, "Expected an integer after array token.\n");
      return;
    }

    mConvert.asLong = offset + (uint32_t)t->mValue * 2;

    mCode += (char)OPCODE_FETCHDOUBLE;
    mCode += mConvert.asChars[0];
    mCode += mConvert.asChars[1];
    mCode += mConvert.asChars[2];
    mCode += mConvert.asChars[3];
  }
};

BlaBoCAssembler::TokenLookup
BlaBoCAssembler::mTokenTypes[] = 
  {
    {L"acos", BlaBoCAssembler::Token::ACOS},
    {L"acosh", BlaBoCAssembler::Token::ACOSH},
    {L"and", BlaBoCAssembler::Token::AND},
    {L"asin", BlaBoCAssembler::Token::ASIN},
    {L"asinh", BlaBoCAssembler::Token::ASINH},
    {L"atan", BlaBoCAssembler::Token::ATAN},
    {L"atanh", BlaBoCAssembler::Token::ATANH},
    {L"callrun", BlaBoCAssembler::Token::CALLRUN},
    {L"calltrain", BlaBoCAssembler::Token::CALLTRAIN},
    {L"ceil", BlaBoCAssembler::Token::CEIL},
    {L"constant", BlaBoCAssembler::Token::CONSTANT},
    {L"convergenceSavePoint", BlaBoCAssembler::Token::SAVEPOINT},
    {L"cos", BlaBoCAssembler::Token::COS},
    {L"cosh", BlaBoCAssembler::Token::COSH},
    {L"createBlackBox", BlaBoCAssembler::Token::CREATEBB},
    {L"def", BlaBoCAssembler::Token::DEF},
    {L"divide", BlaBoCAssembler::Token::DIVIDE},
    {L"end", BlaBoCAssembler::Token::END},
    {L"equal", BlaBoCAssembler::Token::EQUAL},
    {L"exp", BlaBoCAssembler::Token::EXP},
    {L"fabs", BlaBoCAssembler::Token::FABS},
    {L"factorial", BlaBoCAssembler::Token::FACTORIAL},
    {L"floor", BlaBoCAssembler::Token::FLOOR},
    {L"funcptr", BlaBoCAssembler::Token::FUNCPTR},
    {L"gcd", BlaBoCAssembler::Token::GCD},
    {L"geq", BlaBoCAssembler::Token::GEQ},
    {L"getaverageinput", BlaBoCAssembler::Token::GETAVERAGEINPUT},
    {L"gt", BlaBoCAssembler::Token::GT},
    {L"input", BlaBoCAssembler::Token::INPUT},
    {L"integrate", BlaBoCAssembler::Token::INTEGRATE},
    {L"jmpif", BlaBoCAssembler::Token::JMPIF},
    {L"lcm", BlaBoCAssembler::Token::LCM},
    {L"leq", BlaBoCAssembler::Token::LEQ},
    {L"log", BlaBoCAssembler::Token::LOG},
    {L"lt", BlaBoCAssembler::Token::LT},
    {L"max", BlaBoCAssembler::Token::MAX},
    {L"min", BlaBoCAssembler::Token::MIN},
    {L"minimise", BlaBoCAssembler::Token::MINIMISE},
    {L"minus", BlaBoCAssembler::Token::MINUS},
    {L"mod", BlaBoCAssembler::Token::MOD},
    {L"mov", BlaBoCAssembler::Token::MOV},
    {L"neq", BlaBoCAssembler::Token::NEQ},
    {L"not", BlaBoCAssembler::Token::NOT},
    {L"or", BlaBoCAssembler::Token::OR},
    {L"output", BlaBoCAssembler::Token::OUTPUT},
    {L"plus", BlaBoCAssembler::Token::PLUS},
    {L"pow", BlaBoCAssembler::Token::POW},
    {L"quot", BlaBoCAssembler::Token::QUOT},
    {L"resetConvergenceCounter", BlaBoCAssembler::Token::RESETCONVERGENCE},
    {L"return", BlaBoCAssembler::Token::RETURN},
    {L"return1", BlaBoCAssembler::Token::RETURN1},
    {L"runConverged", BlaBoCAssembler::Token::CONVERGED},
    {L"setBlackBoxCount", BlaBoCAssembler::Token::SETBLACKBOXCOUNT},
    {L"setConstantCount", BlaBoCAssembler::Token::SETCONSTANTCOUNT},
    {L"setInputVariableCount", BlaBoCAssembler::Token::SETINPUTVARCOUNT},
    {L"setOtherVariableCount", BlaBoCAssembler::Token::SETOTHERVARCOUNT},
    {L"setOutputVariableCount", BlaBoCAssembler::Token::SETOUTPUTVARCOUNT},
    {L"setTempInputCount", BlaBoCAssembler::Token::SETTMPINPUTCOUNT},
    {L"setTempOutputCount", BlaBoCAssembler::Token::SETTMPOUTPUTCOUNT},
    {L"setjmp", BlaBoCAssembler::Token::SETJMP},
    {L"sin", BlaBoCAssembler::Token::SIN},
    {L"sinh", BlaBoCAssembler::Token::SINH},
    {L"stringconstant", BlaBoCAssembler::Token::STRINGCONSTANT},
    {L"tan", BlaBoCAssembler::Token::TAN},
    {L"tanh", BlaBoCAssembler::Token::TANH},
    {L"times", BlaBoCAssembler::Token::TIMES},
    {L"tmpinputs", BlaBoCAssembler::Token::TMPINPUTS},
    {L"tmpoutputs", BlaBoCAssembler::Token::TMPOUTPUTS},
    {L"uminus", BlaBoCAssembler::Token::UMINUS},
    {L"variable", BlaBoCAssembler::Token::VARIABLE},
    {L"varptr", BlaBoCAssembler::Token::VARPTR},
    {L"xor", BlaBoCAssembler::Token::XOR}
  };

uint32_t
BlaBoCAssembler::tokenCount()
{
  return sizeof(BlaBoCAssembler::mTokenTypes) / sizeof(BlaBoCAssembler::mTokenTypes[0]);
}

uint8_t
BlaBoCAssembler::getOpcodeForToken(Token::Type type)
{
  static uint8_t lookupTable[] =
  {
    OPCODE_ACOS /*ACOS*/,
    OPCODE_ACOSH /*ACOSH*/,
    OPCODE_AND /*AND*/,
    OPCODE_ASIN /*ASIN*/,
    OPCODE_ASINH /*ASINH*/,
    OPCODE_ATAN /*ATAN*/,
    OPCODE_ATANH /*ATANH*/,
    OPCODE_CALLRUN /*CALLRUN*/,
    OPCODE_CALLTRAIN /*CALLTRAIN*/,
    OPCODE_CEIL /*CEIL*/,
    OPCODE_INVALID /*CONSTANT*/,
    OPCODE_CONVERGED /*CONVERGED*/,
    OPCODE_COS /*COS*/,
    OPCODE_COSH /*COSH*/,
    OPCODE_CREATEBB /*CREATEBB*/,
    OPCODE_INVALID /*DEF*/,
    OPCODE_DIVIDE /*DIVIDE*/,
    OPCODE_INVALID /*DOUBLE*/,
    OPCODE_INVALID /*END*/,
    OPCODE_EQUAL /*EQUAL*/,
    OPCODE_EXP /*EXP*/,
    OPCODE_FABS /*FABS*/,
    OPCODE_FACTORIAL /*FACTORIAL*/,
    OPCODE_FLOOR /*FLOOR*/,
    OPCODE_INVALID /*FUNCPTR*/,
    OPCODE_GCD /*GCD*/,
    OPCODE_GETAVERAGEINPUT /*GETAVERAGEINPUT*/,
    OPCODE_GEQ /*GEQ*/,
    OPCODE_GT /*GT*/,
    OPCODE_INVALID /*IDENTIFIER*/,
    OPCODE_INVALID /*INPUT*/,
    OPCODE_INVALID /*INTEGER*/,
    OPCODE_INTEGRATE /*INTEGRATE*/,
    OPCODE_JMPIF /*JMPIF*/,
    OPCODE_LCM /*LCM*/,
    OPCODE_LEQ /*LEQ*/,
    OPCODE_LOG /*LOG*/,
    OPCODE_LT /*LT*/,
    OPCODE_MAX /*MAX*/,
    OPCODE_MIN /*MIN*/,
    OPCODE_MINIMISE /*MINIMISE*/,
    OPCODE_MINUS /*MINUS*/,
    OPCODE_MOD /*MOD*/,
    OPCODE_MOV /*MOV*/,
    OPCODE_NEQ /*NEQ*/,
    OPCODE_NOT /*NOT*/,
    OPCODE_OR /*OR*/,
    OPCODE_INVALID /*OUTPUT*/,
    OPCODE_PLUS /*PLUS*/,
    OPCODE_POW /*POW*/,
    OPCODE_QUOT /*QUOT*/,
    OPCODE_INVALID /*QUOTED*/,
    OPCODE_RESETCONVERGENCE /*RESETCONVERGENCE*/,
    OPCODE_RETURN /*RETURN*/,
    OPCODE_RETURN1 /*RETURN1*/,
    OPCODE_SAVEPOINT /*SAVEPOINT*/,
    OPCODE_INVALID /*SETBLACKBOXCOUNT*/,
    OPCODE_INVALID /*SETCONSTANTCOUNT*/,
    OPCODE_INVALID /*SETINPUTVARCOUNT*/,
    OPCODE_SETJMP /*SETJMP*/,
    OPCODE_INVALID /*SETOTHERVARCOUNT*/,
    OPCODE_INVALID /*SETOUTPUTVARCOUNT*/,
    OPCODE_INVALID /*SETTMPINPUTCOUNT*/,
    OPCODE_INVALID /*SETTMPOUTPUTCOUNT*/,
    OPCODE_SIN /*SIN*/,
    OPCODE_SINH /*SINH*/,
    OPCODE_INVALID /*STRINGCONSTANT*/,
    OPCODE_TAN /*TAN*/,
    OPCODE_TANH /*TANH*/,
    OPCODE_TIMES /*TIMES*/,
    OPCODE_INVALID /*TMPINPUTS*/,
    OPCODE_INVALID /*TMPOUTPUTS*/,
    OPCODE_UMINUS /*UMINUS*/,
    OPCODE_INVALID /*VARIABLE*/,
    OPCODE_INVALID /*VARPTR*/,
    OPCODE_XOR /*XOR*/
  };

  uint32_t idx = (uint32_t)type;
  if (idx > Token::XOR)
    return OPCODE_INVALID;
  return lookupTable[idx];
}

char*
BBABlaBoCBootstrap::assembleBlaBoC(const wchar_t* aSource, uint32_t* len)
  throw()
{
  BlaBoCAssembler ba(aSource);
  return ba.assemble(len);
}
