include_HEADERS += \
  $(top_builddir)/interfaces/IfaceBlaBoC.hxx

$(top_builddir)/interfaces/Iface%.hxx \
$(top_builddir)/interfaces/CCI%.hxx \
$(top_builddir)/interfaces/SCI%.hxx \
$(top_builddir)/interfaces/CCI%.cxx \
$(top_builddir)/interfaces/SCI%.cxx: $(top_srcdir)/interfaces/%.idl
	SAVEDIR=`pwd` && \
	mkdir -p $(top_builddir)/interfaces && \
	cd $(top_builddir)/interfaces && \
	$(CYGWIN_WRAPPER) omniidl -p ${CELLML_API_DIR}/simple_interface_generators/omniidl_be -I${CELLML_API_DIR}/interfaces -bsimple_cpp $$SAVEDIR/$< && \
	cd $$SAVEDIR

BUILT_SOURCES += $(top_builddir)/interfaces/IfaceBlaBoC.hxx
