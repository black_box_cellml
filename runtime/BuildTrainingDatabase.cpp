#include "TrainingDatabase.hpp"
#include <mysql++/mysql++.h>
#include <sys/errno.h>

class DBBException
  : public std::exception
{
public:
  DBBException(const std::string& aWhat)
    : mWhat(aWhat)
  {
  }

  ~DBBException() throw()
  {
  }

  const char*
  what() const throw()
  {
    return mWhat.c_str();
  }

private:
  std::string mWhat;
};

class DatabaseBuilder
{
public:
  DatabaseBuilder(const char* sql_host, const char* sql_user,
                  const char* sql_password, const char* sql_database,
                  const char* db_filename)
    : mFile(NULL), mConn(mysqlpp::use_exceptions)
  {
    mFile = fopen(db_filename, "w");
    if (mFile == NULL)
      throw DBBException("Cannot open the database input file.");

    mConn.connect(sql_database, sql_host, sql_user, sql_password);
  }

  ~DatabaseBuilder()
  {
    if (mFile != NULL)
      fclose(mFile);
  }

  void
  BuildDatabase()
  {
    mysqlpp::Query q(mConn.query());
    q << "SELECT id FROM variants ORDER BY id";
    mysqlpp::Result r(q.store());

    struct TrainingDatabaseHeader tdh;
    
    tdh.first_variant_id = r.at(0)["id"];
    tdh.nvariants = r.size();

    q.reset();
    q << "SELECT v.dirtype, COUNT(*) FROM training_data AS td, variables AS v "
      "WHERE v.id = td.variable_id AND td.variant_id="
      << tdh.first_variant_id << " GROUP BY v.dirtype";
    mysqlpp::Result r2(q.store());

    tdh.ninput_signals = 0;
    tdh.noutput_signals = 0;
    uint32_t i, l = r2.size();

    for (i = 0; i < l; i++)
    {
      uint32_t type = r2.at(i).at(0);
      if (type == 0)
        tdh.ninput_signals = r2.at(i).at(1);
      else
        tdh.noutput_signals = r2.at(i).at(1);
    }

    if (fwrite(&tdh, sizeof(tdh), 1, mFile) != 1)
      throw DBBException(strerror(errno));

    l = r2.size();
    uint32_t mExpectVariant = tdh.first_variant_id;
    double inputs[tdh.ninput_signals + 1], outputs[tdh.noutput_signals];
    double * iotab[] = {inputs, outputs};
    uint32_t iolims[] = {tdh.ninput_signals, tdh.noutput_signals};

    l = r.size();
    for (i = 0; i < l; i++)
    {
      uint32_t variant = r.at(i).at(0);

      if (variant != mExpectVariant++)
        throw DBBException("Expected consecutive variant IDs.");
      
      printf("Building row for variant %u\n", variant);

      q.reset();
      q << "SELECT td.value, td.array_index, v.dirtype FROM training_data AS "
        "td, variables AS v WHERE td.variable_id = v.id AND td.variant_id = "
        << variant;
      mysqlpp::ResUse ru(q.use());
      
      try
      {
        while (true)
        {
          mysqlpp::Row row(ru.fetch_row());
          uint32_t io = row["dirtype"];
          uint32_t idx = row["array_index"];
          
          if (io > 1)
          {
            throw DBBException("dirtype field invalid.");
          }
          else if (idx >= iolims[io])
          {
            throw DBBException("array_index field invalid.");
          }
          
          iotab[io][idx] = row["value"];
        }
      }
      catch (mysqlpp::EndOfResults& eor)
      {
      }

      if (fwrite(inputs, tdh.ninput_signals * sizeof(double), 1, mFile) < 0)
      {
        perror("fwrite");
        throw DBBException("Error writing the input signals.");
      }

      if (fwrite(outputs, tdh.noutput_signals * sizeof(double), 1, mFile) < 0)
      {
        perror("fwrite");
        throw DBBException("Error writing the output signals.");
      }
    }
  }

private:
  FILE* mFile;
  mysqlpp::Connection mConn;
};

int
main(int argc, char** argv)
{
  if (argc < 6)
  {
    printf("Usage: BuildTrainingDatabase sql_host sql_user sql_password sql_database db_filename\n");
    return 0;
  }

  try
  {
    DatabaseBuilder dbb(argv[1], argv[2], argv[3], argv[4], argv[5]);
    dbb.BuildDatabase();
  }
  catch (std::exception& e)
  {
    printf("Error: %s\n", e.what());
  }
}
