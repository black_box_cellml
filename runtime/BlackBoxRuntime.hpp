#include "Utilities.hxx"
// Undo ugly namespace pollution...
#undef N
#undef M
#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <map>
#include <exception>
#include <string>

#define THRESHOLD 1.0

class BlackBoxModelBase
{
public:
  virtual ~BlackBoxModelBase() {}
  virtual void train(double * IN, double * OUT) = 0;
  virtual void run(double * IN, double * OUT) = 0;
  virtual double GetAverageInput(uint32_t index) = 0;
  virtual void Serialise(std::string& aStoreIn) = 0;
  virtual void Deserialise(const std::string& aData) = 0;
};

class BlackBoxModelFactory
{
public:
  virtual BlackBoxModelBase*
  createBlackBoxModel
  (
   std::wstring& aID,
   uint32_t aInputs,
   uint32_t aOutputs
  ) = 0;
};

class BlackBoxPersistBase
{
public:
  virtual ~BlackBoxPersistBase() {}

  virtual void saveState(const char* aURL, const std::string& aData) = 0;
  virtual void restoreState(const char* aURL, std::string& aData) = 0;
};

class MixedModelBase
{
public:
  virtual uint32_t countInputVariables() const = 0;
  virtual uint32_t countOutputVariables() const = 0;
  virtual uint32_t countConstants() const = 0;
  virtual uint32_t countOtherVariables() const = 0;
  virtual uint32_t countTempInputs() const = 0;
  virtual uint32_t countTempOutputs() const = 0;
  virtual uint32_t countBlackBoxes() const = 0;
  virtual void initConstants() = 0;
  virtual void initBlackBoxes() = 0;
  virtual void train(double* aINPUTS, double* aOUTPUTS) = 0;
  virtual void run(double* aINPUTS, double* aOUTPUTS) = 0;

  MixedModelBase();
  virtual ~MixedModelBase();

  void setup()
  {
    BACKVARIABLES = new double[countOtherVariables() + 1];
    BACKOUTPUTS = new double[countOutputVariables()];
    nBlackBoxes = countBlackBoxes();
    blackBoxes = new BlackBoxModelBase*[nBlackBoxes];
    blackBoxNames = new std::string[nBlackBoxes];
    
    uint32_t i;
    for (i = 0; i < nBlackBoxes; i++)
      new (blackBoxNames + i) std::string();
    memset(blackBoxes, 0, sizeof(BlackBoxModelBase*)*nBlackBoxes);
    initBlackBoxes();
    initConstants();
  }

  void resetConvergenceCounter()
  {
    mConvergeSteps = 0;
  }

  void convergenceSavePoint()
  {
#if 0
    int i;
    printf("At convergence save point...\n");
    for (i = 0; i < countOtherVariables(); i++)
      printf(" * VARIABLES[%u] = %g\n", i, VARIABLES[i]);
    for (i = 0; i < countOutputVariables(); i++)
      printf(" * OUTPUTS[%u] = %g\n", i, OUTPUTS[i]);
    printf("End convergence save point.\n");
#endif

    memcpy(BACKVARIABLES, VARIABLES, countOtherVariables() * sizeof(double));
    memcpy(BACKOUTPUTS, OUTPUTS, countOutputVariables() * sizeof(double));
  }

  bool runConverged();

  void createBlackBox
  (
   uint32_t aIdx,
   const wchar_t* URI,
   uint32_t inputs,
   uint32_t outputs
  );

  static void RegisterModelFactory
  (
   const std::wstring aURL,
   BlackBoxModelFactory* aFactory
  )
  {
    blackBoxFactories().insert(std::pair<std::wstring, BlackBoxModelFactory*>
                               (aURL, aFactory)
                              );
  }

protected:
  BlackBoxPersistBase* mPersister;
  double * VARIABLES, * INPUTS, * OUTPUTS , * CONSTANTS, * TMPINPUTS,
         * TMPOUTPUTS;
  BlackBoxModelBase ** blackBoxes;
  std::string* blackBoxNames;

private:
  double * BACKVARIABLES, * BACKOUTPUTS;
  uint32_t nBlackBoxes;
  uint32_t mConvergeSteps;
  static std::map<std::wstring,BlackBoxModelFactory*>& blackBoxFactories()
  {
    if (sBlackBoxFactories == NULL)
      sBlackBoxFactories = new std::map<std::wstring,BlackBoxModelFactory*>();
    return *sBlackBoxFactories;
  }
  static std::map<std::wstring,BlackBoxModelFactory*>* sBlackBoxFactories;
};

#ifdef IN_BBVM_MODULE
#define BBVM_PUBLIC_PRE CDA_EXPORT_PRE
#define BBVM_PUBLIC_POST CDA_EXPORT_POST
#else
#define BBVM_PUBLIC_PRE CDA_IMPORT_PRE
#define BBVM_PUBLIC_POST CDA_IMPORT_POST
#endif
BBVM_PUBLIC_PRE MixedModelBase* createMixedModelVM(const char* filename)
  BBVM_PUBLIC_POST;

class BlackBoxCache
{
public:
  virtual ~BlackBoxCache() {}

  virtual BlackBoxModelBase* FindModelInCache(const std::wstring& aURI) = 0;
  virtual void CacheModel(const std::wstring& aURI, BlackBoxModelBase* aModel) = 0;
  virtual void SaveAllCachedModels() = 0;
  virtual void DiscardCache() = 0;
};

BBVM_PUBLIC_PRE extern BlackBoxCache* gBlackBoxCache BBVM_PUBLIC_POST;
BBVM_PUBLIC_PRE void setupStandardBlackBoxCache() BBVM_PUBLIC_POST;
