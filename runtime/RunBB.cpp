#include <stdio.h>
#include "BlackBoxRuntime.hpp"

int
main(int argc, char** argv)
{
  if (argc < 2)
  {
    printf("Usage: RunBB compiledModel\n");
    return 1;
  }
  MixedModelBase* mmb = createMixedModelVM(argv[1]);
  if (mmb == NULL)
  {
    printf("Couldn't create a virtual machine.\n");
  }

  mmb->setup();

  delete mmb;
}
