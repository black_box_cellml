#include "BlackBoxRuntime.hpp"
#include <mysql++/mysql++.h>

BlackBoxCache* gBlackBoxCache = NULL;

char*
BBArun_wchar_to_UTF8(const wchar_t *str)
{
  uint32_t len = 0;
  const wchar_t* p = str;
  wchar_t c;
  for (; (c = *p); p++)
  {
    if (c <= 0x7F)
      len++;
    else
#ifndef WCHAR_T_IS_32BIT
    if (c <= 0x7FF)
#endif
      len += 2;
#ifndef WCHAR_T_IS_32BIT
    else if (c <= 0xFFFF)
      len += 3;
    else
      len += 4;
#endif
  }
  char* newData = (char*)malloc(len + 1);
  char* np = newData;
  p = str;

  while ((c = *p++))
  {
    if (c <= 0x7F)
      *np++ = (char)c;
    else if (c <= 0x7FF)
    {
      *np++ = (char)(0xC0 | ((c >> 6) & 0x1F));
      *np++ = (char)(0x80 | (c & 0x3F));
    }
#ifndef WCHAR_T_CONSTANT_WIDTH
    else if ((c & 0xFC00) == 0xD800)
    {
      uint16_t u = ((c >> 6) & 0xF) + 1;
      *np++ = 0xF0 | ((u >> 2) & 0x7);
      *np++ = 0x80 | ((u << 4) & 0x30) | ((c >> 2) & 0xF);
      wchar_t c2 = *p++;
      *np++ = 0x80 | ((c << 4) & 0x30) | ((c2 >> 6) & 0xF);
      *np++ = 0x80 | (c2 & 0x3F);
    }
#endif
    else
#if defined(WCHAR_T_CONSTANT_WIDTH) && !defined(WCHAR_T_IS_32BIT)
         if (c <= 0xFFFF)
#endif
    {
      *np++ = (char)(0xD0 | ((c >> 12) & 0xF));
      *np++ = (char)(0x80 | ((c >> 6) & 0x3F));
      *np++ = (char)(0x80 | (c & 0x3F));
    }
#if defined(WCHAR_T_CONSTANT_WIDTH) && !defined(WCHAR_T_IS_32BIT)
    else
    {
      *np++ = (char)(0xF0 | ((c >> 18) & 0x7));
      *np++ = (char)(0x80 | ((c >> 12) & 0x3F));
      *np++ = (char)(0x80 | ((c >> 6) & 0x3F));
      *np++ = (char)(0x80 | (c & 0x3F));
    }
#endif
  }

  *np++ = 0;
  return newData;
}

class SQLModelPersister
  : public BlackBoxPersistBase
{
public:
  SQLModelPersister()
    : mConn(mysqlpp::use_exceptions), mReady(false)
  {
    const char* db = getenv("BLABOC_DB");
    if (db == NULL)
    {
      printf("Missing BLABOC_DB. Not creating persister.\n");
      return;
    }
    const char* host = getenv("BLABOC_HOST");
    if (host == NULL)
    {
      printf("Missing BLABOC_HOST. Not creating persister.\n");
      return;
    }
    const char* user = getenv("BLABOC_USER");
    if (user == NULL)
    {
      printf("Missing BLABOC_USER. Not creating persister.\n");
      return;
    }
    const char* pass = getenv("BLABOC_PASSWORD");
    if (pass == NULL)
    {
      printf("Missing BLABOC_PASSWORD. Not creating persister.\n");
      return;
    }

    try
    {
      mConn.connect(db, host, user, pass);
      mReady = true;
    }
    catch (std::exception& e)
    {
      printf("Error connecting to database: %s. Not creating persister.\n",
             e.what());
    }
  }

  ~SQLModelPersister()
  {
  }

  void
  saveState(const char* aURL, const std::string& aData)
  {
    if (!mReady)
      return;

    // Figure out if we need to update or insert...
    mysqlpp::Query query(mConn.query());

    query << "SELECT COUNT(*) FROM black_box_state WHERE url=\""
          << mysqlpp::escape << aURL << "\"";

    try
    {
      mysqlpp::Result res = query.store();
      unsigned int count = res.at(0)[(unsigned int)0];
      query.reset();
      if (count == 0)
      {
        query << "INSERT INTO black_box_state (url,data) VALUES (\""
              << mysqlpp::escape << aURL << "\", \""
              << mysqlpp::escape << aData << "\")";
        query.execute();
      }
      else
      {
        query << "UPDATE black_box_state SET data=\""
              << mysqlpp::escape << aData << "\" WHERE url=\""
              << mysqlpp::escape << aURL << "\"";
        query.execute();
      }
    }
    catch (std::exception& e)
    {
      printf("Exception saving state: %s\n", e.what());
    }
  }

  void
  restoreState(const char* aURL, std::string& aData)
  {
    if (!mReady)
    {
      aData.assign("");
      return;
    }

    mysqlpp::Query query(mConn.query());
    query << "SELECT data FROM black_box_state WHERE url=\""
          << mysqlpp::escape << aURL << "\"";

    try
    {
      mysqlpp::Result res = query.store();
      if (res.rows() == 0)
      {
        aData.assign("");
        return;
      }
      mysqlpp::ColData col = res.at(0)["data"];
      aData.assign(col.data(), col.size());
    }
    catch (std::exception& e)
    {
      printf("Exception restoring state: %s\n", e.what());
      aData.assign("");
    }
  }

private:
  mysqlpp::Connection mConn;
  bool mReady;
};

MixedModelBase::MixedModelBase()
  : mPersister(new SQLModelPersister()),
    VARIABLES(NULL), INPUTS(NULL), OUTPUTS(NULL),
    CONSTANTS(NULL), TMPINPUTS(NULL), TMPOUTPUTS(NULL),
    blackBoxes(NULL), blackBoxNames(NULL),
    BACKVARIABLES(NULL), BACKOUTPUTS(NULL)
{
}

MixedModelBase::~MixedModelBase()
{
  size_t i;
  
  if (BACKVARIABLES != NULL)
    delete [] BACKVARIABLES;

  if (BACKOUTPUTS != NULL)
    delete [] BACKOUTPUTS;
  
  if (blackBoxes != NULL)
  {
    std::string state;
    
    if (gBlackBoxCache == NULL)
    {
      for (i = 0; i < nBlackBoxes; i++)
        if (blackBoxes[i] != NULL)
        {
          blackBoxes[i]->Serialise(state);
          if (state.size() != 0)
            mPersister->saveState(blackBoxNames[i].c_str(), state);
          delete blackBoxes[i];
        }
    }
    delete [] blackBoxes;
  }

  delete mPersister;

  if (blackBoxNames != NULL)
    delete [] blackBoxNames;
}

bool
MixedModelBase::runConverged()
{
  if (mConvergeSteps++ > 10)
  {
    printf("Warning: It took more than 10 steps for the black-boxes to "
           "converge within tolerance. Quitting early.\n");
    return true;
  }

  uint32_t i, l = countOtherVariables();
  double RSS = 0, t;

#if 0
  printf("Computing RSS with...\n");
  for (i = 0; i < countOtherVariables(); i++)
    printf(" * OTHERVARIABLES[%u] = %g\n", i, VARIABLES[i]);
  for (i = 0; i < countOutputVariables(); i++)
    printf(" * OUTPUTS[%u] = %g\n", i, OUTPUTS[i]);
  printf("Listing complete.\n");
#endif

  for (i = 0; i < l; i++)
  {
    t = VARIABLES[i] - BACKVARIABLES[i];
    RSS += t * t;
  }
  l = countOutputVariables();
  for (i = 0; i < l; i++)
  {
    t = OUTPUTS[i] - BACKOUTPUTS[i];
    RSS += t * t;
  }
  
#if 0
  printf("Step %u: RSS = %g\n", mConvergeSteps, RSS);
#endif

  if (RSS < THRESHOLD)
    return true;
  
  return false;
}

void
MixedModelBase::createBlackBox
(
 uint32_t aIdx,
 const wchar_t* URI,
 uint32_t inputs,
 uint32_t outputs
)
{
  if (gBlackBoxCache)
  {
    BlackBoxModelBase* cachedModel = gBlackBoxCache->FindModelInCache(URI);
    if (cachedModel != NULL)
    {
      blackBoxes[aIdx] = cachedModel;

      char* utf8uri = BBArun_wchar_to_UTF8(URI);
      blackBoxNames[aIdx] = utf8uri;
      free(utf8uri);

      return;
    }
  }

  const wchar_t* offs = wcschr(URI, '#');
  std::wstring name, id;
  if (offs == NULL)
    name = URI;
  else
    name = std::wstring(URI, offs - URI);
  
  if (offs != NULL)
    id = offs + 1;

  std::map<std::wstring, BlackBoxModelFactory*>::iterator i
    = blackBoxFactories().find(name);
  
  if (i == blackBoxFactories().end())
  {
    printf("Warning: Cannot create a black box with URI base %S because "
           "no factory could be found.\n",
           name.c_str());
    throw std::exception();
  }
  
  blackBoxes[aIdx] = (*i).second->createBlackBoxModel(id, inputs, outputs);
  char* utf8uri = BBArun_wchar_to_UTF8(URI);
  blackBoxNames[aIdx] = utf8uri;
  free(utf8uri);

  std::string data;
  mPersister->restoreState(blackBoxNames[aIdx].c_str(), data);
  if (data != "")
    blackBoxes[aIdx]->Deserialise(data);

  if (gBlackBoxCache != NULL)
    gBlackBoxCache->CacheModel(URI, blackBoxes[aIdx]);
}

std::map<std::wstring,BlackBoxModelFactory*>* MixedModelBase::sBlackBoxFactories = NULL;

class StandardBlackBoxCache
  : public BlackBoxCache
{
public:
  StandardBlackBoxCache()
  {
  }

  ~StandardBlackBoxCache()
  {
    DiscardCache();
  }

  BlackBoxModelBase*
  FindModelInCache(const std::wstring& aURI)
  {
    std::map<std::wstring, BlackBoxModelBase*>::iterator i = mModels.find(aURI);

    if (i == mModels.end())
      return NULL;

    return (*i).second;
  }

  void
  CacheModel(const std::wstring& aURI, BlackBoxModelBase* aModel)
  {
    mModels.insert(std::pair<std::wstring, BlackBoxModelBase*>(aURI, aModel));
  }

  void
  SaveAllCachedModels()
  {
    std::map<std::wstring, BlackBoxModelBase*>::iterator i;
    std::string state;

    SQLModelPersister smp;

    for (i = mModels.begin(); i != mModels.end(); i++)
    {
      (*i).second->Serialise(state);
      if (state.size() != 0)
      {
        char* utf8uri = BBArun_wchar_to_UTF8((*i).first.c_str());
        smp.saveState(utf8uri, state);
        free(utf8uri);
      }
    }
  }

  void
  DiscardCache()
  {
    std::map<std::wstring, BlackBoxModelBase*>::iterator i;
    for (i = mModels.begin(); i != mModels.end(); i++)
      delete (*i).second;
    mModels.clear();
  }
private:
  std::map<std::wstring, BlackBoxModelBase*> mModels;
};

void setupStandardBlackBoxCache()
{
  gBlackBoxCache = new StandardBlackBoxCache();
}
