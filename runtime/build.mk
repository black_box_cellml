lib_LTLIBRARIES += libMixedBoxRuntime.la
bin_PROGRAMS += runbb DoCrossValidation BuildTrainingDatabase BootstrapPValue \
	ComputeRSSByLevels ComputeAverageRSS ComputeAverageRMS \
        OptimiseSVMParameters SuperviseSVMOptimisation
libMixedBoxRuntime_la_SOURCES = \
  runtime/BlackBoxRuntime.cpp \
  runtime/BlackBoxVM.cpp \
  runtime/SupportVectorMachines.cpp
  # runtime/KNearestNeighbours.cpp

runbb_SOURCES = runtime/RunBB.cpp

libMixedBoxRuntime_la_CXXFLAGS = \
  -I$(top_srcdir) -I$(top_srcdir)/sources -I $(top_srcdir)/sources/BlaBoC \
  -I $(top_srcdir)/runtime -I$(CELLML_API_DIR)/sources -I$(CELLML_API_DIR) \
  -I$(CELLML_API_DIR)/interfaces \
  $(AM_CXXFLAGS)

# XXX need to get the right suffix...
libMixedBoxRuntime_la_LIBADD = -lsvm

runbb_CXXFLAGS = \
  -I$(top_srcdir) -I$(top_srcdir)/sources -I $(top_srcdir)/sources/BlaBoC \
  -I $(top_srcdir)/runtime -I$(CELLML_API_DIR)/sources -I$(CELLML_API_DIR) \
  -I$(CELLML_API_DIR)/interfaces \
  $(AM_CXXFLAGS)

runbb_LDADD = libMixedBoxRuntime.la -lmysqlpp

DoCrossValidation_SOURCES = runtime/DoCrossValidation.cpp

DoCrossValidation_CXXFLAGS = \
  -I$(top_srcdir) -I$(top_srcdir)/sources -I $(top_srcdir)/sources/BlaBoC \
  -I $(top_srcdir)/runtime -I$(CELLML_API_DIR)/sources -I$(CELLML_API_DIR) \
  -I$(CELLML_API_DIR)/interfaces \
  $(AM_CXXFLAGS)

DoCrossValidation_LDADD = libMixedBoxRuntime.la -lmysqlpp

BuildTrainingDatabase_SOURCES = runtime/BuildTrainingDatabase.cpp
BuildTrainingDatabase_CXXFLAGS = $(AM_CXXFLAGS) -I$(top_srcdir)/runtime -ggdb -O0
BuildTrainingDatabase_LDADD = -lmysqlpp

BootstrapPValue_SOURCES = runtime/BootstrapPValue.cpp
BootstrapPValue_CXXFLAGS = $(AM_CXXFLAGS) -I$(CELLML_API_DIR)/sources \
  -I$(CELLML_API_DIR) -I$(CELLML_API_DIR)/interfaces -I$(top_srcdir)/runtime
BootstrapPValue_LDADD = -lmysqlpp

ComputeRSSByLevels_SOURCES = runtime/ComputeRSSByLevels.cpp
ComputeRSSByLevels_CXXFLAGS = $(AM_CXXFLAGS) -I$(CELLML_API_DIR)/sources \
  -I$(CELLML_API_DIR) -I$(CELLML_API_DIR)/interfaces -I$(top_srcdir)/runtime
ComputeRSSByLevels_LDADD = -lmysqlpp

ComputeAverageRSS_SOURCES = runtime/ComputeAverageRSS.cpp
ComputeAverageRSS_CXXFLAGS = $(AM_CXXFLAGS) -I$(CELLML_API_DIR)/sources \
  -I$(CELLML_API_DIR) -I$(CELLML_API_DIR)/interfaces -I$(top_srcdir)/runtime
ComputeAverageRSS_LDADD = -lmysqlpp

ComputeAverageRMS_SOURCES = runtime/ComputeAverageRMS.cpp
ComputeAverageRMS_CXXFLAGS = $(AM_CXXFLAGS) -I$(CELLML_API_DIR)/sources \
  -I$(CELLML_API_DIR) -I$(CELLML_API_DIR)/interfaces -I$(top_srcdir)/runtime
ComputeAverageRMS_LDADD = -lmysqlpp

OptimiseSVMParameters_SOURCES = runtime/OptimiseSVMParameters.cpp
OptimiseSVMParameters_CXXFLAGS = $(AM_CXXFLAGS) -I$(CELLML_API_DIR)/sources \
  -I$(CELLML_API_DIR) -I$(CELLML_API_DIR)/interfaces -I$(top_srcdir)/runtime
OptimiseSVMParameters_LDADD = -lmysqlpp libMixedBoxRuntime.la

SuperviseSVMOptimisation_SOURCES = runtime/SuperviseSVMOptimisation.cpp
SuperviseSVMOptimisation_LDADD = -lmysqlpp
