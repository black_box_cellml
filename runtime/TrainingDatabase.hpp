#include <inttypes.h>

struct TrainingDatabaseHeader
{
  uint32_t first_variant_id;
  uint32_t nvariants;
  uint32_t ninput_signals;
  uint32_t noutput_signals;
};
