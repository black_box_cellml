#include "TrainingDatabase.hpp"
#include <string>
#include <fcntl.h>
#include "Utilities.hxx"
#include <mysql++/mysql++.h>

class CompAvgException
  : public std::exception
{
public:
  CompAvgException(const std::string& aWhat)
    : mWhat(aWhat)
  {
  }

  ~CompAvgException() throw()
  {
  }

  const char* what() const throw()
  {
    return mWhat.c_str();
  }

private:
  std::string mWhat;
};

class AverageComputer
{
public:
  AverageComputer(mysqlpp::Connection& aConn, const char* aFilename)
    : mConn(aConn), mAnswers(NULL), mSum(NULL), mTemp(NULL)
  {
    mFile = open(aFilename, O_RDONLY | O_LARGEFILE);
    if (mFile == -1)
      throw CompAvgException("Can't open training file.");

    if (read(mFile, &mTdh, sizeof(TrainingDatabaseHeader)) != sizeof(TrainingDatabaseHeader))
      throw CompAvgException("Can't read the training file.");

    mJump = mTdh.ninput_signals * sizeof(double);

    mAnswers = new double[mTdh.noutput_signals];
    mSum = new double[mTdh.noutput_signals];
    mTemp = new double[mTdh.noutput_signals];
  }

  ~AverageComputer()
  {
    if (mFile != -1)
      close(mFile);
    if (mAnswers)
      delete [] mAnswers;
    if (mTemp)
      delete [] mTemp;
    if (mSum)
      delete [] mSum;
  }

  void
  computeRMSForAllVariants()
  {
    uint32_t v = mTdh.first_variant_id;
    uint32_t lv = v + mTdh.nvariants;

    for (; v < lv; v++)
    {
      double RMS = computeRMSFromAverageExcluding(v);
      mysqlpp::Query q(mConn.query());
      q << "UPDATE crossval_rss SET average_rss='" << RMS
        << "' WHERE leftout_variant_id='" << v << "'";
      q.execute();
    }
  }

  double
  computeRMSFromAverageExcluding(uint32_t aExclude)
  {
    if (aExclude < mTdh.first_variant_id)
      throw CompAvgException("Excluded variant out of range.");

    aExclude -= mTdh.first_variant_id;
    if (aExclude >= mTdh.nvariants)
      throw CompAvgException("Excluded variant out of range.");

    lseek64(mFile, sizeof(mTdh), SEEK_SET);
    memset(mSum, 0, sizeof(double) * mTdh.noutput_signals);

    uint32_t i(0);
    for (; i < mTdh.nvariants; i++)
    {
      // Skip the inputs...
      if (mJump != 0)
        lseek64(mFile, mJump, SEEK_CUR);

      double * into = (aExclude == i) ? mAnswers : mTemp;

      if (read(mFile, into, sizeof(double) * mTdh.noutput_signals) != 
          sizeof(double) * mTdh.noutput_signals)
        throw CompAvgException("Couldn't load signals.");

      if (into == mTemp)
      {
        uint32_t j;
        for (j = 0; j < mTdh.noutput_signals; j++)
          mSum[j] += mTemp[j];
      }
    }
    
    double factor = 1.0 / (mTdh.nvariants - 1);

    // RSS isn't robust against a few outliers, so use residual median of
    // squares instead.
    std::vector<double> residualsSq(mTdh.noutput_signals);
    for (i = 0; i < mTdh.noutput_signals; i++)
    {
      double t = mAnswers[i] - factor * mSum[i];
      residualsSq[i] = t * t;
    }
    std::sort(residualsSq.begin(), residualsSq.end());

    double rms;

    if ((mTdh.noutput_signals) & 1 == 1)
    {
      rms = residualsSq[mTdh.noutput_signals / 2];
    }
    else
    {
      uint32_t ofs = mTdh.noutput_signals / 2;
      rms = (residualsSq[ofs - 1] + residualsSq[ofs]) / 2;
    }

    return rms;
  }

private:
  int mFile;
  TrainingDatabaseHeader mTdh;
  mysqlpp::Connection& mConn;
  double* mAnswers, * mSum, * mTemp;
  off64_t mJump;
};

int
main(int argc, char** argv)
{
  if (argc < 6)
  {
    printf("Usage: ComputeAverageRMS db_host db_user db_password db_database trainingfile\n");
    // printf("Set variant=-1 if RMS is sum for all variants.\n");
    return 1;
  }

  try
  {
    mysqlpp::Connection conn(mysqlpp::use_exceptions);
    conn.connect(argv[4], argv[1], argv[2], argv[3]);

    AverageComputer ac(conn, argv[5]);
    ac.computeRMSForAllVariants();
  }
  catch (std::exception& e)
  {
    printf("Error: %s\n", e.what());
  }
}
