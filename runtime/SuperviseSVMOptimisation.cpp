#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <inttypes.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <exception>
#include <mysql++/mysql++.h>
#include <sys/time.h>

class CVException
  : public std::exception
{
public:
  CVException(const std::string& aMessage)
    : mMessage(aMessage)
  {
  }

  ~CVException() throw()
  {
  }

  const char* what() const throw()
  {
    return mMessage.c_str();
  }

private:
  std::string mMessage;
};

class SVMSupervisor
{
public:
  SVMSupervisor(int argc, char** argv)
    : mConn(mysqlpp::use_exceptions)
  {
    if (argc < 6)
    {
      throw CVException("Usage: SuperviseSVMOptimisation db_host db_user "
                        "db_password db_database training.dat");
    }

    mConn.connect(argv[4], argv[1], argv[2], argv[3]);
  }

  int
  RunOptimisationProgram(int argc, char** argv)
  {
    char** newArgv = new char*[argc + 1];
    char* p = strrchr(argv[0], '/');
    if (p == NULL)
      argv[0] = strdup(".libs/lt-OptimiseSVMParameters");
    else
    {
      newArgv[0] = reinterpret_cast<char*>
        (malloc(p - argv[0] + 1 + sizeof(".libs/lt-OptimiseSVMParameters")));
      memcpy(newArgv[0], argv[0], p - argv[0] + 1);
      strcpy(newArgv[0] + (p - argv[0]) + 1, ".libs/lt-OptimiseSVMParameters");
    }

    // Copy the terminating NULL too...
    memcpy(newArgv + 1, argv + 1, argc * sizeof(char*));

    int forkval = fork();
    if (forkval == 0)
    {
      execv(newArgv[0], newArgv);
      // Just in case execv fails...
      perror("execv");
      exit(10);
    }
    else if (forkval < 0)
    {
      perror("fork");
      exit(10);
    }

    free(newArgv[0]);
    delete [] newArgv;

    uint32_t lastProgress = CheckOptimisationProgress();
    sigset_t sset;
    sigemptyset(&sset);
    sigaddset(&sset, SIGCHLD);
    sigprocmask(SIG_BLOCK, &sset, NULL);
    struct timespec timeout;
    timeout.tv_sec = 10 * 60;
    timeout.tv_nsec = 0;
    struct timeval tlast, tnow;
    
    gettimeofday(&tlast, NULL);

    int status;
    while (true)
    {
      if (waitpid(forkval, &status, WNOHANG) > 0)
        return status;
      
      sigtimedwait(&sset, NULL, &timeout);
      
      if (waitpid(forkval, &status, WNOHANG) > 0)
        return status;
      
      gettimeofday(&tnow, NULL);
      if (tnow.tv_sec - tlast.tv_sec < 10*60 - 5)
      {
        timeout.tv_sec = 10*60 - (tnow.tv_sec - tlast.tv_sec);
        continue;
      }
      else
      {
        timeout.tv_sec = 10 * 60;
        tlast.tv_sec = tnow.tv_sec;
        tlast.tv_usec = tnow.tv_usec;
      }

      uint32_t progress = CheckOptimisationProgress();
      if (progress > lastProgress)
        lastProgress = progress;
      else
      {
        kill(forkval, SIGUSR1);
        sleep(2);
        kill(forkval, SIGKILL);
        waitpid(forkval, &status, 0);
        return 10;
      }
    }
  }
private:
  mysqlpp::Connection mConn;

  uint32_t
  CheckOptimisationProgress()
  {
    mysqlpp::Query q(mConn.query());
    q << "SELECT COUNT(*) FROM svm_optimisation";
    mysqlpp::Result res(q.store());
    return res.at(0)[(unsigned int)0];
  }
};

int
main(int argc, char** argv)
{
  int result = 0;

  try
  {
    SVMSupervisor ss(argc, argv);

    while (true)
    {
      result = ss.RunOptimisationProgram(argc, argv);
      if (result != 10)
        break;
    }
  }
  catch (std::exception& e)
  {
    printf("Error: %s\n", e.what());
    return 1;
  }

  return result;
}
