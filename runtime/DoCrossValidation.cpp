#include <stdio.h>
#include "BlackBoxRuntime.hpp"
#include <mysql++/mysql++.h>
#include <memory>
#include <sys/errno.h>
#include "TrainingDatabase.hpp"

class CVException
  : public std::exception
{
public:
  CVException(const std::string& aMessage)
    : mMessage(aMessage)
  {
  }

  ~CVException() throw()
  {
  }

  const char* what() const throw()
  {
    return mMessage.c_str();
  }

private:
  std::string mMessage;
};

template<typename I, typename D>
class IndexByArrayComparator
{
public:
  IndexByArrayComparator(D* aData)
    : mData(aData)
  {
  }

  bool
  operator()(const I v1, const I v2) const
  {
    return (mData[v1] < mData[v2]);
  }

private:
  D* mData;
};

class CrossValidator
{
public:
  CrossValidator(int argc, char** argv)
    : mConn(mysqlpp::use_exceptions)
  {
    if (argc < 6)
    {
      throw CVException("Usage: DoCrossValidation db_host db_user "
                        "db_password db_database training.dat");
    }

    mConn.connect(argv[4], argv[1], argv[2], argv[3]);
    setenv("BLABOC_HOST", argv[1], 0);
    setenv("BLABOC_USER", argv[2], 0);
    setenv("BLABOC_PASSWORD", argv[3], 0);
    setenv("BLABOC_DB", argv[4], 0);

    mFile = fopen(argv[5], "r");
    if (mFile == NULL)
    {
      throw CVException(strerror(errno));
    }

    if (fread(&mTdh, sizeof(mTdh), 1, mFile) != 1)
    {
      throw CVException(strerror(errno));
    }

    mCumulativeError = new double[mTdh.noutput_signals];
    memset(mCumulativeError, 0, sizeof(double) * mTdh.noutput_signals);
  }

  ~CrossValidator()
  {
    if (mFile != NULL)
      fclose(mFile);
    delete [] mCumulativeError;
  }

  void
  StartCrossValidation()
  {
    mysqlpp::Query q(mConn.query());
    q << "SELECT id, name FROM variants ORDER BY name";
    mVariants = q.store();

    uint32_t i = 0, l = mVariants.size();
    for (; i < l; i++)
    {
      mysqlpp::Row r = mVariants.at(i);
      uint32_t variantId = r["id"];
      mysqlpp::const_string variantName = r["name"];
      DoSingleValidation(variantId, variantName.c_str());
      ReportCumulativeError();
    }
  }

private:
  mysqlpp::Connection mConn;
  mysqlpp::Result mVariants;
  double* mCumulativeError;

  void
  WipeAllTraining()
  {
    if (gBlackBoxCache != NULL)
      gBlackBoxCache->DiscardCache();

    mysqlpp::Query q(mConn.query());
    q << "DELETE FROM black_box_state";
    q.execute();
  }

  void
  DoSingleValidation(uint32_t variantId, const char* variantName)
  {
    // Create the model...
    std::string fn = variantName;
    fn += ".obj";

    printf("Cross-validating variant %s\n", variantName);
    WipeAllTraining();
    DoTraining(variantId);

    std::auto_ptr<MixedModelBase> mmb(createMixedModelVM(fn.c_str()));
    if (mmb.get() == NULL)
      throw CVException("Can't load model to test.");

    mmb->setup();
    
    TestModel(mmb.get(), variantId);
  }

  void
  DoTraining(uint32_t aExcludeVariant)
  {
    uint32_t i = 0, l = mVariants.size();

    for (; i < l; i++)
    {
      mysqlpp::Row r = mVariants.at(i);
      uint32_t variantId = r["id"];

      if (variantId == aExcludeVariant)
        continue;

      mysqlpp::const_string variantName = r["name"];

      std::string fn(variantName.c_str(), variantName.length());
      fn += ".obj";
      printf("  Training for variant using data for %s\n", variantName.c_str());
      std::auto_ptr<MixedModelBase> mmb(createMixedModelVM(fn.c_str()));
      if (mmb.get() == NULL)
        throw CVException("Can't load model to train.");

      mmb->setup();

      TrainSingleModel(mmb.get(), variantId);
    }
  }

  class DoubleArrayCleaner
  {
  public:
    DoubleArrayCleaner(double* aD)
      : mD(aD)
    {
    }

    ~DoubleArrayCleaner()
    {
      if (mD != NULL)
        delete [] mD;
    }

    void dontDelete()
    {
      mD = NULL;
    }

  private:
    double* mD;
  };

  void
  CreatePopulatedArrays(MixedModelBase* aModel,
                        uint32_t aVariant,
                        double** aIn,
                        double** aOut)
  {
    uint32_t varOff = aVariant - mTdh.first_variant_id;
    if (varOff >= mTdh.nvariants)
      throw CVException("Variant offset outside the permissible range.");
    uint64_t varOff64 = varOff;
    varOff64 *= mTdh.ninput_signals + mTdh.noutput_signals;
    varOff64 *= sizeof(double);
    varOff64 += sizeof(TrainingDatabaseHeader);
    fseeko(mFile, varOff64, SEEK_SET);

    uint32_t iolims[] = {aModel->countInputVariables(),
                         aModel->countOutputVariables() };

    double* in = new double[iolims[0] + 1];
    double* out = new double[iolims[1] + 1];
    DoubleArrayCleaner dac1(in);
    DoubleArrayCleaner dac2(out);

    if (fread(in, iolims[0] * sizeof(double), 1, mFile) < 0)
      throw CVException("Can't read inputs array.");
    if (fread(out, iolims[1] * sizeof(double), 1, mFile) < 0)
      throw CVException("Can't read outputs array.");

    *aIn = in;
    *aOut = out;
    dac1.dontDelete();
    dac2.dontDelete();
  }

  void
  TrainSingleModel(MixedModelBase* aModel, uint32_t aTrainVariant)
  {
    double * in, * out;
    CreatePopulatedArrays(aModel, aTrainVariant, &in, &out);
    DoubleArrayCleaner dac1(in), dac2(out);

    // We now have complete training data. Train using it...
    aModel->train(in, out);
  }

  void
  TestModel(MixedModelBase* aModel, uint32_t aTestVariant)
  {
    double * in, * realOut, * modelledOut;
    CreatePopulatedArrays(aModel, aTestVariant, &in, &realOut);
    DoubleArrayCleaner dac1(in), dac2(realOut);

    modelledOut = new double[aModel->countOutputVariables() + 1];
    DoubleArrayCleaner dac3(modelledOut);

    // We now have complete training data. Train using it...
    aModel->run(in, modelledOut);

    StoreRSS(aTestVariant, modelledOut, realOut,
             aModel->countOutputVariables());
  }

  void
  ReportCumulativeError()
  {
    mysqlpp::Query q(mConn.query());

    q << "SELECT g.name, v.gene FROM training_data AS td, variables AS v, genes AS g "
      "WHERE td.variant_id=" << mTdh.first_variant_id << " AND td.variable_id=v.id "
      " AND g.locus=v.gene "
      "ORDER BY td.array_index";
    mysqlpp::Result r(q.store());
    if (r.num_rows() != mTdh.noutput_signals)
    {
      std::cout << "Error: training_data table doesn't match training.dat or "
        "is invalid (wrong number of results retrieved). Not listing "
        "cumulative error report as it would be wrong."
                << std::endl;
      return;
    }

    std::vector<int32_t> errorRanks(mTdh.noutput_signals, 0);
    int32_t i;
    for (i = 0; i < mTdh.noutput_signals; i++)
      errorRanks[i] = i;

    IndexByArrayComparator<int32_t, double> ibac(mCumulativeError);

    std::sort(errorRanks.begin(), errorRanks.end(), ibac);

    std::cout << "=== Begin cumulative error ranked gene list ==="
              << std::endl;
    for (i = mTdh.noutput_signals - 1; i >= 0; i--)
    {
      std::string geneName(r.at(errorRanks[i]).at(0));
      std::string locusName(r.at(errorRanks[i]).at(1));
      std::cout << "Gene: " << geneName << ", Locus: " << locusName
                << ", cumulative error: "
                << mCumulativeError[errorRanks[i]] << std::endl;
    }

    std::cout << "=== End cumulative error ranked gene list ==="
              << std::endl;
  }

  void
  UpdateCumulativeError(const double* O, const double* E)
  {
    uint32_t i;
    for (i = 0; i < mTdh.noutput_signals; i++)
    {
      double scale = fabs(E[i]);
      
      // Very small values get normalised to positive values.
      if (scale < 0.01)
        scale = 0.01;

      mCumulativeError[i] += (O[i] - E[i]) * (O[i] - E[i]) / scale;
    }
  }

  void
  StoreRSS(uint32_t aVariant, double* aX, double* aXHat, uint32_t n)
  {
    uint32_t i;

    // RSS isn't robust against a few outliers, so use residual median of
    // squares instead.
    std::vector<double> residualsSq(n);
    for (i = 0; i < n; i++)
    {
      double t = aX[i] - aXHat[i];
      residualsSq[i] = t * t;
    }
    
    UpdateCumulativeError(aX, aXHat);

    std::sort(residualsSq.begin(), residualsSq.end());

    double rms;

    if (n & 1 == 1)
    {
      rms = residualsSq[n / 2];
    }
    else
    {
      uint32_t ofs = n / 2;
      rms = (residualsSq[ofs - 1] + residualsSq[ofs]) / 2;
    }

    mysqlpp::Query q(mConn.query());
    printf("Saving RMS (%g)\n", rms);
    q << "INSERT INTO crossval_rss (leftout_variant_id, rss) VALUES ("
      << aVariant << ", '" << rms << "')";
    // q.execute();
  }

  FILE* mFile;
  struct TrainingDatabaseHeader mTdh;
};

int
main(int argc, char** argv)
{
  try
  {
    setupStandardBlackBoxCache();
    CrossValidator cv(argc, argv);
    cv.StartCrossValidation();
  }
  catch (std::exception& e)
  {
    printf("Error: %s\n", e.what());
    return 1;
  }
}
