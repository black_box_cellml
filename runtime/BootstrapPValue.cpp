#include "TrainingDatabase.hpp"
#include <string>
#include <fcntl.h>
#include "Utilities.hxx"
#include <mysql++/mysql++.h>

class BootstrapException
  : public std::exception
{
public:
  BootstrapException(const std::string& aWhat)
    : mWhat(aWhat)
  {
  }

  ~BootstrapException() throw()
  {
  }

  const char* what() const throw()
  {
    return mWhat.c_str();
  }

private:
  std::string mWhat;
};

class Bootstrapper
{
public:
  Bootstrapper(const char* aFilename)
    : mAnswers(NULL)
  {
    mFile = open(aFilename, O_RDONLY | O_LARGEFILE);
    if (mFile == -1)
      throw BootstrapException("Can't open training file.");

    if (read(mFile, &mTdh, sizeof(TrainingDatabaseHeader)) != sizeof(TrainingDatabaseHeader))
      throw("Can't read the training file.");

    mOffset = sizeof(TrainingDatabaseHeader) +
      mTdh.ninput_signals * sizeof(double);
    mMultiplier = sizeof(double) * (mTdh.ninput_signals + mTdh.noutput_signals);
    mAnswers = new double[mTdh.noutput_signals];

    mersenne_autoseed();
  }

  ~Bootstrapper()
  {
    if (mFile != -1)
      close(mFile);
    if (mAnswers)
      delete [] mAnswers;
  }

  double
  computePValue(double RSS, int whichVariant, double& h0mean)
  {
    uint32_t nsteps = 0, nleq = 0;
    double RSSsum = 0.0;

    if (whichVariant != -1)
      loadAnswers((uint32_t)whichVariant);

    while (nsteps < 10000)
    {
      // if (nsteps > 1000 && nleq > 10)
      //  break;
      
      double bsRSS = sample(whichVariant);
      RSSsum += bsRSS;
      if (bsRSS <= RSS)
        nleq++;
      nsteps++;
    }

    h0mean = RSSsum / nsteps;

    return (nleq + 0.0) / nsteps;
  }
  
private:
  int mFile;
  TrainingDatabaseHeader mTdh;

  uint32_t mOffset, mMultiplier;

  double
  sample(int whichVariant)
  {
    if (whichVariant != -1)
      return sampleOne((uint32_t)whichVariant);
    else
    {
      uint32_t i = mTdh.first_variant_id;
      uint32_t e = i + mTdh.nvariants;
      double sum = 0.0;
      for (; i < e; i++)
      {
        loadAnswers(i);
        sum += sampleOne(i);
      }
      return sum;
    }
  }

  void
  loadAnswers(uint32_t whichVariant)
  {
    if (whichVariant < mTdh.first_variant_id ||
        whichVariant - mTdh.first_variant_id >= mTdh.nvariants)
      throw BootstrapException("Invalid variant ID.");
    whichVariant -= mTdh.first_variant_id;
    uint64_t offs = whichVariant;
    offs *= mMultiplier;
    offs += mOffset;
    lseek64(mFile, offs, SEEK_SET);
    if (read(mFile, mAnswers, sizeof(double) * mTdh.noutput_signals) !=
        sizeof(double) * mTdh.noutput_signals)
      throw BootstrapException("Can't read variant array");
  }

  double
  sampleOne(uint32_t leaveOutVariant)
  {
    double RSS = 0.0;

    uint32_t i;
    for (i = 0; i < mTdh.noutput_signals; i++)
    {
      double resid = mAnswers[i] - sampleOneVariable(i, leaveOutVariant);
      RSS += resid * resid;
    }

    return RSS;
  }

  double
  sampleOneVariable(uint32_t variable, uint32_t notVariant)
  {
    while (true)
    {
      uint32_t chosenVariant = mersenne_genrand_int32() % mTdh.nvariants;
      if (chosenVariant == notVariant)
        continue;

      uint64_t offs = chosenVariant;
      offs *= mMultiplier;
      offs += mOffset;
      offs += sizeof(double) * variable;
      lseek64(mFile, offs, SEEK_SET);

      double val;
      if (read(mFile, &val, sizeof(double)) != sizeof(double))
        throw BootstrapException("Can't read variable");
      
      return val;
    }
  }

  double * mAnswers;
};

int
main(int argc, char** argv)
{
  if (argc < 6)
  {
    printf("Usage: BootstrapPValue db_host db_user db_password db_database trainingfile\n");
    // printf("Set variant=-1 if RSS is sum for all variants.\n");
    return 1;
  }

  try
  {
    mysqlpp::Connection conn(mysqlpp::use_exceptions);
    conn.connect(argv[4], argv[1], argv[2], argv[3]);

    mysqlpp::Query q(conn.query());

    q << "SELECT leftout_variant_id, rss FROM crossval_rss";
    mysqlpp::Result res(q.store());

    Bootstrapper bs(argv[5]);

    double h0mean;

    uint32_t i, l = res.size();

    for (i = 0; i < l; i++)
    {
      uint32_t variant = res.at(i).at(0);
      double RSS = res.at(i).at(1);

      double p = bs.computePValue(RSS, variant, h0mean);
      printf("Variant = %u, p = %g, mean RSS under H0 = %g\n", variant, p,
             h0mean);

      q.reset();
      q << "UPDATE crossval_rss SET null_rss='" << h0mean << "', pval='"
        << p << "' WHERE leftout_variant_id='" << variant << "'";
      q.execute();
    }
  }
  catch (std::exception& e)
  {
    printf("Error: %s\n",
           e.what());
  }
}
