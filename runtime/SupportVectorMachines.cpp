#include "BlackBoxRuntime.hpp"
#include "SVMParams.hpp"

class SupportVectorMachine
  : public BlackBoxModelBase
{
public:
  SupportVectorMachine
  (
   std::wstring& aID,
   uint32_t aInputs,
   uint32_t aOutputs
  )
    : mInputs(aInputs), mOutputs(aOutputs),
      mProblem(NULL), mModel(NULL)
  {
    if (aOutputs != 1)
      throw std::exception();

    mProblem = reinterpret_cast<svm_problem*>(malloc(sizeof(svm_problem)));
    mProblem->l = 0;
    mProblem->y = reinterpret_cast<double*>(malloc(sizeof(double) * 10));
    mProblem->x = reinterpret_cast<svm_node**>(malloc(sizeof(svm_node*) * 10));
    memset(mProblem->x, 0, sizeof(svm_node*) * 10);
    mProblemCapacity = 10;
  }

  ~SupportVectorMachine();

  void Serialise(std::string& storeIn);
  void Deserialise(const std::string& aInput);
  void train(double * IN, double * OUT);
  void run(double * IN, double * OUT);
  double GetAverageInput(uint32_t index);

private:
  void EnsureProblemCapacity(uint32_t n);
  void DestroyProblem();

  uint32_t mInputs, mOutputs, mProblemCapacity;
  double mAverage;

  svm_problem* mProblem;
  struct svm_model* mModel;
  svm_problem* mProblem;
};

SupportVectorMachine::~SupportVectorMachine()
{
  if (mModel)
    svm_destroy_model(mModel);

  if (mProblem)
    DestroyProblem();
}

void
SupportVectorMachine::Serialise(std::string& storeIn)
{
  char sfn[17] = "/tmp/svmt.XXXXXX";
  int fd = mkstemp(sfn);
  FILE* f = fdopen(fd, "r");

  svm_save_model(sfn, mModel);

  char buf[1024];
  uint32_t l;

  while ((l = fread(buf, 1, 1024, f)) != 0)
    storeIn += std::string(buf, l);

  unlink(sfn);
  fclose(f);
}

void
SupportVectorMachine::Deserialise(const std::string& storeIn)
{
  char sfn[17] = "/tmp/svmt.XXXXXX";
  int fd = mkstemp(sfn);

  FILE* f = fdopen(fd, "w");
  fwrite(storeIn.data(), storeIn.length(), 1, f);

  if (mModel != NULL)
    svm_destroy_model(mModel);

  mModel = svm_load_model(sfn);

  DestroyProblem();

  unlink(sfn);
  fclose(f);
}

void
SupportVectorMachine::train(double * IN, double * OUT)
{
  if (mModel != NULL)
    throw std::exception(); // run already called, can't train now!

  uint32_t i = mProblem->l;
  EnsureProblemCapacity(i + 1);

  mProblem->y[i] = OUT[0];
  mProblem->x[i] = reinterpret_cast<svm_node*>(malloc(sizeof(svm_node) *
                                                      (mInputs + 1)));
  uint32_t j;
  for (j = 0; j < mInputs; j++)
  {
    mProblem->x[i][j].index = j;
    mProblem->x[i][j].value = IN[j];
  }

  mProblem->x[i][mInputs].index = -1;

  mProblem->l++;
}

svm_parameter gSVMParams =
  {
    /* int svm_type; */EPSILON_SVR,
    /* int kernel_type; */RBF,
    /* int degree; for poly */0,
    /* double gamma; for poly/rbf/sigmoid */1E-3,
    /* double coef0; for poly/sigmoid */0.0,
    /* these are for training only */
    /* double cache_size; in MB */100.0,
    /* double eps; stopping criteria */0.001,
    /* double C; for C_SVC, EPSILON_SVR and NU_SVR */100,
    /* int nr_weight; for C_SVC */0,
    /* int *weight_label; for C_SVC */0,
    /* double* weight; for C_SVC */NULL,
    /* double nu; for NU_SVC, ONE_CLASS, and NU_SVR */0.0,
    /* double p; for EPSILON_SVR */0.1,
    /* int shrinking; use the shrinking heuristics */1,
    /* int probability; do probability estimates */0
  };

svm_parameter*
GetSVMParams()
{
  return &gSVMParams;
}

void
SupportVectorMachine::run(double * IN, double * OUT)
{
  if (mModel == NULL)
  {
    if (mProblem == NULL)
    {
      OUT[0] = mAverage;
      return;
    }

    if (mInputs == 0)
    {
      uint32_t i;
      double sum = 0.0;
      for (i = 0; i < mProblem->l; i++)
        sum += mProblem->y[i];
      mAverage = sum / mProblem->l;
      DestroyProblem();
      OUT[0] = mAverage;
      return;
    }
    else
      mModel = svm_train(mProblem, &gSVMParams);
    // DestroyProblem();
  }

  svm_node inputs[mInputs + 1];
  uint32_t i;
  for (i = 0; i < mInputs; i++)
  {
    inputs[i].index = i;
    inputs[i].value = IN[i];
  }
  inputs[mInputs].index = -1;

  OUT[0] = svm_predict(mModel, inputs);
}

double
SupportVectorMachine::GetAverageInput(uint32_t index)
{
  uint32_t i;
  double sum = 0.0;

  if (mProblem == NULL)
    throw std::exception();

  for (i = 0; i < mProblem->l; i++)
    sum += mProblem->x[i][index].value;

  return sum / mProblem->l;
}

void
SupportVectorMachine::DestroyProblem()
{
  free(mProblem->y);
  uint32_t i;
  svm_node* p;
  for (i = 0; i < mProblem->l; i++)
    if (mProblem->x[i] != NULL)
      free(mProblem->x[i]);

  mProblem = NULL;
}

void
SupportVectorMachine::EnsureProblemCapacity(uint32_t n)
{
  if (mProblemCapacity >= n)
    return;

  uint32_t newCapacity = mProblemCapacity * 2;
  if (n > newCapacity)
    newCapacity = n;

  mProblem->y = reinterpret_cast<double*>
    (realloc(mProblem->y, newCapacity * sizeof(double)));
  mProblem->x = reinterpret_cast<svm_node**>
    (realloc(mProblem->x, newCapacity * sizeof(svm_node*)));
  memset(mProblem->x + mProblemCapacity, 0,
         (newCapacity - mProblemCapacity) * sizeof(svm_node*));
  
  mProblemCapacity = newCapacity;
}

class SVMFactory
  : public BlackBoxModelFactory
{
public:
  SVMFactory()
  {
    MixedModelBase::RegisterModelFactory
    (
     // XXX the decision to hard-code the black-box identifier into models is
     //     not necessarily the best long term solution becuase it makes it hard
     //     to change black-box types without recompiling...
     L"http://www.bioeng.auckland.ac.nz/people/miller/black_box/k-nearest-neighbours",
     this
    );
  }

  BlackBoxModelBase*
  createBlackBoxModel
  (
   std::wstring& aID,
   uint32_t aInputs,
   uint32_t aOutputs
  )
  {
    return new SupportVectorMachine(aID, aInputs, aOutputs);
  }
};

SVMFactory gSVMFactory;
