#include <stdio.h>
#include "BlackBoxRuntime.hpp"
#include <mysql++/mysql++.h>
#include <memory>
#include <sys/errno.h>
#include "TrainingDatabase.hpp"
#include "SVMParams.hpp"
#include <assert.h>
#include <signal.h>

char* const* gARGV;

class CVException
  : public std::exception
{
public:
  CVException(const std::string& aMessage)
    : mMessage(aMessage)
  {
  }

  ~CVException() throw()
  {
  }

  const char* what() const throw()
  {
    return mMessage.c_str();
  }

private:
  std::string mMessage;
};

void handle_giveup(int signum);

class ParamOptimiser;

ParamOptimiser* gParamOptimiser;

class ParamOptimiser
{
public:
  ParamOptimiser(int argc, char** argv)
    : mConn(mysqlpp::use_exceptions)
  {
    if (argc < 6)
    {
      throw CVException("Usage: DoCrossValidation db_host db_user "
                        "db_password db_database training.dat");
    }

    mConn.connect(argv[4], argv[1], argv[2], argv[3]);
    setenv("BLABOC_HOST", argv[1], 0);
    setenv("BLABOC_USER", argv[2], 0);
    setenv("BLABOC_PASSWORD", argv[3], 0);
    setenv("BLABOC_DB", argv[4], 0);

    mFile = fopen(argv[5], "r");
    if (mFile == NULL)
    {
      throw CVException(strerror(errno));
    }

    if (fread(&mTdh, sizeof(mTdh), 1, mFile) != 1)
    {
      throw CVException(strerror(errno));
    }
  }

  ~ParamOptimiser()
  {
    if (mFile != NULL)
      fclose(mFile);
  }

  void
  StartOptimisation()
  {
    double minObj = HUGE_VAL, minGamma, minC, minp;

    double gamma, C, p, x;
    printf("Starting coarse grid search...\n");

    // 10 x 10 x 5 course grid.
    for (p = 1E-3; p <= 10; p *= 10.0)
    {
      for (gamma = 1E-10; gamma <= 1E10; gamma *= 100.0)
      {
        for (C = 1E-10; C <= 1E10; C *= 100.0)
        {
          printf("gamma = %g, C = %g, p = %g\n", gamma, C, p);
          x = DoOptimisationRun(gamma, C, p);
          if (x < minObj)
          {
            printf("New minimum found: obj(gamma=%g, C=%g, p=%g) = %g\n",
                   gamma, C, p, x);
            minObj = x;
            minGamma = gamma;
            minC = C;
            minp = p;
          }
        }
      }
    }

    printf("Coarse grid search finished. Optimum: "
           "obj(gamma = %g, C = %g, p = %g) = %g\n", minGamma, minC, minp,
           minObj);

    // 5 x 5 x 5 fine grid 
    double Cstart = minC * 0.01, Cend = minC * 100.0,
      gammastart = minGamma * 0.01, gammaend = minGamma * 100.0,
      pstart = minp * 0.01, pend = minp * 100.0;

    printf("Starting fine grid search...\n");

    for (gamma = gammastart; gamma <= gammaend; gamma *= 6.3096)
      for (C = Cstart; C <= Cend; C *= 6.3096)
        for (p = pstart; p <= pend; p *= 6.3096)
        {
          printf("gamma = %g, C = %g, p = %g\n", gamma, C, p);
          x = DoOptimisationRun(gamma, C, p);
          if (x < minObj)
          {
            printf("New minimum found: obj(gamma=%g, C=%g, p=%g) = %g\n",
                   gamma, C, p, x);
            minObj = x;
            minGamma = gamma;
            minC = C;
            minp = p;
          }
        }

    printf("Fine grid search finished. Optimum: "
           "obj(gamma = %g, C = %g, p = %g) = %g\n", minGamma, minC, minp,
           minObj);
  }

  void
  HandleGiveup()
  {
    mysqlpp::Query q(mConn.query());
    double gamma = GetSVMParams()->gamma;
    double C = GetSVMParams()->C;
    double p = GetSVMParams()->p;

    // We just make up rms1, rms2 and cumulative values to be a value big
    // enough to ensure that they are not the minimum. In general this is not
    // guaranteed, although in practice I have never seen cumulative values
    // that big.
    q << "INSERT INTO svm_optimisation (gamma, c, p, rms1, rms2, cumulative) "
         "VALUES ('" << gamma << "', '" << C << "', '" << p
      << "', '1E20', '1E20', '1E20')";
    q.execute();

    // Restart because we are potentially anywhere in the code an longjmp would
    // not be safe. We do this by returning an exit value of 10, which tells
    // SuperviseSVMOptimisation to restart us.
    exit(10);
  }

private:
  mysqlpp::Connection mConn;
  mysqlpp::Result mVariants;

  double mCumulativeDifference, mRMS1, mRMS2;

  void
  WipeAllTraining()
  {
    if (gBlackBoxCache != NULL)
      gBlackBoxCache->DiscardCache();

    mysqlpp::Query q(mConn.query());
    q << "DELETE FROM black_box_state";
    q.execute();
  }

  double
  DoOptimisationRun(double gamma, double C, double p)
  {
    // See if we have already run this optimisation...
    mysqlpp::Query q(mConn.query());
    q << "SELECT cumulative FROM svm_optimisation WHERE gamma='" << gamma
      << "' AND c='" << C << "' AND p='" << p << "'";
    mysqlpp::Result r = q.store();
    if (r.size() != 0)
    {
      mysqlpp::Row ro = r.at(0);
      mCumulativeDifference = ro["cumulative"];
      return mCumulativeDifference;
    }

    mCumulativeDifference = 0.0;

    GetSVMParams()->gamma = gamma;
    GetSVMParams()->C = C;
    GetSVMParams()->p = p;

    q.reset();
    q << "SELECT id, name FROM variants ORDER BY name";
    mVariants = q.store();

    // We set a 10 minute timeout on each point. If we don't find an answer by
    // then, we assume that we are stuck, assume that we would have got a very
    // bad fit anyway, and so insert an invented big result into the database
    // and restart the process.
    signal(SIGUSR1, handle_giveup);
    gParamOptimiser = this;

    uint32_t i = 0, l = 2;
    for (; i < l; i++)
    {
      mysqlpp::Row r = mVariants.at(i);
      uint32_t variantId = r["id"];
      mysqlpp::const_string variantName = r["name"];

      DoSingleValidation(variantId, variantName.c_str());
    }

    q.reset();
    q << "INSERT INTO svm_optimisation (gamma, c, p, rms1, rms2, cumulative) "
         "VALUES ('" << gamma << "', '" << C << "', '" << p
      << "', '" << mRMS1 << "', '" << mRMS2 << "', '" << mCumulativeDifference
      << "')"
      ;
    q.execute();
    
    return mCumulativeDifference;
  }

  void
  DoSingleValidation(uint32_t variantId, const char* variantName)
  {
    // Create the model...
    std::string fn = variantName;
    fn += ".obj";

    printf("Cross-validating variant %s\n", variantName);
    WipeAllTraining();
    DoTraining(variantId);

    std::auto_ptr<MixedModelBase> mmb(createMixedModelVM(fn.c_str()));
    if (mmb.get() == NULL)
      throw CVException("Can't load model to test.");

    mmb->setup();
    
    TestModel(mmb.get(), variantId);
  }

  void
  DoTraining(uint32_t aExcludeVariant)
  {
    uint32_t i = 0, l = mVariants.size();

    for (; i < l; i++)
    {
      mysqlpp::Row r = mVariants.at(i);
      uint32_t variantId = r["id"];

      if (variantId == aExcludeVariant)
        continue;

      mysqlpp::const_string variantName = r["name"];

      std::string fn(variantName.c_str(), variantName.length());
      fn += ".obj";
      printf("  Training for variant using data for %s\n", variantName.c_str());
      std::auto_ptr<MixedModelBase> mmb(createMixedModelVM(fn.c_str()));
      if (mmb.get() == NULL)
        throw CVException("Can't load model to train.");

      mmb->setup();

      TrainSingleModel(mmb.get(), variantId);
    }
  }

  class DoubleArrayCleaner
  {
  public:
    DoubleArrayCleaner(double* aD)
      : mD(aD)
    {
    }

    ~DoubleArrayCleaner()
    {
      if (mD != NULL)
        delete [] mD;
    }

    void dontDelete()
    {
      mD = NULL;
    }

  private:
    double* mD;
  };

  void
  CreatePopulatedArrays(MixedModelBase* aModel,
                        uint32_t aVariant,
                        double** aIn,
                        double** aOut)
  {
    uint32_t varOff = aVariant - mTdh.first_variant_id;
    if (varOff >= mTdh.nvariants)
      throw CVException("Variant offset outside the permissible range.");
    uint64_t varOff64 = varOff;
    varOff64 *= mTdh.ninput_signals + mTdh.noutput_signals;
    varOff64 *= sizeof(double);
    varOff64 += sizeof(TrainingDatabaseHeader);
    fseeko(mFile, varOff64, SEEK_SET);

    uint32_t iolims[] = {aModel->countInputVariables(),
                         aModel->countOutputVariables() };

    double* in = new double[iolims[0] + 1];
    double* out = new double[iolims[1] + 1];
    DoubleArrayCleaner dac1(in);
    DoubleArrayCleaner dac2(out);

    if (fread(in, iolims[0] * sizeof(double), 1, mFile) < 0)
      throw CVException("Can't read inputs array.");
    if (fread(out, iolims[1] * sizeof(double), 1, mFile) < 0)
      throw CVException("Can't read outputs array.");

    *aIn = in;
    *aOut = out;
    dac1.dontDelete();
    dac2.dontDelete();
  }

  void
  TrainSingleModel(MixedModelBase* aModel, uint32_t aTrainVariant)
  {
    double * in, * out;
    CreatePopulatedArrays(aModel, aTrainVariant, &in, &out);
    DoubleArrayCleaner dac1(in), dac2(out);

    // We now have complete training data. Train using it...
    aModel->train(in, out);
  }

  void
  TestModel(MixedModelBase* aModel, uint32_t aTestVariant)
  {
    double * in, * realOut, * modelledOut;
    CreatePopulatedArrays(aModel, aTestVariant, &in, &realOut);
    DoubleArrayCleaner dac1(in), dac2(realOut);

    modelledOut = new double[aModel->countOutputVariables() + 1];
    DoubleArrayCleaner dac3(modelledOut);

    // We now have complete training data. Train using it...
    aModel->run(in, modelledOut);

    UpdateCostByMRS(aTestVariant, modelledOut, realOut,
                    aModel->countOutputVariables());
  }

  void
  UpdateCostByMRS(uint32_t aVariant, double* aX, double* aXHat, uint32_t n)
  {
    uint32_t i;

    // RSS isn't robust against a few outliers, so use residual median of
    // squares instead.
    std::vector<double> residualsSq(n);
    for (i = 0; i < n; i++)
    {
      double t = aX[i] - aXHat[i];
      residualsSq[i] = t * t;
    }
    std::sort(residualsSq.begin(), residualsSq.end());

    double rms;

    if (n & 1 == 1)
    {
      rms = residualsSq[n / 2];
    }
    else
    {
      uint32_t ofs = n / 2;
      rms = (residualsSq[ofs - 1] + residualsSq[ofs]) / 2;
    }

    double avgRMS;
    // XXX this hard-coding is ugly.
    if (aVariant == 3289)
    {
      mRMS1 = rms;
      avgRMS = 0.256956;
    }
    else if (aVariant == 3194)
    {
      mRMS2 = rms;
      avgRMS = 0.0991742;
    }
    else
      assert(0);


    mCumulativeDifference += rms - avgRMS;
  }

  FILE* mFile;
  struct TrainingDatabaseHeader mTdh;
};

void
handle_giveup(int signum)
{
  gParamOptimiser->HandleGiveup();
}


int
main(int argc, char** argv)
{
  gARGV = argv;
  try
  {
    setupStandardBlackBoxCache();
    ParamOptimiser cv(argc, argv);
    cv.StartOptimisation();
  }
  catch (std::exception& e)
  {
    printf("Error: %s\n", e.what());
    return 1;
  }
}
