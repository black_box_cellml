#include "TrainingDatabase.hpp"
#include <string>
#include <fcntl.h>
#include "Utilities.hxx"
#include <mysql++/mysql++.h>

class KOLevException
  : public std::exception
{
public:
  KOLevException(const std::string& aWhat)
    : mWhat(aWhat)
  {
  }

  ~KOLevException() throw()
  {
  }

  const char* what() const throw()
  {
    return mWhat.c_str();
  }

private:
  std::string mWhat;
};

class LevelPredictor
{
public:
  LevelPredictor(mysqlpp::Connection& aConn, const char* aFilename)
    : mConn(aConn), mAnswers(NULL)
  {
    mFile = open(aFilename, O_RDONLY | O_LARGEFILE);
    if (mFile == -1)
      throw KOLevException("Can't open training file.");

    if (read(mFile, &mTdh, sizeof(TrainingDatabaseHeader)) != sizeof(TrainingDatabaseHeader))
      throw("Can't read the training file.");

    mOffset = sizeof(TrainingDatabaseHeader) +
      mTdh.ninput_signals * sizeof(double);
    mMultiplier = sizeof(double) * (mTdh.ninput_signals + mTdh.noutput_signals);
    mAnswers = new double[mTdh.noutput_signals];
    mNearest = new double[mTdh.noutput_signals];
  }

  ~LevelPredictor()
  {
    if (mFile != -1)
      close(mFile);
    if (mAnswers)
      delete [] mAnswers;
    if (mNearest)
      delete [] mNearest;
  }

  double
  computeRSS(uint32_t variant)
  {
    loadArray(mAnswers, variant);

    // What have we knocked down?
    mysqlpp::Query q(mConn.query());
    q << "SELECT v.id FROM  variants_knockedgenes AS vkg, variables AS v "
      "WHERE vkg.gene=v.gene AND vkg.variant='" << variant << "'";
    mysqlpp::Result res(q.store());

    uint32_t minVariable = res.at(0).at(0);
    uint32_t minVariant = findVariantWithMinimalVariable(minVariable, variant);

    loadArray(mNearest, minVariant);

    uint32_t i;
    double RSSsum = 0.0;

    for (i = 0; i < mTdh.noutput_signals; i++)
    {
      double t = mNearest[i] - mAnswers[i];
      RSSsum += t * t;
    }

    return RSSsum;
  }
  
private:
  int mFile;
  TrainingDatabaseHeader mTdh;
  mysqlpp::Connection& mConn;

  uint32_t mOffset, mMultiplier;

  uint32_t
  findVariantWithMinimalVariable(uint32_t varId, uint32_t excludeVariant)
  {
    mysqlpp::Query q(mConn.query());
    q << "SELECT variant_id FROM training_data WHERE value=(SELECT min(value) "
      "FROM training_data WHERE variant_id != '" << excludeVariant
      << "' AND variable_id='" << varId <<
      "') AND variant_id != '" << excludeVariant << "' AND variable_id='"
      << varId << "'";
    return q.store().at(0).at(0);
  }

  void
  loadArray(double* aDest, uint32_t aVariant)
  {
    if (aVariant < mTdh.first_variant_id ||
        aVariant - mTdh.first_variant_id >= mTdh.nvariants)
      throw KOLevException("Invalid variant ID.");
    aVariant -= mTdh.first_variant_id;
    uint64_t offs = aVariant;
    offs *= mMultiplier;
    offs += mOffset;
    lseek64(mFile, offs, SEEK_SET);
    if (read(mFile, aDest, sizeof(double) * mTdh.noutput_signals) !=
        sizeof(double) * mTdh.noutput_signals)
      throw KOLevException("Can't read variant array");
  }

  double * mAnswers;
  double * mNearest;
};

int
main(int argc, char** argv)
{
  if (argc < 6)
  {
    printf("Usage: ComputeRSSByLevels db_host db_user db_password db_database trainingfile\n");
    // printf("Set variant=-1 if RSS is sum for all variants.\n");
    return 1;
  }

  try
  {
    mysqlpp::Connection conn(mysqlpp::use_exceptions);
    conn.connect(argv[4], argv[1], argv[2], argv[3]);

    mysqlpp::Query q(conn.query());

    q << "SELECT leftout_variant_id FROM crossval_rss";
    mysqlpp::Result res(q.store());

    LevelPredictor lp(conn, argv[5]);

    uint32_t i, l = res.size();
    for (i = 0; i < l; i++)
    {
      uint32_t variant = res.at(i).at(0);
      double rss = lp.computeRSS(variant);
      q << "UPDATE crossval_rss SET nearest_rss='" << rss
        << "' WHERE leftout_variant_id='" << variant << "'";
      q.execute();
    }
  }
  catch (std::exception& e)
  {
    printf("Error: %s\n", e.what());
  }
}
