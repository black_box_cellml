#include "BlackBoxRuntime.hpp"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

static double
random_double_logUniform()
{
  union
  {
    double asDouble;
#ifdef WIN32
    uint16_t asIntegers[4];
#else
    uint32_t asIntegers[2];
#endif
  } X;

#ifdef WIN32
  uint16_t spareRand;
#else
  uint32_t spareRand;
#endif

  do
  {
    spareRand = rand();
#ifdef WIN32
    X.asIntegers[0] = rand() | ((spareRand & 0x1) << 15);
    X.asIntegers[1] = rand() | ((spareRand & 0x2) << 14);
    X.asIntegers[2] = rand() | ((spareRand & 0x4) << 13);
    X.asIntegers[3] = rand() | ((spareRand & 0x8) << 12);
#else
    X.asIntegers[0] = rand() | ((spareRand & 0x1) << 31);
    X.asIntegers[1] = rand() | ((spareRand & 0x2) << 30);
#endif
  }
  while (!isfinite(X.asDouble));

  return X.asDouble;
}

class MixedModelVM
  : public MixedModelBase
{
public:
  MixedModelVM(const char* filename)
    : mFile(NULL), mData(NULL), mCode(NULL), mInitBlackBoxes(NULL),
      mInitConstants(NULL), mRun(NULL), mTrain(NULL), mFileEnd(NULL),
      mInputVariableCount(0), mOutputVariableCount(0), mConstantCount(0),
      mOtherVariableCount(0), mTempInputCount(0), mTempOutputCount(0),
      mBlackBoxCount(0), mnSetJumps(10), mVMFlags(0), mReady(false),
      mReturned(0.0)
  {
    memset(mSetJumps, 0xFF, sizeof(mSetJumps));
    mStack = new uint32_t[1000];
    mStackEnd = mStack + 1000 - 1;

    int fd;
    fd = open(filename, O_RDONLY);
    if (fd == -1)
    {
      perror("Load black/white model VM file");
      return;
    }
    
    struct stat sb;
    if (fstat(fd, &sb) < 0)
    {
      perror("Stat black/white model VM file");
      close(fd);
      return;
    }

    if (!S_ISREG(sb.st_mode))
    {
      fprintf(stderr, "Black/white model VM file isn't a regular file.\n");
      close(fd);
      return;
    }

    mFileSize = sb.st_size;
    mFile = (uint8_t*)mmap(0, sb.st_size, PROT_READ | PROT_WRITE, MAP_PRIVATE, fd, 0);
    if (mFile == NULL)
    {
      perror("mmap black/white model VM file");
      close(fd);
      return;
    }

    close(fd);
    
    // Check the header is right...
    if (mFileSize < 8)
    {
      unmap();
      fprintf(stderr, "Black/white model VM file < 8 bytes!\n");
      return;
    }

    if (memcmp(mFile, "\xB1\xAB\x0C\x00", 4))
    {
      unmap();
      fprintf(stderr, "Black/white model has wrong signature.\n");
      return;
    }

    mHeaderEnd = *reinterpret_cast<uint32_t*>(mFile + 4);
    mHeaderEnd += 8;
    if (mHeaderEnd >= mFileSize)
    {
      unmap();
      fprintf(stderr, "Black/white file truncated in header.\n");
      return;
    }

    mFileEnd = mFile + mFileSize;

    mCode = mFile + mHeaderEnd;
    
    uint8_t * ptr = mFile + 8;
    while (ptr + 8 < mCode)
    {
      uint32_t slen = *reinterpret_cast<uint32_t*>(ptr);
      ptr += 4;
      if (slen + ptr + 4 < ptr || slen + ptr + 4 > mCode)
      {
        unmap();
        fprintf(stderr, "Black/white header has invalid string length.\n");
        return;
      }

      char* str = reinterpret_cast<char*>(ptr);

      ptr += slen;

      uint32_t offset = *reinterpret_cast<uint32_t*>(ptr);
      ptr += 4;

      if (slen == 18 && !strncmp(str, "inputVariableCount", 18))
      {
        mInputVariableCount = offset;
        continue;
      }

      if (slen == 19 && !strncmp(str, "outputVariableCount", 19))
      {
        mOutputVariableCount = offset;
        continue;
      }

      if (slen == 13 && !strncmp(str, "constantCount", 13))
      {
        mConstantCount = offset;
        continue;
      }

      if (slen == 18 && !strncmp(str, "otherVariableCount", 18))
      {
        mOtherVariableCount = offset;
        continue;
      }

      if (slen == 14 && !strncmp(str, "tempInputCount", 14))
      {
        mTempInputCount = offset;
        continue;
      }

      if (slen == 15 && !strncmp(str, "tempOutputCount", 15))
      {
        mTempOutputCount = offset;
        continue;
      }

      if (slen == 13 && !strncmp(str, "blackBoxCount", 13))
      {
        mBlackBoxCount = offset;
        continue;
      }

      if (mCode + offset < mCode || mCode + offset >= mFileEnd)
      {
        unmap();
        fprintf(stderr, "Black/white header has invalid function offset.\n");
        return;
      }
      if (slen == 4 && !strncmp(str, "data", 4))
        mData = mCode + offset;
      else if (slen == 14 && !strncmp(str, "initBlackBoxes", 14))
        mInitBlackBoxes = mCode + offset;
      else if (slen == 13 && !strncmp(str, "initConstants", 13))
        mInitConstants = mCode + offset;
      else if (slen == 3 && !strncmp(str, "run", 3))
        mRun = mCode + offset;
      else if (slen == 5 && !strncmp(str, "train", 5))
        mTrain = mCode + offset;
    }

    if (mData == NULL)
      mData = mFileEnd;

    INPUTS = reinterpret_cast<double*>(mData);
    OUTPUTS = INPUTS + mInputVariableCount;
    CONSTANTS = OUTPUTS + mOutputVariableCount;
    VARIABLES = CONSTANTS + mConstantCount;
    TMPINPUTS = VARIABLES + mOtherVariableCount;
    TMPOUTPUTS = TMPINPUTS + mTempInputCount;

    mReady = true;
  }

  ~MixedModelVM()
  {
    unmap();
  }

  uint32_t
  countInputVariables() const
  {
    return mInputVariableCount;
  }

  uint32_t
  countOutputVariables() const
  {
    return mOutputVariableCount;
  }

  uint32_t
  countConstants() const
  {
    return mConstantCount;
  }

  uint32_t
  countOtherVariables() const
  {
    return mOtherVariableCount;
  }

  uint32_t
  countTempInputs() const
  {
    return mTempInputCount;
  }

  uint32_t
  countTempOutputs() const
  {
    return mTempOutputCount;
  }

  uint32_t
  countBlackBoxes() const
  {
    return mBlackBoxCount;
  }

  void
  initConstants()
  {
    if (mInitConstants != NULL)
      execute(mInitConstants);
  }

  void
  initBlackBoxes()
  {
    if (mInitBlackBoxes != NULL)
      execute(mInitBlackBoxes);
  }

  void
  train(double * aINPUTS, double * aOUTPUTS)
  {
    memcpy(INPUTS, aINPUTS, mInputVariableCount * sizeof(double));
    memcpy(OUTPUTS, aOUTPUTS, mOutputVariableCount * sizeof(double));

    if (mTrain != NULL)
      execute(mTrain);
  }

  void
  run(double * aINPUTS, double * aOUTPUTS)
  {
    memcpy(INPUTS, aINPUTS, mInputVariableCount * sizeof(double));

    if (mRun != NULL)
      execute(mRun);

    memcpy(aOUTPUTS, OUTPUTS, mOutputVariableCount * sizeof(double));
  }

private:
  size_t mFileSize;
  uint8_t * mFile, * mData, * mCode, * mInitBlackBoxes, * mInitConstants,
          * mRun, * mTrain, * mFileEnd, * mEIP, * mSetJumps[10];
  uint32_t * mStack, * mStackEnd, * mESP;
  uint32_t mHeaderEnd;
  uint32_t mInputVariableCount, mOutputVariableCount, mConstantCount,
    mOtherVariableCount, mTempInputCount, mTempOutputCount, mBlackBoxCount,
    mnSetJumps;
#define VMFLAG_HALT 1
#define VMFLAG_GPE 2
  uint32_t mVMFlags;
#define VM_HAD_HALT (mVMFlags & VMFLAG_HALT)
#define VM_HAD_GPE (mVMFlags & VMFLAG_GPE)
  bool mReady;
  double mReturned;

  typedef void (MixedModelVM::* OpcodeFunction)();
  static OpcodeFunction opCodes[256];

  void unmap()
  {
    if (mFile != NULL)
      munmap(mFile, mFileSize);
    mFile = NULL;
  }

  void execute(uint8_t* aEIP);

  void gpe(const char* aWhy)
  {
    printf("VM General Protection Error: %s\n", aWhy);
    mVMFlags |= VMFLAG_HALT | VMFLAG_GPE;
  }

  uint32_t pop()
  {
    if (mESP != mStack)
    {
      mESP--;
      return *mESP;
    }

    gpe("Attempt to pop from an empty stack.");
    return 0;
  }

  double popDouble()
  {
    if (mESP - 1 >  mStack)
    {
      mESP -= 2;
      return *reinterpret_cast<double*>(mESP);
    }

    gpe("Attempt to pop double from an empty stack.");
    return 0.0;
  }

  void push(uint32_t aVal)
  {
    if (mESP == mStackEnd - 1)
      gpe("Stack overflow.");
    *mESP++ = aVal;
  }

  void pushDouble(double aVal)
  {
    if (mESP >= mStackEnd - 2)
    {
      gpe("Stack overflow.");
      return;
    }
    uint32_t* aRep = reinterpret_cast<uint32_t*>(&aVal);
    *mESP++ = aRep[0];
    *mESP++ = aRep[1];
  }

  void invalidOpcode()
  {
    gpe("Invalid opcode invoked.");
  }

  uint32_t getCodeLong()
  {
    if (mEIP + 3 >= mData)
    {
      gpe("Instruction pointer outside code range.");
      return 0;
    }
    uint32_t ret = *reinterpret_cast<uint32_t*>(mEIP);
    mEIP += 4;

    return ret;
  }

  void pushImmed()
  {
    push(getCodeLong());
  }

  void opAcos()
  {
    pushDouble(acos(popDouble()));
  }

  void opAcosh()
  {
    pushDouble(acosh(popDouble()));
  }

  void opAnd()
  {
    uint32_t i = 0, l = pop();
    for (; i < l; i++)
      if (popDouble() == 0.0)
      {
        pushDouble(0.0);
        for (i++; i < l; i++)
          popDouble();
        return;
      }
    pushDouble(1.0);
  }

  void opAsin()
  {
    pushDouble(asin(popDouble()));
  }

  void opAsinh()
  {
    pushDouble(asinh(popDouble()));
  }

  void opAtan()
  {
    pushDouble(atan(popDouble()));
  }

  void opAtanh()
  {
    pushDouble(atanh(popDouble()));
  }

  void opCeil()
  {
    pushDouble(ceil(popDouble()));
  }

  void opCos()
  {
    pushDouble(cos(popDouble()));
  }

  void opCosh()
  {
    pushDouble(cosh(popDouble()));
  }

  void opDivide()
  {
    double d = popDouble(), n = popDouble();
    pushDouble(n / d);
  }

  void opEqual()
  {
    uint32_t i = 0, l = pop();
    if (l == 0)
    {
      pushDouble(1.0);
      return;
    }
    double val = popDouble();

    for (i--; i < l; i++)
      if (popDouble() != val)
      {
        pushDouble(0.0);
        for (; i < l; i++)
          popDouble();
        return;
      }
      
    pushDouble(1.0);
  }

  void opExp()
  {
    pushDouble(exp(popDouble()));
  }

  void opFabs()
  {
    pushDouble(fabs(popDouble()));
  }

  void opFactorial()
  {
    double f = popDouble();
    double v = 1.0;
    while (f > 0.0)
    {
      v *= f;
      f -= 1.0;
    }
    pushDouble(v);
  }

  void opFloor()
  {
    pushDouble(floor(popDouble()));
  }

  double gcd_pair(double a, double b)
  {
    uint32_t ai = (uint32_t)fabs(a), bi = (uint32_t)fabs(b);
    unsigned int shift = 0;
    if (ai == 0)
      return bi;
    if (bi == 0)
      return ai;
#define EVEN(x) ((x&1)==0)
    while (EVEN(ai) && EVEN(bi))
    {
      shift++;
      ai >>= 1;
      bi >>= 1;
    }
    do
    {
      if (EVEN(ai))
        ai >>= 1;
      else if (EVEN(bi))
        bi >>= 1;
      else if (ai >= bi)
        ai = (ai - bi) >> 1;
      else
        bi = (bi - ai) >> 1;
    }
    while (ai > 0);

    return (bi << shift);
  }

  double lcm_pair(double a, double b)
  {
    return (a * b) / gcd_pair(a, b);
  }

  void opGcd()
  {
    uint32_t i = 0, l = pop(), j = 0;

    if (l == 0)
    {
      pushDouble(1.0);
      return;
    }

    double * storage1, * storage2, * t, ret;
    storage1 = new double[l];
    if (storage1 == NULL)
    {
      gpe("Not enough memory to compute operation result.");
      return;
    }
    storage2 = new double[l >> 1];
    if (storage2 == NULL)
    {
      delete [] storage1;
      gpe("Not enough memory to compute operation result.");
      return;
    }

    for (; i < l - 1; i += 2)
      storage1[j++] = gcd_pair(popDouble(), popDouble());

    if (i < l)
      storage1[j++] = popDouble();

    while (j != 1)
    {
      l = j;
      j = 0;

      for (i = 0; i < j - 1; i += 2)
        storage2[j++] = gcd_pair(storage1[i], storage1[i + 1]);
      if (i < j)
        storage2[j++] = storage1[i];

      t = storage1;
      storage1 = storage2;
      storage2 = t;
    }

    ret = storage1[0];
    delete [] storage1;
    delete [] storage2;

    pushDouble(ret);
  }

  void opGeq()
  {
    uint32_t i = 0, l = pop();
    if (l == 0)
    {
      pushDouble(1.0);
      return;
    }
    double val = popDouble(), t;

    // Remember, the order is reversed due to the stack.
    for (i--; i < l; i++)
      if ((t = popDouble()) > val)
      {
        pushDouble(0.0);
        for (; i < l; i++)
          popDouble();
        return;
      }
      else
        val = t;

    pushDouble(1.0);
  }

  void opGt()
  {
    uint32_t i = 0, l = pop();
    if (l == 0)
    {
      pushDouble(1.0);
      return;
    }
    double val = popDouble(), t;

    // Remember, the order is reversed due to the stack.
    for (i--; i < l; i++)
      if ((t = popDouble()) >= val)
      {
        pushDouble(0.0);
        for (; i < l; i++)
          popDouble();
        return;
      }
      else
        val = t;
      
    pushDouble(1.0);
  }

  void opIntegrate()
  {
    gpe("Sorry, definite integrals not implemented.");
  }

  void opLcm()
  {
    uint32_t i = 0, l = pop(), j = 0;

    if (l == 0)
    {
      pushDouble(1.0);
      return;
    }

    double * storage1, * storage2, * t, ret;
    storage1 = new double[l];
    if (storage1 == NULL)
    {
      gpe("Not enough memory to compute operation result.");
      return;
    }
    storage2 = new double[l >> 1];
    if (storage2 == NULL)
    {
      delete [] storage1;
      gpe("Not enough memory to compute operation result.");
      return;
    }

    for (; i < l - 1; i += 2)
      storage1[j++] = lcm_pair(popDouble(), popDouble());

    if (i < l)
      storage1[j++] = popDouble();

    while (j != 1)
    {
      l = j;
      j = 0;

      for (i = 0; i < j - 1; i += 2)
        storage2[j++] = lcm_pair(storage1[i], storage1[i + 1]);
      if (i < j)
        storage2[j++] = storage1[i];

      t = storage1;
      storage1 = storage2;
      storage2 = t;
    }

    ret = storage1[0];
    delete [] storage1;
    delete [] storage2;

    pushDouble(ret);
  }

  void opLeq()
  {
    uint32_t i = 0, l = pop();
    if (l == 0)
    {
      pushDouble(1.0);
      return;
    }
    double val = popDouble(), t;

    // Remember, the order is reversed due to the stack.
    for (i--; i < l; i++)
      if ((t = popDouble()) > val)
      {
        pushDouble(0.0);
        for (; i < l; i++)
          popDouble();
        return;
      }
      else
        val = t;
      
    pushDouble(1.0);
  }

  void opLog()
  {
    pushDouble(log(popDouble()));
  }

  void opLt()
  {
    uint32_t i = 0, l = pop();
    if (l == 0)
    {
      pushDouble(1.0);
      return;
    }
    double val = popDouble(), t;

    // Remember, the order is reversed due to the stack.
    for (i--; i < l; i++)
      if ((t = popDouble()) <= val)
      {
        pushDouble(0.0);
        for (; i < l; i++)
          popDouble();
        return;
      }
      else
        val = t;
    
    pushDouble(1.0);
  }

  void opMax()
  {
    uint32_t i = 0, l = pop();
    if (l == 0)
    {
      pushDouble(strtod("NAN", NULL));
      return;
    }

    double best = popDouble(), t;
    for (l--; i < l; i++)
    {
      t = popDouble();
      if (t > best)
        best = t;
    }
    pushDouble(best);
  }

  void opMin()
  {
    uint32_t i = 0, l = pop();
    if (l == 0)
    {
      pushDouble(strtod("NAN", NULL));
      return;
    }

    double best = popDouble(), t;
    for (l--; i < l; i++)
    {
      t = popDouble();
      if (t < best)
        best = t;
    }
    pushDouble(best);
  }

  void opMinus()
  {
    pushDouble(-(popDouble() - popDouble()));
  }

  void opMod()
  {
    double d = popDouble(), n = popDouble();
    if (!isfinite(n) || !isfinite(d))
    {
      pushDouble(strtod("NAN", NULL));
      return;
    }
    int in = (int)n;
    int id = (int)d;
    if (id == 0)
    {
      pushDouble(strtod("NAN", NULL));
      return;
    }

    pushDouble(in % id);
  }

  void opNeq()
  {
    pushDouble((popDouble() == popDouble()) ? 0.0 : 1.0);
  }

  void opNot()
  {
    pushDouble((popDouble() == 0.0) ? 1.0 : 0.0);
  }

  void opOr()
  {
    uint32_t i = 0, l = pop();
    for (; i < l; i++)
      if (popDouble() != 0.0)
      {
        pushDouble(1.0);
        for (i++; i < l; i++)
          popDouble();
        return;
      }
    pushDouble(0.0);
  }

  void opPlus()
  {
    uint32_t i = 0, l = pop();
    double sum = 0.0;
    for (; i < l; i++)
      sum += popDouble();
    pushDouble(sum);
  }

  void opPow()
  {
    double e = popDouble(), v = popDouble();
    pushDouble(pow(v, e));
  }

  void opQuot()
  {
    double d = popDouble(), n = popDouble();
    if (!isfinite(n) || !isfinite(d))
    {
      pushDouble(strtod("NAN", NULL));
      return;
    }
    int in = (int)n;
    int id = (int)d;
    if (id == 0)
    {
      pushDouble(strtod("NAN", NULL));
      return;
    }

    pushDouble(in / id);
  }

  void opReturn()
  {
    // Return with no stack means halt...
    if (mESP == mStack)
    {
      mVMFlags |= VMFLAG_HALT;
      return;
    }

    mEIP = mCode + pop();
    if (mEIP < mCode || mEIP >= mData)
      gpe("Return into non-code address.");
  }

  void opReturn1()
  {
    if (mESP == mStack)
    {
      gpe("Return1 without stack operand.");
      return;
    }

    double retVal = popDouble();

    // Return with no stack means halt...
    if (mESP == mStack)
    {
      mVMFlags |= VMFLAG_HALT;
      mReturned = retVal;
      return;
    }

    mEIP = mCode + pop();
    if (mEIP < mCode || mEIP >= mData)
      gpe("Return into non-code address.");

    pushDouble(retVal);
  }

  void opSin()
  {
    pushDouble(sin(popDouble()));
  }

  void opSinh()
  {
    pushDouble(sinh(popDouble()));
  }

  void opTan()
  {
    pushDouble(tan(popDouble()));
  }

  void opTanh()
  {
    pushDouble(tanh(popDouble()));
  }

  void opTimes()
  {
    uint32_t i = 0, l = pop();
    double prod = 0.0;
    for (; i < l; i++)
      prod *= popDouble();
    pushDouble(prod);
  }

  void opUminus()
  {
    pushDouble(-popDouble());
  }

  void opXor()
  {
    uint32_t i = 0, l = pop();
    bool val = false;
    for (; i < l; i++)
      if (popDouble() != 0.0)
        val = !val;
    pushDouble(val ? 1.0 : 0.0);
  }

  void opConverged()
  {
    pushDouble(runConverged() ? 1.0 : 0.0);
  }

  void opJmpIf()
  {
    bool doJump = (popDouble() != 0.0);
    uint32_t jmpTo = pop();

    if (doJump == 0.0)
      return;

    if (jmpTo > sizeof(mnSetJumps))
    {
      gpe("jmpif for invalid setjmp ID.");
      return;
    }
    if (mSetJumps[jmpTo] == (uint8_t*)0xFFFFFFFF)
    {
      gpe("jmpif to ID not previously setjmpd.");
      return;
    }

    mEIP = mSetJumps[jmpTo];
  }

  void opResetConvergence()
  {
    resetConvergenceCounter();
  }

  void opSavePoint()
  {
    convergenceSavePoint();
  }

  void opCallTrain()
  {
    uint32_t bbn = pop();
    if (bbn >= mBlackBoxCount)
    {
      gpe("calltrain: reference to black box which doesn't exist.");
      return;
    }
    if (blackBoxes[bbn] == NULL)
    {
      gpe("calltrain: reference to black box which hasn't been initialised.");
      return;
    }

    blackBoxes[bbn]->train(TMPINPUTS, TMPOUTPUTS);
  }

  void opCallRun()
  {
    uint32_t bbn = pop();
    if (bbn >= mBlackBoxCount)
    {
      printf("Attempted to access black box 0x%x but only %u\n",
             bbn, mBlackBoxCount);
      gpe("callrun: reference to black box which doesn't exist.");
      return;
    }
    if (blackBoxes[bbn] == NULL)
    {
      gpe("callrun: reference to black box which hasn't been initialised.");
      return;
    }

    blackBoxes[bbn]->run(TMPINPUTS, TMPOUTPUTS);
  }

  void opCreateBB()
  {
    uint32_t bbn = pop();
    if (bbn >= mBlackBoxCount)
    {
      gpe("createbb: reference to black box which doesn't exist.");
      return;
    }
    if (blackBoxes[bbn] != NULL)
    {
      gpe("createbb: attempt to create the same black box twice.");
      return;
    }

    uint32_t bbOutCount = pop(), bbInCount = pop(), bbStringAddr = pop();

    if (bbInCount > mTempInputCount)
    {
      gpe("Black box input count exceeds specified maximal input count.");
      return;
    }

    if (bbOutCount > mTempOutputCount)
    {
      gpe("Black box output count exceeds specified maximal output count.");
      return;
    }

    if (bbStringAddr >= (mFileEnd - mData) ||
        (bbStringAddr + 3) >= (mFileEnd - mData))
    {
      gpe("String constant address out of data range.");
      return;
    }
    uint32_t len = *reinterpret_cast<uint32_t*>(mData + bbStringAddr);
    
    if (bbStringAddr + 4 + len > (mFileEnd - mData))
    {
      gpe("String length puts end of string out of data region.");
      return;
    }

    std::wstring URI(reinterpret_cast<wchar_t*>(bbStringAddr + 4 + mData),
                     len / sizeof(wchar_t));

    try
    {
      createBlackBox(bbn, URI.c_str(), bbInCount, bbOutCount);
    }
    catch (...)
    {
      gpe("createBlackBox instruction failed.");
    }
  }

  void opGetAvgInput()
  {
    uint32_t idx = pop();
    uint32_t bbn = pop();
    if (bbn >= mBlackBoxCount)
    {
      gpe("getavginput: reference to black box which doesn't exist.");
      return;
    }
    if (blackBoxes[bbn] == NULL)
    {
      gpe("getavginput: reference to black box which hasn't been initialised.");
      return;
    }

    pushDouble(blackBoxes[bbn]->GetAverageInput(idx));
  }

  double
  executeCallback(uint8_t* func)
  {
    uint32_t* stackSave = mStack;
    execute(func);
    mStack = stackSave;
    if (VM_HAD_GPE)
      return 0.0;
    mVMFlags &= ~VMFLAG_HALT;
    return mReturned;
  }

  double
  take_numeric_derivative
  (
   uint8_t* aFunc,
   double* aVal
  )
  {
    double saved_x, value0, value1, value2, delta, slope1, slope2, ratio;
    saved_x = *aVal;
    /*
     * Since we have only 52 bits of mantissa, we need delta to flip only the
     * last couple of bits.
     */
    delta = saved_x * 1E-10;
    value0 = executeCallback(aFunc);
    *aVal -= delta;
    value1 = executeCallback(aFunc);
    *aVal = saved_x + delta;
    value2 = executeCallback(aFunc);
    *aVal = saved_x;
    /* We take two numeric derivatives, so we can compare them. This stops us
     * from getting trapped at around discontinuities(which otherwise produce
     * very steep trapping slopes).
     */
    slope1 = (value0 - value1) / delta;
    slope2 = (value2 - value0) / delta;
    
    /* If one slope is zero, return the other... */
    if (slope1 == 0.0 || !isfinite(slope1))
      return slope2;
    if (slope2 == 0.0 || !isfinite(slope2))
      return slope1;
    
    ratio = slope1 / slope2;
    /* If the slopes are similar, return the average... */
    if (ratio > 0.5 && ratio < 2)
      return (slope1 + slope2) / 2.0;
    /* Otherwise, return the least steep of the two... */
    if (fabs(slope1) < fabs(slope2))
      return slope1;
    return slope2;
  }

#define NR_RANDOM_STARTS 100
#define NR_MAX_STEPS 1000
#define NR_MAX_STEPS_INITIAL 10

  void opMinimise()
  {
    uint32_t minVar = pop();
    uint32_t minFunc = pop();

    if (minFunc >= mData - mCode)
    {
      gpe("Attempt to minimise a function which isn't in the code block.");
      return;
    }
    uint8_t * func = mCode + minFunc;
    
    if (minVar + 2 > (mFileEnd - mData) * 4 ||
        minVar > minVar + 2)
    {
      gpe("Attempt to minimise a function over a variable not in the data block.");
      return;
    }

    double * val = reinterpret_cast<double*>(mData + minVar * 4);

    double best_X, best_fX = INFINITY;
    double current_X, current_fX, current_dfX_dX;
    uint32_t steps, maxsteps;
    uint32_t i;
    
    current_X = *val;

    /* We use a 100 random start Newton-Raphson algorithm... */
    for (i = 0; i < NR_RANDOM_STARTS; i++)
    {
      /* Choose a random X as a starting point, except for the first run. */
      if (i > 0 || !isfinite(current_X))
        current_X = random_double_logUniform();

      maxsteps = NR_MAX_STEPS_INITIAL;
      for (steps = 0; steps < maxsteps; steps++)
      {
        *val = current_X;
        current_fX = executeCallback(func);
        if (!isfinite(current_fX))
        {
          break;
        }
        
        if (best_fX > current_fX)
        {
          /* We can go past NR_MAX_STEPS_INITIAL steps up to NR_MAX_STEPS steps,
           * but only if we keep improving on our previous answer.
           */
          maxsteps += 2;
          if (maxsteps > NR_MAX_STEPS)
            maxsteps = NR_MAX_STEPS;
          best_fX = current_fX;
          best_X = current_X;
          /* This is quite far from 0(relatively speaking for a double) to avoid
           * numerical instability issues when the minimum doesn't exist but the
           * limit at a point from a particular direction would otherwise be the
           * minimum.
           */
          if (best_fX <= 1E-250)
          {
            break;
          }
        }
        
        current_dfX_dX = take_numeric_derivative(func, val);
        /* If it is completely flat, or infinite, we are done with this round. */
        if (!isfinite(current_dfX_dX) || current_dfX_dX == 0.0)
        {
          break;
        }

        current_X -= current_fX / current_dfX_dX;
        if (!isfinite(current_X))
        {
          break;
        }
      }
      if (best_fX <= 1E-250)
        break;
    }

    *val = best_X;
    
    take_numeric_derivative(func, val);
  }

  void opSetJmp()
  {
    uint32_t sj = pop();
    if (sj >= mnSetJumps)
    {
      gpe("Attempt to setjmp to an index out of range.");
      return;
    }

    mSetJumps[sj] = mEIP;
  }

  void opFetchDouble()
  {
    uint32_t fetchVar = getCodeLong();
    
    if (fetchVar + 2 > (mFileEnd - mData) * 4 ||
        fetchVar > fetchVar + 2)
    {
      gpe("Attempt to fetch a variable which isn't in the data block.");
      return;
    }

    pushDouble(*reinterpret_cast<double*>(mData + fetchVar * 4));
  }

  void opMov()
  {
    uint32_t movInto = pop();
    double movWhat = popDouble();

    if (movInto + 2 > (mFileEnd - mData) * 4 ||
        movInto > movInto + 2)
    {
      gpe("Attempt to move into a variable which isn't in the data block.");
      return;
    }

    *reinterpret_cast<double*>(mData + movInto * 4) = movWhat;
  }
};

MixedModelVM::OpcodeFunction
MixedModelVM::opCodes[256] = 
{
  &MixedModelVM::invalidOpcode /* 00 */,
  &MixedModelVM::pushImmed     /* 01 */,
  &MixedModelVM::opAcos          /* 02 */,
  &MixedModelVM::opAcosh         /* 03 */,
  &MixedModelVM::opAnd          /* 04 */,
  &MixedModelVM::opAsin         /* 05 */,
  &MixedModelVM::opAsinh        /* 06 */,
  &MixedModelVM::opAtan         /* 07 */,
  &MixedModelVM::opAtanh        /* 08 */,
  &MixedModelVM::opCeil         /* 09 */,
  &MixedModelVM::opCos          /* 0A */,
  &MixedModelVM::opCosh         /* 0B */,
  &MixedModelVM::opDivide       /* 0C */,
  &MixedModelVM::opEqual        /* 0D */,
  &MixedModelVM::opExp          /* 0E */,
  &MixedModelVM::opFabs         /* 0F */,
  &MixedModelVM::opFactorial    /* 10 */,
  &MixedModelVM::opFloor       /* 11 */,
  &MixedModelVM::opGcd         /* 12 */,
  &MixedModelVM::opGeq         /* 13 */,
  &MixedModelVM::opGt          /* 14 */,
  &MixedModelVM::opIntegrate   /* 15 */,
  &MixedModelVM::opLcm         /* 16 */,
  &MixedModelVM::opLeq         /* 17 */,
  &MixedModelVM::opLog         /* 18 */,
  &MixedModelVM::opLt          /* 19 */,
  &MixedModelVM::opMax         /* 1A */,
  &MixedModelVM::opMin         /* 1B */,
  &MixedModelVM::opMinus       /* 1C */,
  &MixedModelVM::opMod         /* 1D */,
  &MixedModelVM::opNeq         /* 1E */,
  &MixedModelVM::opNot         /* 1F */,
  &MixedModelVM::opOr          /* 20 */,
  &MixedModelVM::opPlus        /* 21 */,
  &MixedModelVM::opPow         /* 22 */,
  &MixedModelVM::opQuot        /* 23 */,
  &MixedModelVM::opReturn      /* 24 */,
  &MixedModelVM::opSin         /* 25 */,
  &MixedModelVM::opSinh        /* 26 */,
  &MixedModelVM::opTan         /* 27 */,
  &MixedModelVM::opTanh        /* 28 */,
  &MixedModelVM::opTimes       /* 29 */,
  &MixedModelVM::opUminus      /* 2A */,
  &MixedModelVM::opXor         /* 2B */,
  &MixedModelVM::opConverged   /* 2C */,
  &MixedModelVM::opJmpIf       /* 2D */,
  &MixedModelVM::opResetConvergence /* 2E */,
  &MixedModelVM::opSavePoint   /* 2F */,
  &MixedModelVM::opCallTrain   /* 30 */,
  &MixedModelVM::opCallRun     /* 31 */,
  &MixedModelVM::opCreateBB    /* 32 */,
  &MixedModelVM::opGetAvgInput /* 33 */,
  &MixedModelVM::opMinimise    /* 34 */,
  &MixedModelVM::opSetJmp      /* 35 */,
  &MixedModelVM::opFetchDouble /* 36 */,
  &MixedModelVM::opMov         /* 37 */,
  &MixedModelVM::opReturn1     /* 38 */,
  &MixedModelVM::invalidOpcode /* 39 */,
  &MixedModelVM::invalidOpcode /* 3A */,
  &MixedModelVM::invalidOpcode /* 3B */,
  &MixedModelVM::invalidOpcode /* 3C */,
  &MixedModelVM::invalidOpcode /* 3D */,
  &MixedModelVM::invalidOpcode /* 3E */,
  &MixedModelVM::invalidOpcode /* 3F */,
  &MixedModelVM::invalidOpcode /* 40 */,
  &MixedModelVM::invalidOpcode /* 41 */,
  &MixedModelVM::invalidOpcode /* 42 */,
  &MixedModelVM::invalidOpcode /* 43 */,
  &MixedModelVM::invalidOpcode /* 44 */,
  &MixedModelVM::invalidOpcode /* 45 */,
  &MixedModelVM::invalidOpcode /* 46 */,
  &MixedModelVM::invalidOpcode /* 47 */,
  &MixedModelVM::invalidOpcode /* 48 */,
  &MixedModelVM::invalidOpcode /* 49 */,
  &MixedModelVM::invalidOpcode /* 4A */,
  &MixedModelVM::invalidOpcode /* 4B */,
  &MixedModelVM::invalidOpcode /* 4C */,
  &MixedModelVM::invalidOpcode /* 4D */,
  &MixedModelVM::invalidOpcode /* 4E */,
  &MixedModelVM::invalidOpcode /* 4F */,
  &MixedModelVM::invalidOpcode /* 50 */,
  &MixedModelVM::invalidOpcode /* 51 */,
  &MixedModelVM::invalidOpcode /* 52 */,
  &MixedModelVM::invalidOpcode /* 53 */,
  &MixedModelVM::invalidOpcode /* 54 */,
  &MixedModelVM::invalidOpcode /* 55 */,
  &MixedModelVM::invalidOpcode /* 56 */,
  &MixedModelVM::invalidOpcode /* 57 */,
  &MixedModelVM::invalidOpcode /* 58 */,
  &MixedModelVM::invalidOpcode /* 59 */,
  &MixedModelVM::invalidOpcode /* 5A */,
  &MixedModelVM::invalidOpcode /* 5B */,
  &MixedModelVM::invalidOpcode /* 5C */,
  &MixedModelVM::invalidOpcode /* 5D */,
  &MixedModelVM::invalidOpcode /* 5E */,
  &MixedModelVM::invalidOpcode /* 5F */,
  &MixedModelVM::invalidOpcode /* 60 */,
  &MixedModelVM::invalidOpcode /* 61 */,
  &MixedModelVM::invalidOpcode /* 62 */,
  &MixedModelVM::invalidOpcode /* 63 */,
  &MixedModelVM::invalidOpcode /* 64 */,
  &MixedModelVM::invalidOpcode /* 65 */,
  &MixedModelVM::invalidOpcode /* 66 */,
  &MixedModelVM::invalidOpcode /* 67 */,
  &MixedModelVM::invalidOpcode /* 68 */,
  &MixedModelVM::invalidOpcode /* 69 */,
  &MixedModelVM::invalidOpcode /* 6A */,
  &MixedModelVM::invalidOpcode /* 6B */,
  &MixedModelVM::invalidOpcode /* 6C */,
  &MixedModelVM::invalidOpcode /* 6D */,
  &MixedModelVM::invalidOpcode /* 6E */,
  &MixedModelVM::invalidOpcode /* 6F */,
  &MixedModelVM::invalidOpcode /* 70 */,
  &MixedModelVM::invalidOpcode /* 71 */,
  &MixedModelVM::invalidOpcode /* 72 */,
  &MixedModelVM::invalidOpcode /* 73 */,
  &MixedModelVM::invalidOpcode /* 74 */,
  &MixedModelVM::invalidOpcode /* 75 */,
  &MixedModelVM::invalidOpcode /* 76 */,
  &MixedModelVM::invalidOpcode /* 77 */,
  &MixedModelVM::invalidOpcode /* 78 */,
  &MixedModelVM::invalidOpcode /* 79 */,
  &MixedModelVM::invalidOpcode /* 7A */,
  &MixedModelVM::invalidOpcode /* 7B */,
  &MixedModelVM::invalidOpcode /* 7C */,
  &MixedModelVM::invalidOpcode /* 7D */,
  &MixedModelVM::invalidOpcode /* 7E */,
  &MixedModelVM::invalidOpcode /* 7F */,
  &MixedModelVM::invalidOpcode /* 80 */,
  &MixedModelVM::invalidOpcode /* 81 */,
  &MixedModelVM::invalidOpcode /* 82 */,
  &MixedModelVM::invalidOpcode /* 83 */,
  &MixedModelVM::invalidOpcode /* 84 */,
  &MixedModelVM::invalidOpcode /* 85 */,
  &MixedModelVM::invalidOpcode /* 86 */,
  &MixedModelVM::invalidOpcode /* 87 */,
  &MixedModelVM::invalidOpcode /* 88 */,
  &MixedModelVM::invalidOpcode /* 89 */,
  &MixedModelVM::invalidOpcode /* 8A */,
  &MixedModelVM::invalidOpcode /* 8B */,
  &MixedModelVM::invalidOpcode /* 8C */,
  &MixedModelVM::invalidOpcode /* 8D */,
  &MixedModelVM::invalidOpcode /* 8E */,
  &MixedModelVM::invalidOpcode /* 8F */,
  &MixedModelVM::invalidOpcode /* 90 */,
  &MixedModelVM::invalidOpcode /* 91 */,
  &MixedModelVM::invalidOpcode /* 92 */,
  &MixedModelVM::invalidOpcode /* 93 */,
  &MixedModelVM::invalidOpcode /* 94 */,
  &MixedModelVM::invalidOpcode /* 95 */,
  &MixedModelVM::invalidOpcode /* 96 */,
  &MixedModelVM::invalidOpcode /* 97 */,
  &MixedModelVM::invalidOpcode /* 98 */,
  &MixedModelVM::invalidOpcode /* 99 */,
  &MixedModelVM::invalidOpcode /* 9A */,
  &MixedModelVM::invalidOpcode /* 9B */,
  &MixedModelVM::invalidOpcode /* 9C */,
  &MixedModelVM::invalidOpcode /* 9D */,
  &MixedModelVM::invalidOpcode /* 9E */,
  &MixedModelVM::invalidOpcode /* 9F */,
  &MixedModelVM::invalidOpcode /* A0 */,
  &MixedModelVM::invalidOpcode /* A1 */,
  &MixedModelVM::invalidOpcode /* A2 */,
  &MixedModelVM::invalidOpcode /* A3 */,
  &MixedModelVM::invalidOpcode /* A4 */,
  &MixedModelVM::invalidOpcode /* A5 */,
  &MixedModelVM::invalidOpcode /* A6 */,
  &MixedModelVM::invalidOpcode /* A7 */,
  &MixedModelVM::invalidOpcode /* A8 */,
  &MixedModelVM::invalidOpcode /* A9 */,
  &MixedModelVM::invalidOpcode /* AA */,
  &MixedModelVM::invalidOpcode /* AB */,
  &MixedModelVM::invalidOpcode /* AC */,
  &MixedModelVM::invalidOpcode /* AD */,
  &MixedModelVM::invalidOpcode /* AE */,
  &MixedModelVM::invalidOpcode /* AF */,
  &MixedModelVM::invalidOpcode /* B0 */,
  &MixedModelVM::invalidOpcode /* B1 */,
  &MixedModelVM::invalidOpcode /* B2 */,
  &MixedModelVM::invalidOpcode /* B3 */,
  &MixedModelVM::invalidOpcode /* B4 */,
  &MixedModelVM::invalidOpcode /* B5 */,
  &MixedModelVM::invalidOpcode /* B6 */,
  &MixedModelVM::invalidOpcode /* B7 */,
  &MixedModelVM::invalidOpcode /* B8 */,
  &MixedModelVM::invalidOpcode /* B9 */,
  &MixedModelVM::invalidOpcode /* BA */,
  &MixedModelVM::invalidOpcode /* BB */,
  &MixedModelVM::invalidOpcode /* BC */,
  &MixedModelVM::invalidOpcode /* BD */,
  &MixedModelVM::invalidOpcode /* BE */,
  &MixedModelVM::invalidOpcode /* BF */,
  &MixedModelVM::invalidOpcode /* C0 */,
  &MixedModelVM::invalidOpcode /* C1 */,
  &MixedModelVM::invalidOpcode /* C2 */,
  &MixedModelVM::invalidOpcode /* C3 */,
  &MixedModelVM::invalidOpcode /* C4 */,
  &MixedModelVM::invalidOpcode /* C5 */,
  &MixedModelVM::invalidOpcode /* C6 */,
  &MixedModelVM::invalidOpcode /* C7 */,
  &MixedModelVM::invalidOpcode /* C8 */,
  &MixedModelVM::invalidOpcode /* C9 */,
  &MixedModelVM::invalidOpcode /* CA */,
  &MixedModelVM::invalidOpcode /* CB */,
  &MixedModelVM::invalidOpcode /* CC */,
  &MixedModelVM::invalidOpcode /* CD */,
  &MixedModelVM::invalidOpcode /* CE */,
  &MixedModelVM::invalidOpcode /* CF */,
  &MixedModelVM::invalidOpcode /* D0 */,
  &MixedModelVM::invalidOpcode /* D1 */,
  &MixedModelVM::invalidOpcode /* D2 */,
  &MixedModelVM::invalidOpcode /* D3 */,
  &MixedModelVM::invalidOpcode /* D4 */,
  &MixedModelVM::invalidOpcode /* D5 */,
  &MixedModelVM::invalidOpcode /* D6 */,
  &MixedModelVM::invalidOpcode /* D7 */,
  &MixedModelVM::invalidOpcode /* D8 */,
  &MixedModelVM::invalidOpcode /* D9 */,
  &MixedModelVM::invalidOpcode /* DA */,
  &MixedModelVM::invalidOpcode /* DB */,
  &MixedModelVM::invalidOpcode /* DC */,
  &MixedModelVM::invalidOpcode /* DD */,
  &MixedModelVM::invalidOpcode /* DE */,
  &MixedModelVM::invalidOpcode /* DF */,
  &MixedModelVM::invalidOpcode /* E0 */,
  &MixedModelVM::invalidOpcode /* E1 */,
  &MixedModelVM::invalidOpcode /* E2 */,
  &MixedModelVM::invalidOpcode /* E3 */,
  &MixedModelVM::invalidOpcode /* E4 */,
  &MixedModelVM::invalidOpcode /* E5 */,
  &MixedModelVM::invalidOpcode /* E6 */,
  &MixedModelVM::invalidOpcode /* E7 */,
  &MixedModelVM::invalidOpcode /* E8 */,
  &MixedModelVM::invalidOpcode /* E9 */,
  &MixedModelVM::invalidOpcode /* EA */,
  &MixedModelVM::invalidOpcode /* EB */,
  &MixedModelVM::invalidOpcode /* EC */,
  &MixedModelVM::invalidOpcode /* ED */,
  &MixedModelVM::invalidOpcode /* EE */,
  &MixedModelVM::invalidOpcode /* EF */,
  &MixedModelVM::invalidOpcode /* F0 */,
  &MixedModelVM::invalidOpcode /* F1 */,
  &MixedModelVM::invalidOpcode /* F2 */,
  &MixedModelVM::invalidOpcode /* F3 */,
  &MixedModelVM::invalidOpcode /* F4 */,
  &MixedModelVM::invalidOpcode /* F5 */,
  &MixedModelVM::invalidOpcode /* F6 */,
  &MixedModelVM::invalidOpcode /* F7 */,
  &MixedModelVM::invalidOpcode /* F8 */,
  &MixedModelVM::invalidOpcode /* F9 */,
  &MixedModelVM::invalidOpcode /* FA */,
  &MixedModelVM::invalidOpcode /* FB */,
  &MixedModelVM::invalidOpcode /* FC */,
  &MixedModelVM::invalidOpcode /* FD */,
  &MixedModelVM::invalidOpcode /* FE */,
  &MixedModelVM::invalidOpcode /* FF */
};

void
MixedModelVM::execute(uint8_t* aEIP)
{
  mVMFlags = 0;
  mEIP = aEIP;
  mESP = mStack;

  while (!VM_HAD_HALT)
  {
    if (mEIP >= mData)
    {
      gpe("Instruction pointer outside code range.");
      return;
    }
    
    (this->*opCodes[*mEIP++])();
  }
}


MixedModelBase*
createMixedModelVM(const char* filename)
{
  return new MixedModelVM(filename);
}
